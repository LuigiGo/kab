package com.ph.com.psr_kap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.commons.net.ftp.FTPClient;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class Asyc_GetUserFromFTP extends AsyncTask<String, Integer, Boolean> {

	String error;
	File _uploadFile;
	String fileName, fname;
	FileInputStream fis = null;
	boolean result;
	FTPClient client = new FTPClient();
	
	ProgressDialog progressDialog;
	Context context;
	Calendar calendar;
	Dialog dialog1;
	Typeface tf;

	ArrayList<String> listUsername;
	ArrayList<String> listPassword;
	ArrayList<String> listPsrCode;
	ArrayList<String> listPsrName;

	String user, pass;
	boolean invalidUser;
	DatabaseHandler databaseHandler;
	int totalNoOfItemsToBeUpdated, percentage;
	double itemsUpdated;

	public Asyc_GetUserFromFTP(Context context, String user, String pass) {

		this.context = context;
		this.user = user;
		this.pass = pass;
//		tf = Typeface.createFromAsset(context.getAssets(), "fonts/GOTHIC.TTF");
		calendar = Calendar.getInstance();

		listUsername = new ArrayList<String>();
		listPassword = new ArrayList<String>();
		listPsrCode = new ArrayList<String>();
		listPsrName = new ArrayList<String>();

		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}

	}

	protected void onPreExecute() {
		
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Validating login credentials...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	
	}

	@Override
	protected Boolean doInBackground(String... params) {

//		CSV();

//		uploadFile();
		
		
		
		String txtUsersFile = Constants.pathUsers + "/Users.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\t";

		// READ CSV
		try {

			br = new BufferedReader(new FileReader(txtUsersFile));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] UserDetails = line.split(cvsSplitBy);

				// if (UserDetails.length == 3) {
				Log.i("Username", UserDetails[0].replace("\"", ""));
				Log.i("Password", UserDetails[1].replace("\"", ""));
				Log.i("psr_code", UserDetails[2].replace("\"", ""));
				Log.i("psr_name", UserDetails[3].replace("\"", ""));

					listUsername.add(UserDetails[0].replace("\"", ""));
					listPassword.add(UserDetails[1].replace("\"", ""));
					listPsrCode.add(UserDetails[2].replace("\"", ""));
					listPsrName.add(UserDetails[2].replace("\"", ""));


			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// ///////////////////////// UPDATING TABLES
		// ////////////////////////////////
		totalNoOfItemsToBeUpdated = listUsername.size();

		// update table account

		for (int i = 0; i < listUsername.size(); i++) {
			if (i >= 1) {

				String userName = listUsername.get(i);
				String userPass = listPassword.get(i);
				String userPSRCode = listPsrCode.get(i);
				String userPSRName = listPsrName.get(i);
				ContentValues values = new ContentValues();

				values.put(Constants.PSR_USERNAME, userName);
				values.put(Constants.PSR_PASSWORD, userPass);
				values.put(Constants.PSR_CODE, userPSRCode);
				values.put(Constants.PSR_NAME, userPSRName);

				databaseHandler.updatePSRTable(values);

				itemsUpdated++;
				percentage = (int) ((itemsUpdated / totalNoOfItemsToBeUpdated) * 100);
				updateProgress(percentage);

			}

		}
//		Log.i("user", "user: " + user + " pass: " + pass);
//
//		if (databaseHandler.getAccount(user, pass)) {
//
//			return true;
//		} else {
//			invalidUser = true;
//
//			return false;
//		}
		
		return true;

	}

	private void deleteFile(String inputPath, String inputFile) {
		try {
			// delete the original file
			new File(inputPath + inputFile).delete();

		} catch (Exception e) {
			Log.e("tag", e.getMessage());
		}
	}

	
	protected void updateProgress(Integer value) {
		if (progressDialog != null && progressDialog.isShowing() == true) {
			progressDialog.setProgress(value);
			Log.i("Percent",
					String.valueOf(value) + " " + String.valueOf(itemsUpdated)
							+ " " + String.valueOf(totalNoOfItemsToBeUpdated));
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);

		progressDialog.dismiss();
		
		Log.i("ON POST EXECUTE ----------------", "ASYC_GETUSERFROMFTP ---------------");

//		if (result) {
//
//		
//			
////			Intent i = new Intent(context, RegistrationPage.class);
////			context.startActivity(i);
////			
////			((Activity) context).finish();
//			
//			
//			
//		
//			
//		} else if (result == false && invalidUser == true) {
//			Log.i("aaaaa", "" + result);
//
//			// AlertDialog.Builder builder = new AlertDialog.Builder(context);
//			// builder.setTitle("Information");
//			// builder.setMessage("Username is invalid").setNeutralButton("OK",
//			// new DialogInterface.OnClickListener() {
//			// public void onClick(DialogInterface dialog, int which) {
//			// dialog.cancel();
//			// }
//			// });
//			// AlertDialog alertDialog = builder.create();
//			// alertDialog.show();
////			alertDialogNeutral("Information", "Account does not exist.");
//			
//			Toast.makeText(context,
//					"Login failed. Username and password don't match.",
//					Toast.LENGTH_LONG).show();
//			
//		} else {
//			
//			if(error == null){
//			AlertDialog.Builder builder = new AlertDialog.Builder(context);
//			builder.setTitle("Information");
//			builder.setMessage("File not found.").setNeutralButton("OK",
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.cancel();
//						}
//					});
//			AlertDialog alertDialog = builder.create();
//			alertDialog.show();
//			
//			}else{
//				AlertDialog.Builder builder = new AlertDialog.Builder(context);
//				builder.setTitle("Information");
//				builder.setMessage("No internet access.").setNeutralButton("OK",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog, int which) {
//								dialog.cancel();
//							}
//						});
//				AlertDialog alertDialog = builder.create();
//				alertDialog.show();
//				
//			}
//		}

	}

	// create CSV file
//	private File createCSV(String fileName, String columnString,
//			String dataString) {
//		String combinedString = columnString + "\n" + dataString;
//
//		File myFile = null;
//
//		Log.i("000000000000", "2222222222");
//
//		try {
//			Log.i("111111111", "SSSSSSSS");
//
//			
//			fname = fileName + ".txt";
//			myFile = new File(Constants.pathRequest, fileName + ".txt");
//			FileOutputStream mFileOutStream = new FileOutputStream(myFile);
//			mFileOutStream.write(combinedString.getBytes());
//			mFileOutStream.close();
//
//			
//			
//
//			// Toast.makeText(getApplicationContext(),"CSV File Save in " +
//			// String.valueOf(myFile), 10).show();
//
//		} catch (Exception e) {
//
//			e.printStackTrace();
//
//		}
//		return myFile;
//	}
//
//	public void CSV() {
//
//		String a, b,c,d;
//
//		if (calendar.get(Calendar.MONTH) < 10 && calendar.get(Calendar.DATE) < 10) {
//			a = "0" + (calendar.get(Calendar.MONTH) + 1);
//			b = "0" + calendar.get(Calendar.DATE);
//		} else if (calendar.get(Calendar.DATE) < 10) {
//			b = "0" + calendar.get(Calendar.DATE);
//			a = "" + (calendar.get(Calendar.MONTH) + 1);
//		} else if (calendar.get(Calendar.MONTH) < 10) {
//			a = "0" + (calendar.get(Calendar.MONTH) + 1);
//			b = "" + calendar.get(Calendar.DATE);
//		} else {
//			a = "" + (calendar.get(Calendar.MONTH) + 1);
//			b = "" + calendar.get(Calendar.DATE);
//		}
//
//
//		String date = String.valueOf(calendar.get(Calendar.YEAR)) + "" + a + ""
//				+ b;
//		
//		if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
//			c = "0" + (calendar.get(Calendar.HOUR_OF_DAY));
//			d = "" + calendar.get(Calendar.MINUTE);
//		} else if (calendar.get(Calendar.MINUTE) < 10) {
//			d = "0" + (calendar.get(Calendar.MINUTE));
//			c = "" + (calendar.get(Calendar.HOUR_OF_DAY));
//		} else {
//			c = "" + (calendar.get(Calendar.HOUR_OF_DAY));
//			d = "" + calendar.get(Calendar.MINUTE);
//		}
//
//		String time = c + ""+ d;
//
//		String column_header = "username";
//
//		String data_string = user;
//
//		// createCSV("RiteCare: Profiling data_" + date + " " + time,
//		// column_header, data_string);
//
//		createCSV("" + user + "_coderequest_" + date + "_" + time,
//				column_header, data_string);
//
//	}

//	public void alertDialogNeutral(String _title, String _message) {
//		dialog1 = new Dialog(context);
//		dialog1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//		dialog1.getWindow().setBackgroundDrawable(
//				new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		dialog1.setContentView(R.layout.custom_dialog_neutral);
//
//		TextView lbl_title = (TextView) dialog1.findViewById(R.id.lbl_title);
//		TextView lbl_message = (TextView) dialog1
//				.findViewById(R.id.lbl_message);
//		TextView btn_dismiss = (TextView) dialog1
//				.findViewById(R.id.btn_dismiss);
//		lbl_title.setTypeface(tf);
//		lbl_message.setTypeface(tf);
//		btn_dismiss.setTypeface(tf);
//
//		lbl_message.setText(_message);
//		lbl_title.setText(_title);
//
//		btn_dismiss.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				dialog1.dismiss();
//			}
//		});
//
//		dialog1.show();
//	}
	
	
	
//	
//	public void uploadFile()
//	{
//		try {
//			Log.i("--- Processing", "institution list...");
//		if(NW_Helper.isInternetAvailable(context)){
//			try {
//				client.connect(Constants.FTP_ADDRESS);
//
//				
//				result = client.login(Constants.FTP_USERNAME, Constants.FTP_PASSWORD);
//
//				Log.i("Username and Password", Constants.FTP_USERNAME + Constants.FTP_PASSWORD );
//				Log.i("RESULT", ""+result);
//				
//				
//				if (result == true) {
//					System.out.println("Successfully logged in!");
//					client.setFileType(FTP.BINARY_FILE_TYPE);
//
//					client.changeWorkingDirectory(Constants.FTP_FOLDER_UPLOAD_ACTION_REQUEST);
//
//					File file = new File(Constants.pathRequest + fname);
//					fileName = file.getName();
//					fis = new FileInputStream(file);
//
//					// Upload file to the ftp server
//					result = client.storeFile(fileName, fis);
//
//					Log.i("path upload", file.getName());
//					Log.i("File name", fileName);
//					
//					if (result == true) {
//						System.out.println("File is uploaded successfully");
//					} else {
//						System.out.println("File uploading failed");
//					}
//					client.logout();
//					
//				} else {
//					System.out.println("Login Fail!");
//				}
//				
//			} catch (FTPConnectionClosedException e) {
//				e.printStackTrace();
//				
//			} finally {
//				try {
//					client.disconnect();
//				} catch (FTPConnectionClosedException e) {
//					System.out.println(e);
//					
//				}
//			}
//		}else{
//			error = "No internet connection.";
//			//alertDialogNeutral("Information" ,"No Internet Connection");
//		}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//	
//	
//	
//	
//	public void deleteFilecoderequest()
//	{
//		File path = Environment.getExternalStorageDirectory();
//		String fullPath = path.toString() + "/RiteCare1/Action_Request";
//        System.out.println(fullPath);
//        try
//        {
//            File file = new File(fullPath, fileName);
//            if(file.exists())
//            {
//
//                boolean result = file.delete();
//                System.out.println("Application able to delete the file and result is: " + result);
//               // file.delete();
//            }
//            else
//            {
//                System.out.println("Application doesn't able to delete the file");
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("App", "Exception while deleting file " + e.getMessage());
//        }
//	}
//	
//	
//	
//	
//	public void deleteFile()
//	{
//		File path = Environment.getExternalStorageDirectory();
//		String fullPath = path.toString() + "/RiteCare1/Users";
//        System.out.println(fullPath);
//        try
//        {
//            File file = new File(fullPath, "users.txt");
//            if(file.exists())
//            {
//
//                boolean result = file.delete();
//                System.out.println("Application able to delete the file and result is: " + result);
//               // file.delete();
//            }
//            else
//            {
//                System.out.println("Application doesn't able to delete the file");
//            }
//        }
//        catch (Exception e)
//        {
//            Log.e("App", "Exception while deleting file " + e.getMessage());
//        }
//	}
	
//	public void writeFile(){
//		String combinedString = columnString + "\n" + dataString;
//
//		File myFile = null;
//
//		Log.i("000000000000", "2222222222");
//
//		try {
//			Log.i("111111111", "SSSSSSSS");
//
//			
//			fname = fileName + ".txt";
//			myFile = new File(Constants.pathRequest, fileName + ".txt");
//			FileOutputStream mFileOutStream = new FileOutputStream(myFile);
//			mFileOutStream.write(combinedString.getBytes());
//			mFileOutStream.close();
//	}
}
