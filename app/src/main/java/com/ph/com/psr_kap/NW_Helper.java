package com.ph.com.psr_kap;
/**
 * Created by Corporate IT for United Laboratories, Inc.
 * Date Created: October 1, 2013
 * Created by: Rose Jessica Cabigao
 * 				Jess Mark Panadero
 * 				Leo Paul Marcellana
 * Short Description
 * Date updated
 **/
import android.content.Context;
import android.net.NetworkInfo;
import android.util.Log;
import android.net.ConnectivityManager;

public class NW_Helper 
{
	private static final String TAG=NW_Helper.class.getSimpleName();

	public static boolean isInternetAvailable(Context context)
	{
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

	    if (info == null)
	    {
	         Log.d(TAG,"No Network connection");
	         return false;
	    }
	    else
	    {
	        if(info.isConnected())
	        {
	            Log.d(TAG," Network connection available...");
	            return true;
	        }
	        else
	        {
	            Log.d(TAG,"no Network connection");
	            return false;
	        }

	    }
	}
}
