package com.ph.com.psr_kap;

import com.ph.com.psr_kap.utilities.constants.Constants;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class Async_Admin_Adapter extends AsyncTask<String, Integer, Boolean>{
	
	DatabaseHandler databaseHandler;
	Context context;
	private ProgressDialog progressDialog;
	String keySearch;
	String dialogMessage;
	public Async_Admin_Adapter(Context context, String searchKey, String message) {
		this.context = context;
		this.keySearch = searchKey;
		this.dialogMessage = message;
		
		databaseHandler = new DatabaseHandler(this.context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}

	}
	
	protected void onPreExecute() {

		progressDialog = new ProgressDialog(this.context);

		progressDialog.setMessage(dialogMessage);

		progressDialog.setCancelable(false);

		progressDialog.show();

	}
	
	protected Boolean doInBackground(String... params) {
		
		Constants.MDlist = databaseHandler._mdListDisplay(this.keySearch);
		
		
		return true;
	}
	

	
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
	progressDialog.dismiss();
	
		if (result) {
			AdminPage.lv_mdlist(Constants.MDlist);
//			Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
		}
		
		
	}
}


