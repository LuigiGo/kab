package com.ph.com.psr_kap;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ph.com.psr_kap.utilities.constants.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomAdapterMDlist {

	public static final String TAG = CustomAdapterMDlist.class.getSimpleName();
	static LayoutInflater lInflater;
	static DatabaseHandler databaseHandler;
	
	

	public static class AdminAdapter extends ArrayAdapter<HashMap<String, String>>
	{
//		objectsList = databaseHandler.get_MD_List(Constants.g_psrCode);
		HashMap<String, String> map = new HashMap<String, String>();
		static List<HashMap<String, String>> objectsList;
		static List<HashMap<String, String>> objects;
		static Context context;
		public static ArrayList<String> mdInMDListSeleceted;

		// String pass_code,pass_name;

		public AdminAdapter(Context context, int resource,
				List<HashMap<String, String>> objects) {
			super(context, resource, objects);
			databaseHandler = new DatabaseHandler(context);
			if (databaseHandler != null) {
				databaseHandler.close();
				databaseHandler.createDB();   
			}
			this.objects = objects;
			this.context = context;
//			unCheckAll();
			objectsList = databaseHandler.get_MD_List(Constants.g_psrCode);
			Log.i("objectlist: ", "" + objectsList.size());
			Log.i(TAG, " md list size 1: " + Constants.getMDlist.size());
			

			AdminAdapter.mdInMDListSeleceted = new ArrayList<String>();
			
			for (HashMap<String, String> hashMap : databaseHandler.get_MD_List(Constants.g_psrCode)) {
				String mdName = hashMap.get(Constants.MDLIST_NAME);
				AdminAdapter.mdInMDListSeleceted.add(mdName);
				
			}
			Log.i("jessmark", mdInMDListSeleceted.toString());

			
			lInflater = LayoutInflater.from(context);

		}

		@Override
		public int getCount() {
			return objects.size();
		}

		@Override
		public HashMap<String, String> getItem(int position) {
			return objects.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			final int pos = position;
			View view = convertView;
			
			final ViewHolder holder;
			if (view == null) {

				view = lInflater.inflate(R.layout.mdlist_item, parent, false);
				holder = new ViewHolder();
				holder.tv_name = (TextView) view.findViewById(R.id.tvName);
				holder.chk = (CheckBox) view.findViewById(R.id.chkbox_mdlist);

				
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			String mdCode = this.objects.get(pos).get(Constants.MD_CODE);
			String name = this.objects.get(pos).get(Constants.MD_NAME);
			holder.tv_name.setText(name);

			holder.chk.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					
					
					String code = objects.get(pos).get(Constants.MD_CODE);
					String name = objects.get(pos).get(Constants.MD_NAME);
					
					HashMap<String, String> map = new HashMap<String, String>();
					
					map.put(Constants.MD_CODE, code);
					map.put(Constants.MD_NAME, name);

//					if (holder.chk.isChecked()) {
//						Constants.checkPosition.add(String.valueOf(pos));
//						Constants.getMDlist.add(map);
//						Log.i("add", "pos: " + name);
//					} else {
//						Constants.checkPosition.remove(String.valueOf(pos));
//						Constants.getMDlist.remove(map);
//						Log.i("remove", "pos: " + name);
//					}
					
					//edited by Jess
					if (holder.chk.isChecked()) {
						if (!Constants.checkPosition.contains(name)) {
							Constants.checkPosition.add(name);
						}
						
						if (!Constants.getMDlist.contains(map)) {
							Constants.getMDlist.add(map);
						}
						
						Constants.tempRemove.remove(name);
						
						
						Log.i("add", "Constants.checkPosition: " + Constants.checkPosition);
						Log.i("add", Constants.getMDlist.toString());
					} else {
						
						
						Constants.checkPosition.remove(name);
						Constants.getMDlist.remove(map);
						
						if (!Constants.tempRemove.contains(name)) {
							Constants.tempRemove.add(name);
						}
						
						Log.i("remove", "Constants.checkPosition: " + Constants.checkPosition);
						Log.i("remove", Constants.getMDlist.toString());
					}
				}
			});

			holder.chk.setTag(position);
			
//			if (Constants.checkPosition.contains(String.valueOf(pos))) {
//				holder.chk.setChecked(true);
//				
//			} else {
//				holder.chk.setChecked(false);
//			}
			
//			//put the selectedMdList in checkPosition Tag
//			if (databaseHandler.checkIfInMDLIST(mdCode) ) {
//				Constants.checkPosition.add(mdCode);
//			}else{
//				Constants.checkPosition.remove(mdCode);
//			}
			
			
			//edited by jess
			if (Constants.checkPosition.contains(name)) {
				holder.chk.setChecked(true);
				
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put(Constants.MD_CODE, mdCode);
				map.put(Constants.MD_NAME, name);
				
				
				if (mdInMDListSeleceted.contains(name) && !Constants.tempRemove.contains(name)) {

					if (!Constants.getMDlist.contains(map)) {
						Constants.getMDlist.add(map);
					}

					holder.chk.setChecked(true);
				} else {

					Constants.getMDlist.remove(map);
					holder.chk.setChecked(false);
				}
				
				
			}
			return view;
		}

		public void unCheckAll() {
			map.clear();
//			Constants.checkPosition.clear();
			Constants.getMDlist.clear();
			notifyDataSetChanged();
		}
		
		

	}

}

class ViewHolder {

	TextView tv_name;
	CheckBox chk;
	TextView tvNoData;

}

// // if check all
// if (Constants.ismarkAllMD) {
//
// Log.i("isMarkALL", "asdasdasdas");
// cbBuy.setChecked(true);
//
// code = objects.get(pos).get(Constants.MD_CODE);
// name = objects.get(pos).get(Constants.MD_NAME);
// // if (Constants.checkPosition.contains(code)) {
// // for(int k=0;k<Constants.MDlist.size();k++){
// // pass_code =
// objects.get(pos).get(Constants.MDlist.get(k).get(Constants.MD_CODE));
// // pass_name =
// objects.get(pos).get(Constants.MDlist.get(k).get(Constants.MD_NAME));
//
// map.put(Constants.MD_CODE, code);
// map.put(Constants.MD_NAME, name);
//
// Constants.checkPosition.add(code);
// Constants.getMDlist.add(map);
// // }
// Log.i("add", "pos: " + Constants.getMDlist.toString());
// }
// // else {
// // cbBuy.setChecked(false);
// // Constants.checkPosition.remove(md_code_pass);
// // Constants.getMDlist.remove(map);
// // }
//
// //}
// else {
// Log.i("isunMarkALL", "asdasdasdas");
// cbBuy.setChecked(false);
//
// for(int k=0;k<Constants.MDlist.size();k++){
// code = objects.get(pos).get(Constants.MDlist.get(k).get(Constants.MD_CODE));
// name = objects.get(pos).get(Constants.MDlist.get(k).get(Constants.MD_NAME));
//
// map.put(Constants.MD_CODE, code);
// map.put(Constants.MD_NAME, name);
//
// Constants.checkPosition.remove(code);
// Constants.getMDlist.remove(map);
// }
// Log.i("add", "pos: " + Constants.getMDlist.toString());
// }