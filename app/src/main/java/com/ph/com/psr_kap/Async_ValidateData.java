package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class Async_ValidateData extends AsyncTask<Void, Void, Cursor> {
	private ProgressDialog progressDialog;
	private String username, password;
	DatabaseHandler databaseHandler;
	Context context;
//	public static SharedPreferences sharedPreferences;
//	public static SharedPreferences.Editor sharedPreferencesEditor;
	public SharedPreferences sharedPreference;

	
	public Async_ValidateData(Context context, String username, String password)
	{
		Log.i(Constants.TAG + 2, username);
		Log.i(Constants.TAG + 2, password);
		this.username = username;
		this.password = password;
		this.context = context;
		sharedPreference = context.getSharedPreferences(Constants.SHARED_PREFERENCES_CREATOR, 0);
		databaseHandler = new DatabaseHandler(this.context);
		
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}
	}

	protected void onPreExecute() {
		progressDialog = new ProgressDialog(this.context);
		progressDialog.setMessage("Validating....");
		progressDialog.show();
		progressDialog.setCancelable(false);
	}

	@Override
	protected Cursor doInBackground(Void... params) {
		Log.i(Constants.TAG + 3, username);
		Log.i(Constants.TAG + 3, password);
		return databaseHandler.getCursorUsernamePassword(username,
				password);

	}

	@Override
	protected void onProgressUpdate(Void... unused) {

	}

	protected void onPostExecute(Cursor cursor) {
		Log.i("onPostExecute", "onPostExecute");
		progressDialog.dismiss();
		if (cursor != null) {
			if (cursor.getCount() != 0) {
				Cursor user_cursor = databaseHandler
						.get_PSR_Code(this.username);
				String psr_code = user_cursor.getString(user_cursor
						.getColumnIndex(Constants.PSR_CODE));
				Constants.g_psrCode = psr_code;
//				sharedPreferencesEditor.putString(Constants.SP_PSR_CODE,
//						psr_code);
//				sharedPreferencesEditor.commit();
				Editor sEdit = sharedPreference.edit();
				sEdit.putString(Constants.SP_PSR_CODE, psr_code);
				sEdit.commit();
				Log.i(Constants.TAG, "Successful");
				
				
				if (NetworkHelper.isInternetAvailable(context)) {
					
					
					
					if (databaseHandler.isInitialUpdate()) {
						Constants.isOnline = true;
						FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
						brand.execute();
						
						
					}else{
						
						context.startActivity(new Intent(this.context, RegistrationPage.class));
						
						((Activity) context).finish();
					}
					
				}else{
					context.startActivity(new Intent(this.context, RegistrationPage.class));
					
					((Activity) context).finish();
				}
				
				
			} else {
//				Toast.makeText(this.context,
//						"Account does not exist. Please contact CRM group for assistance. (02)858-1000 loc. 7463.",
//						Toast.LENGTH_LONG).show();
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				alertDialogBuilder.setTitle("Login Error");
				alertDialogBuilder
						.setMessage(
								"Account does not exist. Please contact CRM group for assistance. (02)858-1000 loc. 7463.")
						.setCancelable(false)
						.setNeutralButton(
								"OK",
								new DialogInterface.OnClickListener() {
									public void onClick(
											DialogInterface dialog,
											int id) {
									}
								});

				AlertDialog alertDialog = alertDialogBuilder
						.create();
				alertDialog.show();
			}
		} else {
			Toast.makeText(this.context,
					"Account does not exist. Please contact CRM group for assistance. (02)858-1000 loc. 7463.",
					Toast.LENGTH_LONG).show();
		}

	}
}

