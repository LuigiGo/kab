package com.ph.com.psr_kap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class Async_FTP_BrandProducts_Save extends
		AsyncTask<String, Integer, Boolean> {

	ProgressDialog progressDialog;
	Context context;

	DatabaseHandler databaseHandler;
	int totalNoOfItemsToBeUpdated, percentage;
	double itemsUpdated;
	ArrayList<String> listDeliveryNo;

	ArrayList<String> listBrand_Name;
	ArrayList<String> listBrand_Code;
	ArrayList<String> listKeyword;

	// ====================
	Calendar calendar;
	String fileName, fname;

	AlertDialog alertDialog;

	public Async_FTP_BrandProducts_Save(Context context) {
		this.context = context;

		listDeliveryNo = new ArrayList<String>();

		listBrand_Name = new ArrayList<String>();
		listBrand_Code = new ArrayList<String>();
		listKeyword = new ArrayList<String>();

		calendar = Calendar.getInstance();

		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}
	}

	protected void onPreExecute() {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Updating products. Please wait...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub

		String CSVBrandProducts = Environment.getExternalStorageDirectory()
				.toString()
				+ "/Kabalikat_Advocacy_Program_2015/Products/Products.csv";

		Log.i("CSVBrandProducts--------------", CSVBrandProducts);

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		// READ CSV Members
		try {
			Log.i("try", "try");
			br = new BufferedReader(new FileReader(CSVBrandProducts));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] ProductDetails = line.split(cvsSplitBy);

				listBrand_Name.add(ProductDetails[0]);
				Log.i("ProductDetails[0]", ProductDetails[0]);// BrandName
				Log.i("ProductDetails[1]", ProductDetails[1]);// Keyword
				Log.i("ProductDetails[2]", ProductDetails[2]);// BrandCode

				listKeyword.add(ProductDetails[1]);
				listBrand_Code.add(ProductDetails[2]);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		totalNoOfItemsToBeUpdated = listBrand_Name.size();

		Log.i("totalNoOfItemsToBeUpdated--------------", ""
				+ totalNoOfItemsToBeUpdated);
		//
		databaseHandler.deleteTableBrand();
		
		for (int i = 0; i < listBrand_Name.size(); i++) {
			if (i >= 1) {

				String brandName = listBrand_Name.get(i);
				String keyword = listKeyword.get(i);
				String brandCode = listBrand_Code.get(i);
				ContentValues values = new ContentValues();

				values.put(Constants.BRAND_CODE, brandCode);
				values.put(Constants.BRAND_NAME, brandName);
				values.put(Constants.BRAND_KEYWORD, keyword);

				databaseHandler.updateBrandTable(values);

				itemsUpdated++;
				percentage = (int) ((itemsUpdated / totalNoOfItemsToBeUpdated) * 100);
				updateProgress(percentage);

				Log.i("updateBrandTable-------------------", "updateBrandTable");

			}

		}

		return true;
	}

	private void updateProgress(Integer value) {
		if (progressDialog != null && progressDialog.isShowing() == true) {
			progressDialog.setProgress(value);
			Log.i("Percent",
					String.valueOf(value) + " " + String.valueOf(itemsUpdated)
							+ " " + String.valueOf(totalNoOfItemsToBeUpdated));
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progressDialog.dismiss();

		// if (result) {
		// Log.i("RESULT VALID---------------", "RESULT VALID---------------");
		// }

		if (NW_Helper.isInternetAvailable(context)) {
			if (result == true) {
				System.out.println("Done Updating BrandProducts data.");
				Log.i("Done Updating BrandProducts data.",
						"Done Updating BrandProducts data.");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				// set title
				alertDialogBuilder.setTitle("Information");
				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);

				// set dialog message
				alertDialogBuilder
						.setMessage("Products have been successfully updated.");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setNeutralButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent refresh = new Intent(context,
										RegistrationPage.class);
								context.startActivity(refresh);
								((Activity) context).finish();
								dialog.cancel();
							}
						});
				alertDialog = alertDialogBuilder.create();
				alertDialog.show();

			} else {
				System.out
						.println("File not updating Async_FTP_BrandProducts_Save, RETRY");
				Log.i("File not updating Async_FTP_BrandProducts_Save",
						"File not updating Async_FTP_BrandProducts_Save, RETRY");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				// set title
				alertDialogBuilder.setTitle("Server Error");
				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);

				// set dialog message
				alertDialogBuilder
						.setMessage("There was a problem updating the data. Would you like to try again.");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setNeutralButton("Update",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
//								Async_FTP_BrandProducts_Save a = new Async_FTP_BrandProducts_Save(
//										context);
//								a.execute();
								 Async_FTP_Download_BrandProduct a2 = new Async_FTP_Download_BrandProduct(context);
								 a2.execute();
								dialog.cancel();
							}
						});
				alertDialogBuilder.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								dialog.cancel();
							}
						});

				alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}
		} else {
			Toast.makeText(context, "Please check your internet connection.",
					Toast.LENGTH_LONG).show();
		}
	}

}