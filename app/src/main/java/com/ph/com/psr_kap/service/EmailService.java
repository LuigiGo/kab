package com.ph.com.psr_kap.service;

import java.io.File;
import java.util.Calendar;










import com.ph.com.psr_kap.Login;
import com.ph.com.psr_kap.R;
import com.ph.com.psr_kap.SplashScreen;
import com.ph.com.psr_kap.email.EmailHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class EmailService extends Service{
	
	AlarmManager alarmManager;
	PendingIntent pendingIntent;
	
	Calendar calendar = Calendar.getInstance();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
    public void onCreate() 
    {
       // TODO Auto-generated method stub  
       super.onCreate();
       alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    }
 
   @SuppressWarnings("static-access")
   @Override
   public void onStart(Intent intent, int startId)
   {
       super.onStart(intent, startId);
      
       
    }
   
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
//		Toast.makeText(getApplicationContext(), "Service onStartCommand", Toast.LENGTH_LONG).show();
		
		schedule1(getApplicationContext());
		schedule2(getApplicationContext());

		
		return START_NOT_STICKY;
//        return super.onStartCommand(intent,flags,startId);
	}
   
   
 
    @Override
    public void onDestroy() 
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
    
    public void schedule1(Context context) {
		
		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.DAY_OF_MONTH, 1);
		calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
		
		Intent myIntent = new Intent(context, AutoEmailReceiver.class);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent,PendingIntent.FLAG_ONE_SHOT);

		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

//		alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
		        AlarmManager.INTERVAL_DAY, pendingIntent);
		Log.d("MyActivity", "Set alarmManager.set to: " + calendar.getTime().toLocaleString());
	}
    
    public void schedule2(Context context) {
		
		Calendar calendar = Calendar.getInstance();
//		calendar.add(Calendar.DAY_OF_MONTH, 1);
		calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
		
		Intent myIntent = new Intent(context, AutoEmailReceiver.class);

		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1, myIntent,PendingIntent.FLAG_ONE_SHOT);

		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

//		alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);	
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
		        AlarmManager.INTERVAL_DAY, pendingIntent);
		Log.d("MyActivity", "Set alarmManager.set to: " + calendar.getTime().toLocaleString());
	}
    
    

}
