/**Created by Corporate IT for United Laboratories, Inc.
 * Created by	: Rose Jessica M. Cabigao
 * Date Created	: January 03, 2013
 * Date Updated	: January 03, 2013
 * Description	: For PSR Registration
 */
package com.ph.com.psr_kap;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class DatabaseHandler extends SQLiteOpenHelper {
	// declare static variables
	static final String TAG = "Unilab_PSR_Registration";
	public final String DATABASE_PATH = Environment.getDataDirectory()
			+ "/data/com.ph.com.psr_kap/databases/";
	public final String DATABASE_PATH2 = Environment
			.getExternalStorageDirectory() + "/";
	public static final String DATABASE_NAME = "db_PSR.sqlite";

	private static final int DATABASE_VERSION = 2;

	public SQLiteDatabase dbSqlite = null;
	private final Context myContext;

	// set the context of a database
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.myContext = context;
	}

	// create database
	public void createDB() {
		boolean dbExists = DBExists();
		// checks if database exist, if not create and make a copy of database
		if (dbExists) {
		} else {
			dbSqlite = this.getWritableDatabase();
			dbSqlite.close();

			try {
				copyDBFromResource();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		openDataBase();
	}

	public void copyDBFromResource2() throws IOException {
		InputStream inputStream = null;
		OutputStream outStream = null;

		String dbFilePath = DATABASE_PATH2 + DATABASE_NAME;
		try {
			// inputStream = myContext.getAssets().open(DATABASE_NAME);
			inputStream = new FileInputStream(DATABASE_PATH + DATABASE_NAME);
			// inputStream = DATABASE_PATH + DATABASE_NAME;
			outStream = new FileOutputStream(dbFilePath);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();
		} catch (IOException e) {
			// e.printStackTrace();
			throw new Error("Problem copying database from resource file.");
		}
	}

	// checks if database exist in the database path
	private boolean DBExists() {
		SQLiteDatabase db = null;
		try {
			String databasePath = DATABASE_PATH + DATABASE_NAME;
			db = SQLiteDatabase.openDatabase(databasePath, null,
					SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		} catch (SQLiteException e) {
			Log.e("SqlHelper", "Database not found");
		}
		if (db != null) {
			db.close();
			Log.i(TAG, "Database Exists");
		}
		return db != null ? true : false;
	}

	// makes a copy of the database and place it on the database path
	private void copyDBFromResource() throws IOException {
		InputStream inputStream = null;
		OutputStream outStream = null;

		String dbFilePath = DATABASE_PATH + DATABASE_NAME;
		try {
			inputStream = myContext.getAssets().open(DATABASE_NAME);
			outStream = new FileOutputStream(dbFilePath);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) > 0) {
				outStream.write(buffer, 0, length);
			}
			outStream.flush();
			outStream.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Error("Problem copying database from resource file.");
		}
	}

	// open the database from the database path
	public DatabaseHandler openDataBase() throws SQLException {
		try {
			if (dbSqlite.isOpen()) {
				dbSqlite.close();
			}
			String myPath = DATABASE_PATH + DATABASE_NAME;
			dbSqlite = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	// close the database
	public synchronized void close() {
		if (dbSqlite != null)
			dbSqlite.close();
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// do nothing
	}

	@Override
	// makes upgrade if the database has new Version number
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("SqlHelper", "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		onCreate(db);
	}

	/**
	 * 
	 * 
	 * //============= Q U E R I E S ===============================//
	 * 
	 */

	// Signing in
	public Cursor getCursorUsernamePassword(String username, String password) {
		Log.i("Username", username);
		Log.i("Password", password);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(Constants.DATABASE_TABLE_PSR, new String[] {
				Constants.COLUMN_ID, Constants.PSR_CODE, Constants.PSR_NAME,
				Constants.PSR_USERNAME, Constants.PSR_PASSWORD },
				Constants.PSR_USERNAME + " = ? AND " + Constants.PSR_PASSWORD
						+ " = ?", new String[] { username, password }, null,
				null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	
	public ArrayList<PSRhandler> getPSR() {
		String query = "SELECT * FROM tbl_PSR";
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.rawQuery(query, null);
		ArrayList<PSRhandler> data = new ArrayList<PSRhandler>();
		if (cursor.getCount() > 0) {
			
			for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
//				int id = cursor.getInt(cursor.getColumnIndex("rowid"));
				String psr_code = cursor.getString(cursor.getColumnIndex("PSR_Code"));
				String list_psr_name = cursor.getString(cursor.getColumnIndex("PSR_Name"));
				PSRhandler item = new PSRhandler();
//				item.setId(id);
				item.setPsr_code(psr_code);
				item.setPsr_name(list_psr_name);
				data.add(item);
			}
			
		}

		cursor.close();
		db.close();

		return data;
	}
	

	public List<String> setItemOnBrandSpinner() {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<String> Brand = new ArrayList<String>();
		Cursor cursor = db.query(Constants.DATABASE_TABLE_BRANDS,
				new String[] { Constants.BRAND_NAME }, null, null, null, null,
				null);
		Brand.add("Select All");

		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			Brand.add(cursor.getString(0));

		}
		cursor.close();
		db.close();
		return Brand;
	}

	public Cursor _searchName(String _filter,String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		Cursor cursor = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected, new String[] {Constants.MDLIST_CODE, Constants.MDLIST_NAME},
				Constants.MDLIST_NAME + " like ? AND " + Constants.MDLIST_PSR_CODE + " =? ",
				new String[]{"%" + _filter + "%",psr_code}, null, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}
		dbSqlite.close();
		return cursor;
	}
	
	


public Cursor _searchName(String _filter) {
		
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		Cursor cursor = dbSqlite.query(Constants.DATABASE_TABLE_MD, new String[] {Constants.MD_CODE, Constants.MD_NAME},
				Constants.MD_NAME + " like ?",
				new String[]{"%" + _filter + "%"}, Constants.MD_NAME, null, null);
		
		if (cursor != null) {
			cursor.moveToFirst();
		}
		dbSqlite.close();
		return cursor;
	}
	
	
	public Cursor GetMDTemp() {

		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		Cursor cursor = dbSqlite.query(Constants.TBL_MDTEMP,
				new String[] { Constants.MD_CODE, Constants.MD_NAME }, null,
				null, null, null, null);

		if (cursor != null) {
			cursor.moveToFirst();
		}
		dbSqlite.close();
		return cursor;
	}

	
	
	

	public Cursor get_PSR_Code(String username) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_PSR, new String[] {
				Constants.COLUMN_ID, Constants.PSR_CODE, Constants.PSR_NAME,
				Constants.PSR_USERNAME, Constants.PSR_PASSWORD },
				Constants.PSR_USERNAME + "=?", new String[] { username }, null,
				null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}

	public Cursor get_brand_Code(String brandname) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_BRANDS,
				new String[] { Constants.BRAND_CODE, Constants.BRAND_NAME,
						Constants.BRAND_KEYWORD }, Constants.BRAND_NAME + "=?",
				new String[] { brandname }, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}

	public Cursor getKeyword(String brandcode) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_BRANDS,
				new String[] { Constants.BRAND_CODE, Constants.BRAND_NAME,
						Constants.BRAND_KEYWORD }, Constants.BRAND_CODE + "=?",
				new String[] { brandcode }, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}

	public Cursor get_PSR_details(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_PSR, new String[] {
				Constants.COLUMN_ID, Constants.PSR_CODE, Constants.PSR_NAME,
				Constants.PSR_USERNAME, Constants.PSR_PASSWORD },
				Constants.PSR_CODE + "=?", new String[] { psr_code }, null,
				null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}

	public String get_psr_name(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		String name = "";
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_PSR, new String[] {
				Constants.COLUMN_ID, Constants.PSR_CODE, Constants.PSR_NAME,
				Constants.PSR_USERNAME, Constants.PSR_PASSWORD },
				Constants.PSR_CODE + "=?", new String[] { psr_code }, null,
				null, null);
		if (c != null) {
			if (c.getCount() > 0) {
				c.moveToFirst();
				name = c.getString(c.getColumnIndex(Constants.PSR_NAME));
			}
			
		}
		dbSqlite.close();
		return name;
	}

	public String get_md_name(String md_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		String name = "";
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_MD, new String[] {
				Constants.COLUMN_ID2, Constants.MD_CODE, Constants.MD_NAME },
				Constants.MD_CODE + "=?", new String[] { md_code }, null, null,
				null);
		if (c != null) {
		 if(c.getCount() > 0){
			 c.moveToFirst();
			 name = c.getString(c.getColumnIndex(Constants.MD_NAME));
		 }
			
			
		}
		dbSqlite.close();
		return name;
	}

	public Cursor get_md(String md_name) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		Cursor c;
		if(Constants.choosefilterQueryName == false){
			c = dbSqlite.query(Constants.DATABASE_TABLE_MD, new String[] {
					Constants.COLUMN_ID2, Constants.MD_CODE, Constants.MD_NAME },
					Constants.MD_NAME + " =?", new String[] { md_name }, null, null,
					null);
			if (c != null) {
				c.moveToFirst();
			}
		}
		else{
			c = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected, new String[] {Constants.COLUMN_ID2,
					Constants.MDLIST_CODE, 
					Constants.MDLIST_NAME},
					Constants.MDLIST_NAME + " =?", new String[] {md_name},
					null, null, null);
			if (c != null) {
				c.moveToFirst();
			}
		}
		
		dbSqlite.close();
		return c;
	}

	public Cursor getCodeCount(String queryCount) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(queryCount, null);
		if (cursor != null)
			;
		cursor.moveToFirst();

		db.close();
		return cursor;
	}

	public Cursor getLastRecordId(String query) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		if (cursor != null)
			;
		cursor.moveToFirst();

		db.close();
		return cursor;
	}

	public void SaveBrand(String patient_code, String brand_code,
			String coupon_code) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put(Constants.PATIENT_CODE, patient_code);
		contentValues.put(Constants.BRAND_CODE, brand_code);
		contentValues.put(Constants.COUPON_CODE, coupon_code);

		db.insert(Constants.DATABASE_TABLE_BRANDTRANSACTION, null,
				contentValues);
		db.close();

	}

	public void SaveMD(String md_code, String md_name) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		// contentValues.put(Constants.PSR_CODE, psr_code);
		contentValues.put(Constants.MD_CODE, md_code);
		contentValues.put(Constants.MD_NAME, md_name);

		db.insert(Constants.DATABASE_TABLE_MD, null, contentValues);
		db.close();

	}
	
	
	public void SaveMD_Temp(String md_code, String md_name) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		// contentValues.put(Constants.PSR_CODE, psr_code);
		contentValues.put(Constants.MD_CODE, md_code);
		contentValues.put(Constants.MD_NAME, md_name);

		db.insert(Constants.TBL_MDTEMP, null, contentValues);
		db.close();

	}
	

	public void SavePatient(String patient_code, String patient_fname,
			String patient_lname, String patient_mobile,
			String patient_landline, String psr_code, String md_code,
			String timeStamp) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put(Constants.PATIENT_CODE, patient_code);
		contentValues.put(Constants.PATIENT_FNAME, patient_fname);
		contentValues.put(Constants.PATIENT_LNAME, patient_lname);
		contentValues.put(Constants.PATIENT_MOBILE, patient_mobile);
		contentValues.put(Constants.PATIENT_LANDLINE, patient_landline);
		contentValues.put(Constants.PSR_CODE, psr_code);
		contentValues.put(Constants.MD_CODE, md_code);
		contentValues.put(Constants.PATIENT_REGDATE, timeStamp);
		contentValues.put(Constants.PATIENT_EXPORT, "No");

		db.insert(Constants.DATABASE_TABLE_PATIENTS, null, contentValues);
		db.close();
		
		Log.i("Save: ", "SAVE!!!!!");

	}

	public Cursor getPendingDetails(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null,Constants.VW_PATIENT_PSRCODE + " =? AND " + Constants.VW_PATIENT_EXPORTED + "=?", new String[] {psr_code, "No" }, null,
				null, Constants.COLUMN_ID + " DESC");
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;

	}

	public Cursor getPendingDetails(String _filter,String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null,
				"(" + Constants.VW_PATIENT_FIRSTNAME + " like \"" + _filter
						+ "%\" OR " + Constants.VW_PATIENT_LASTNAME + " like \""
						+ _filter + "%\") AND "  + Constants.VW_PATIENT_PSRCODE + " =?" + " AND " + Constants.VW_PATIENT_EXPORTED
						+ " = " + "'No'", new String[] {psr_code}, null, null, Constants.COLUMN_ID
						+ " DESC");

		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}

	public Cursor get_brands_prescribed(String patient_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_BRANDTRANSACTION,
				new String[] { Constants.COLUMN_ID, Constants.PATIENT_CODE,
						Constants.BRAND_CODE, Constants.COUPON_CODE },
				Constants.PATIENT_CODE + "=?", new String[] { patient_code },
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
//		dbSqlite.close();
		return c;
	}

	public String get_brand_name(String brand_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		String name = "";
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_BRANDS,
				new String[] { Constants.BRAND_CODE, Constants.BRAND_NAME,
						Constants.BRAND_KEYWORD }, Constants.BRAND_CODE + "=?",
				new String[] { brand_code }, null, null, null);
		if (c != null) {
			if(c.getCount() > 0){
				c.moveToFirst();
				name = c.getString(c.getColumnIndex(Constants.BRAND_NAME));
			}
			
		}
		dbSqlite.close();
		return name;
	}

	public String get_brand_id(String brand_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		String id = "";
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_BRANDS,
				new String[] { Constants.COLUMN_ID, Constants.BRAND_CODE,
						Constants.BRAND_NAME, Constants.BRAND_KEYWORD },
				Constants.BRAND_CODE + "=?", new String[] { brand_code }, null,
				null, null);
		if (c != null) {
			if(c.getCount() > 0){
				c.moveToFirst();
				id = c.getString(c.getColumnIndex(Constants.COLUMN_ID));
			}
			
		}
		dbSqlite.close();
		return id;
	}

	public Cursor getCursorFilterDateFromAndTo(String dateFrom, String dateTo,String psr_code) {
		String datefrom = null, dateto = null;
		datefrom = new StringBuilder().append(dateFrom).append(" 00:00.00")
				.toString();
		dateto = new StringBuilder().append(dateTo).append(" 24:00.00")
				.toString();
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_DATEREG
						+ " BETWEEN ? AND ? AND " + Constants.VW_PATIENT_PSRCODE + " =? AND "
						+ Constants.VW_PATIENT_EXPORTED
						+ " =? ", new String[] { datefrom, dateto,psr_code, "No" },
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		
		dbSqlite.close();
		return c;
	}

	@SuppressWarnings("deprecation")
	public Cursor getCursorFilterDateFrom(String dateFrom,String psr_code) {
		String datefrom = null, dateto = null;
		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new Timestamp(calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DATE),
				calendar.get(Calendar.YEAR) - 1900,
				calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE),
				calendar.get(Calendar.SECOND), 0);

		datefrom = new StringBuilder().append(dateFrom).append(" 00:00.00")
				.toString();
		dateto = timeStamp.toString();
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_DATEREG
						+ " BETWEEN ? AND ? AND " + Constants.VW_PATIENT_PSRCODE + " =? AND " + Constants.VW_PATIENT_EXPORTED
						+ " =? ", new String[] { datefrom, dateto,psr_code, "No" },
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		
		dbSqlite.close();
		return c;
	}

	public Cursor getCursorFilterDateTo(String dateTo,String psr_code) {
		String datefrom = null, dateto = null;
		datefrom = new StringBuilder().append("1980-01-01").append(" 00:00.00")
				.toString();
		dateto = new StringBuilder().append(dateTo).append(" 24:00.00")
				.toString();
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_DATEREG
						+ " BETWEEN ? AND ? AND " + Constants.VW_PATIENT_PSRCODE + " =? AND "
						+ Constants.VW_PATIENT_EXPORTED
						+ " =? ", new String[] { datefrom, dateto,psr_code, "No" },
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		
		dbSqlite.close();
		return c;
	}

	public Cursor getAllDetails(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_PSRCODE + " =?", new String[] {psr_code}, null, null,
				null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;

	}
	
	public Cursor getAllUnexprotedDetails(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_PSRCODE + " =? AND " + Constants.VW_PATIENT_EXPORTED + " =? ", new String[] {psr_code,"No"}, null, null,
				null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;

	}

	public Cursor getAllDetails(String _filter,String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.VW_TABLE_PATIENT,
				null, Constants.VW_PATIENT_FIRSTNAME
						+ " like \"" + _filter + "%\" OR "
						+ Constants.VW_PATIENT_LASTNAME + " like \"" + _filter
						+ "%\"" + " AND " + Constants.VW_PATIENT_PSRCODE + " =?", new String[] {psr_code}, null, null, null);

		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;

	}

	public ArrayList<String> getAllRegistration_Id() {
		SQLiteDatabase db = this.getReadableDatabase();
		ArrayList<String> all_id = new ArrayList<String>();
		Cursor c = db.query(Constants.DATABASE_TABLE_PATIENTS,
				new String[] { Constants.COLUMN_ID }, null, null, null, null,
				null, null);
		if (c.moveToFirst()) {
			do {
				all_id.add(c.getString(0));
			} while (c.moveToNext());
		}
		Log.i("jessmark", all_id.toString());
		return all_id;
	}

//	public Cursor getCursorDetailsById(String cons_id) {
//		SQLiteDatabase db = this.getReadableDatabase();
//		Cursor c = db.query(Constants.DATABASE_TABLE_PATIENTS, new String[] {
//				Constants.COLUMN_ID, Constants.PATIENT_CODE,
//				Constants.PATIENT_FNAME, Constants.PATIENT_LNAME,
//				Constants.PATIENT_MOBILE, Constants.PATIENT_LANDLINE,
//				Constants.PSR_CODE, Constants.PATIENT_MD,
//				Constants.PATIENT_REGDATE, Constants.PATIENT_EXPORT },
//				Constants.COLUMN_ID + "=?", new String[] { cons_id }, null,
//				null, null);
//		if (c != null) {
//			c.moveToFirst();
//		}
//		db.close();
//		return c;
// 
//	}
	
	public Cursor getRegisteredPatientDetails(String psrCode) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(Constants.VW_TABLE_PATIENT, null,
				Constants.VW_PATIENT_PSRCODE + "=?", new String[] { psrCode }, null,
				null, null);
		if (c != null) {
			c.moveToFirst();
		}
		db.close();
		return c;
 
	}

	public void updateExportedmark(String consumerId) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put(Constants.PATIENT_EXPORT, "Yes");
		db.update(Constants.DATABASE_TABLE_PATIENTS, contentValues,
				Constants.COLUMN_ID + "=?", new String[] { consumerId });
		db.close();
	}

	public List<String> get_coupon_code(String patient_code) {
		String selectquery = "Select * from "
				+ Constants.DATABASE_TABLE_BRANDTRANSACTION + " WHERE "
				+ Constants.PATIENT_CODE + " = '" + patient_code + "'";
		List<String> _couponcode = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectquery, null);
		if (cursor.moveToFirst()) {
			do {
				_couponcode.add(cursor.getString(cursor
						.getColumnIndex(Constants.COUPON_CODE)));
			} while (cursor.moveToNext());
		}
		db.close();
		return _couponcode;
	}

	public void deleteRecord(String ID) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Constants.DATABASE_TABLE_PATIENTS,
				Constants.COLUMN_ID + "=?", new String[] { ID });
		db.close();

	}

	public void deleteBrand(String patientcode) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Constants.DATABASE_TABLE_BRANDTRANSACTION,
				Constants.PATIENT_CODE + "=?", new String[] { patientcode });
		db.close();

	}

	public Cursor getDetailstoUpdate(String _id) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();

		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_PATIENTS,
				new String[] { Constants.COLUMN_ID, Constants.PATIENT_CODE,
						Constants.PATIENT_FNAME, Constants.PATIENT_LNAME,
						Constants.PATIENT_MOBILE, Constants.PATIENT_LANDLINE,
						Constants.PSR_CODE, Constants.PATIENT_MD,
						Constants.PATIENT_REGDATE, Constants.PATIENT_EXPORT },
				Constants.COLUMN_ID + "=?", new String[] { _id }, null, null,
				null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;

	}

	public void updateDetails(String patient_code, String patient_fname,
			String patient_lname, String patient_mobile,
			String patient_landline, String psr_code, String md_code,
			String timeStamp) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put(Constants.PATIENT_FNAME, patient_fname);
		contentValues.put(Constants.PATIENT_LNAME, patient_lname);
		contentValues.put(Constants.PATIENT_MOBILE, patient_mobile);
		contentValues.put(Constants.PATIENT_LANDLINE, patient_landline);
		contentValues.put(Constants.PSR_CODE, psr_code);
		contentValues.put(Constants.PATIENT_MD, md_code);
		contentValues.put(Constants.PATIENT_UPDATE_DATE, timeStamp);
		db.update(Constants.DATABASE_TABLE_PATIENTS, contentValues,
				Constants.PATIENT_CODE + "=?", new String[] { patient_code });
		db.close();
	}

	// REX==================================================
	public void deleteTablePSR() {
		SQLiteDatabase db = this.getWritableDatabase();

		String sql = "DELETE FROM tbl_PSR";

		Log.i("DELETE SQL", sql);

		db.execSQL(sql);
		db.close();
	}

	public void deleteTableMD() {
		SQLiteDatabase db = this.getWritableDatabase();

		String sql = "DELETE FROM tbl_MD";

		Log.i("DELETE SQL", sql);

		db.execSQL(sql);
		db.close();
	}

	public void deleteTableBrand() {
		SQLiteDatabase db = this.getWritableDatabase();

		String sql = "DELETE FROM tbl_Brand";

		Log.i("DELETE SQL", sql);

		db.execSQL(sql);
		db.close();
	}

	public void updatePSRTable(ContentValues contentValues) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(Constants.DATABASE_TABLE_PSR, null, contentValues);

		db.close();

	}

	public void updateMDTable(ContentValues contentValues) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(Constants.DATABASE_TABLE_MD, null, contentValues);

		db.close();

	}

	public void updateBrandTable(ContentValues contentValues) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.insert(Constants.DATABASE_TABLE_BRANDS, null, contentValues);

		db.close();

	}

	public boolean getAccount(String username, String password) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(Constants.DATABASE_TABLE_PSR, null,
				Constants.PSR_USERNAME + "=? AND " + Constants.PSR_PASSWORD
						+ "=?", new String[] { username, password }, null,
				null, null);
		if (cursor.getCount() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Check if PSR is already exists
	public boolean check_PSR_if_Exist(String psr) {

		Log.d("DB - check_PSR_if_Exist", psr);

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(Constants.DATABASE_TABLE_PSR,
				new String[] { Constants.PSR_USERNAME }, Constants.PSR_USERNAME
						+ "=?", new String[] { psr }, null, null, null, null);

		if (cursor.moveToFirst()) {
			return true; // row exists
		} else {
			return false;

		}

	}

	// FAYE

	public ArrayList<HashMap<String, String>> _mdListDisplay(String name) {
		ArrayList<HashMap<String, String>> listMD = new ArrayList<HashMap<String, String>>();
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
//		String query = "SELECT tbl_MD.*, tbl_MDlistSelected.* FROM "
//				+ Constants.DATABASE_TABLE_MD + 
//				" LEFT JOIN " + Constants.DATABASE_TABLE_MDlistSelected +
//				" ON  tbl_MD.MD_Code = tbl_MDlistSelected.MDLIST_CODE AND tbl_MDlistSelected.MDLIST_PSR_CODE " + Constants.g_psrCode + " WHERE tbl_MD.MD_Name LIKE +'%" + name + "%'"   
//				+ " GROUP BY tbl_MD.MD_Code ORDER BY tbl_MDlistSelected.MDLIST_CODE desc";
//		String query = "SELECT tbl_MD.*, tbl_MDlistSelected.* " +
//				"FROM tbl_MD " +
//				"LEFT JOIN  tbl_MDlistSelected " +
//				"ON  tbl_MD.MD_Code= tbl_MDlistSelected.MDLIST_CODE  AND  tbl_MDlistSelected.MDLIST_PSR_CODE = " + Constants.g_psrCode +
//				" WHERE tbl_MD.MD_Name LIKE '%" + name +  "%' "  +
//				"GROUP BY tbl_MD.MD_Code " +
//				"ORDER BY tbl_MDlistSelected.MDLIST_NAME desc";
		String query = "SELECT tbl_MD.*, tbl_MDlistSelected.* " +
				"FROM tbl_MD LEFT JOIN  tbl_MDlistSelected ON tbl_MD.MD_Name = tbl_MDlistSelected.MDLIST_NAME AND " +
				"tbl_MDlistSelected.MDLIST_PSR_CODE = '" + Constants.g_psrCode + "' WHERE tbl_MD.MD_Name LIKE '%" + name +"%' " +
						"GROUP BY tbl_MD.MD_Name, tbl_MD.MD_Code ORDER BY tbl_MDlistSelected.MDLIST_NAME desc,  tbl_MD.MD_Name asc";
		Log.i(TAG,"query:" + query);
		
		
		Cursor cursor = dbSqlite.rawQuery(query,null);
				
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				String mdCode = cursor.getString(cursor
						.getColumnIndex(Constants.MD_CODE));
				String mdName = cursor.getString(cursor
						.getColumnIndex(Constants.MD_NAME));

				HashMap<String, String> map = new HashMap<String, String>();
				map.put(Constants.MD_CODE, mdCode);
				map.put(Constants.MD_NAME, mdName);

				listMD.add(map);

			} while (cursor.moveToNext());
		}

		cursor.close();
		dbSqlite.close();
		return listMD;
	}
	
//	public ArrayList<HashMap<String, String>> _mdListDisplay(String name) {
//		ArrayList<HashMap<String, String>> listMD = new ArrayList<HashMap<String, String>>();
//		SQLiteDatabase dbSqlite = this.getReadableDatabase();
//		Cursor cursor = dbSqlite.query(Constants.DATABASE_TABLE_MD,
//				new String[] { Constants.MD_CODE, Constants.MD_NAME },
//				Constants.MD_NAME + " like ? ",
//				new String[] { "%" + name + "%" }, Constants.MD_CODE, null,
//				Constants.MD_NAME);
//		// Cursor cursor = dbSqlite.query(true, Constants.DATABASE_TABLE_MD, new
//		// String[] {Constants.MD_CODE, Constants.MD_NAME},
//		// Constants.MD_NAME + " like ? ",new String[]{"%" + name + "%"}, null,
//		// null, Constants.MD_NAME, null, null);
//
//		if (cursor.getCount() > 0) {
//			cursor.moveToFirst();
//			do {
//				String mdCode = cursor.getString(cursor
//						.getColumnIndex(Constants.MD_CODE));
//				String mdName = cursor.getString(cursor
//						.getColumnIndex(Constants.MD_NAME));
//
//				HashMap<String, String> map = new HashMap<String, String>();
//				map.put(Constants.MD_CODE, mdCode);
//				map.put(Constants.MD_NAME, mdName);
//
//				listMD.add(map);
//
//			} while (cursor.moveToNext());
//		}
//
//		cursor.close();
//		dbSqlite.close();
//		return listMD;
//	}

	public void StoreMDlist(String md_psr_code, String md_code, String md_name) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();

		contentValues.put(Constants.MDLIST_PSR_CODE, md_psr_code);
		contentValues.put(Constants.MDLIST_CODE, md_code);
		contentValues.put(Constants.MDLIST_NAME, md_name);

		db.insert(Constants.DATABASE_TABLE_MDlistSelected, null, contentValues);
		db.close();

		return;

	}

	public ArrayList<HashMap<String, String>> get_MD_List(String psr_code) {

		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		ArrayList<HashMap<String, String>> get_MD_List_byPSR = new ArrayList<HashMap<String, String>>();
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected,
				new String[] { Constants.MDLIST_PSR_CODE, Constants.MDLIST_CODE, Constants.MDLIST_NAME },
				Constants.MDLIST_PSR_CODE + "=?", new String[] { psr_code },
				Constants.MDLIST_CODE + "," + Constants.MDLIST_NAME, null,
				Constants.COLUMN_ID + " asc");
		if (c.moveToFirst()) {
			do {
				HashMap<String, String> hashmap = new HashMap<String, String>();
				hashmap.put(Constants.MDLIST_CODE,
						c.getString(c.getColumnIndex(Constants.MDLIST_CODE)));
				hashmap.put(Constants.MDLIST_NAME,
						c.getString(c.getColumnIndex(Constants.MDLIST_NAME)));
				get_MD_List_byPSR.add(hashmap);
			} while (c.moveToNext());
		}
		dbSqlite.close();
		return get_MD_List_byPSR;
	}
	
	

	public boolean isMDAlreadySelected(String mdCode) {
		boolean isExist = false;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(
				"SELECT * FROM tbl_MDlistSelected where MDLIST_CODE = ?",
				new String[] { mdCode });
		if (cursor.getCount() > 0) {
			isExist = true;

		} else {
			isExist = false;
		}

		cursor.close();
		db.close();
		return isExist;
	}
	
	public Cursor _mdLIST(String psr_code) {
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
//		Cursor cursor = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected, new String[] {Constants.MDLIST_CODE, Constants.MDLIST_NAME},
//				Constants.MDLIST_NAME + " like ? AND " + Constants.MDLIST_PSR_CODE + " =? ",
//				new String[]{"%" + _filter + "%",psr_code}, null, null, null);
		Cursor cursor = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected, new String[] {Constants.MDLIST_CODE, Constants.MDLIST_NAME},
				Constants.MDLIST_PSR_CODE + " =?",
				new String[] {psr_code}, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		dbSqlite.close();
		return cursor;
	}
	
	
//	public void deleteRecordSelected(String ID) {
//		SQLiteDatabase db = this.getWritableDatabase();
//		db.delete(Constants.DATABASE_TABLE_MDlistSelected,
//				Constants.COLUMN_ID + "=?", new String[] { ID });
//		db.close();
//		String query = "DELETE * FROM tbl_MDlistSelected WHERE MDLIST_PSR_CODE = " + "'"+ID+"'";
//		db.rawQuery(query, null);
//
//	}
	
	public void deleteRecordSelected(String ID) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Constants.DATABASE_TABLE_MDlistSelected,
				Constants.MDLIST_PSR_CODE + " =?", new String[] { ID});
		db.close();

	}
	
	public Cursor get_mdTrue(String md_name){
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_MDlistSelected, new String[] {Constants.COLUMN_ID2,
				Constants.MDLIST_CODE, 
				Constants.MDLIST_NAME},
				Constants.MDLIST_NAME + "=?", new String[] {md_name},
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}
	
	
	public Cursor get_mdFalse(String md_name){
		SQLiteDatabase dbSqlite = this.getReadableDatabase();
		
		Cursor c = dbSqlite.query(Constants.DATABASE_TABLE_MD, new String[] {Constants.COLUMN_ID2,
				Constants.MD_CODE, 
				Constants.MD_NAME},
				Constants.MD_NAME + "=?", new String[] {md_name},
				null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		dbSqlite.close();
		return c;
	}
	
	public boolean checkIfInMDLIST(String name, String psrCode){
		SQLiteDatabase db = this.getReadableDatabase();
		boolean isExist = false;
		Cursor cursor = db.query(Constants.DATABASE_TABLE_MDlistSelected, null, Constants.MDLIST_NAME + " =? AND " + Constants.MDLIST_PSR_CODE + " =? ", new String[]{name, psrCode}, null, null, null);
		if (cursor.getCount() > 0) {
			
			isExist = true;
		}else{
			isExist = false;
					
		}
		
		db.close();
		return isExist;
		
	}
	
	public int countSelectedMDList(String psrCode){
		SQLiteDatabase db = this.getReadableDatabase();
		int count = 0;
		Cursor cursor = db.query(Constants.DATABASE_TABLE_MDlistSelected, new String[]{"count(*)"}, Constants.MDLIST_PSR_CODE + "=?", new String[]{psrCode}, null, null, null);
		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			count = cursor.getInt(cursor.getColumnIndex("count(*)"));
		}
		
		db.close();
		return count;
	}
	
	
	public void updateInitialLogin(){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("initial_login", "1");
		db.update("tbl_InitialLogin", values, "rowid = ?", new String[]{"1"});
		db.close();
	}
	
	public boolean isInitialUpdate(){
		SQLiteDatabase db = this.getReadableDatabase();
		boolean isInitialLogin = false;
		Cursor c = db.query("tbl_InitialLogin", new String[]{"initial_login"}, null, null, null, null, null);
		if (c.getCount() > 0) {
			c.moveToFirst();
			String initialLogin = c.getString(0);
			if (initialLogin.equals("0")) {
				isInitialLogin = true;
			}else{
				isInitialLogin = false;
						
			}
		}
		
		c.close();
		db.close();
		return isInitialLogin;
	}
	
	public boolean isValidDoctor(String doctorName, boolean isHaveReferedMd){
		SQLiteDatabase db = this.getReadableDatabase();
		boolean isValidDoctor;
		Cursor cursor;
		if (isHaveReferedMd) {
			cursor = db.query(Constants.DATABASE_TABLE_MDlistSelected, null, Constants.MDLIST_PSR_CODE + " =? AND " + Constants.MDLIST_NAME + " =? ", new String[]{Constants.g_psrCode, doctorName}, null, null, null);
			
		}else{
			cursor = db.query(Constants.DATABASE_TABLE_MD, null, Constants.MD_NAME + " =? ", new String[]{doctorName}, null, null, null);
		}
		
		if (cursor.getCount() > 0) {
			isValidDoctor = true;
		}else{
			isValidDoctor = false;
		}
		
		cursor.close();
		db.close();
		return isValidDoctor;
		
	}
	
	public boolean isHasRecepient(){
		boolean hasRecipient;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(Constants.DATABASE_TABLE_RECEPIENT, null, null, null, null, null, null);
		if (cursor.getCount() > 0) {
			hasRecipient = true;
		}else{
			hasRecipient = false;
		}
		
		cursor.close();
		db.close();
		return hasRecipient;
	}
	
	public ArrayList<String> getEmailRecipient(){
		ArrayList<String> listRecipient = new ArrayList<String>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(Constants.DATABASE_TABLE_RECEPIENT, null, null, null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				listRecipient.add(cursor.getString(cursor.getColumnIndex(Constants.RECEPIENT_EMAIL_ADDRESS)));
			} while (cursor.moveToNext());
			
		}
		cursor.close();
		db.close();
		return listRecipient;
	}
	
	public boolean checkIfEmailExist(String emailAddress){
		
		boolean isExist;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(Constants.DATABASE_TABLE_RECEPIENT, null, Constants.RECEPIENT_EMAIL_ADDRESS + "=?", new String[]{emailAddress}, null, null, null);
		if (cursor.getCount() > 0) {
			isExist = true;
		}else{
			isExist = false;
		}
		
		cursor.close();
		db.close();
		return isExist;
		
	}
	
	public void addRecepient(String emailRecepient){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Constants.RECEPIENT_EMAIL_ADDRESS, emailRecepient);
		db.insert(Constants.DATABASE_TABLE_RECEPIENT, null, values);
		db.close();
	}
	
	public void updateRecepient(String emailRecepient){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Constants.RECEPIENT_EMAIL_ADDRESS, emailRecepient);
		db.update(Constants.DATABASE_TABLE_RECEPIENT, values, Constants.COLUMN_ID + "=?", new String[]{"1"});
		db.close();
		
	}
	
	public void deleteRecipient(String emailAddress){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Constants.DATABASE_TABLE_RECEPIENT, Constants.RECEPIENT_EMAIL_ADDRESS + "=?", new String[]{emailAddress});
		db.close();
	}
	
	
	public void insertUnsentRegistration(String unsentMessage){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(Constants.UNSENT_MESSAGE, unsentMessage);
		db.insert(Constants.TABLE_UNSENT, null, values);
		db.close();
	}
	
	public void deleteUnsentRegistration(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Constants.TABLE_UNSENT, null, null);
		db.close();
	} 
	
	public ArrayList<UnsentRegistration> getAllUnsentRegistration(){
		ArrayList<UnsentRegistration> listUnsentReg = new ArrayList<UnsentRegistration>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(Constants.TABLE_UNSENT, null, null, null, null, null, null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				UnsentRegistration unsentReg = new UnsentRegistration();
				unsentReg.setId(cursor.getInt(cursor.getColumnIndex(Constants.COLUMN_ID)));
				unsentReg.setMessage(cursor.getString(cursor.getColumnIndex(Constants.UNSENT_MESSAGE)));
				listUnsentReg.add(unsentReg);
			} while (cursor.moveToNext());
			
		}
		cursor.close();
		db.close();
		return listUnsentReg;
	}
	
	public boolean checkIfMessageExist(String message){
		
		boolean isExist;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(Constants.TABLE_UNSENT, null, Constants.UNSENT_MESSAGE + "=?", new String[]{message}, null, null, null);
		if (cursor.getCount() > 0) {
			isExist = true;
		}else{
			isExist = false;
		}
		
		cursor.close();
		db.close();
		return isExist;
	}

}
