package com.ph.com.psr_kap.utilities.application;

import android.app.Application;

import com.tsengvn.typekit.Typekit;

public class ApplicationController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setupFonts();
    }

    public void setupFonts() {
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "quicksan_regular.otf"))
                .addBold(Typekit.createFromAsset(this, "quicksand_bold.otf"));
    }
}
