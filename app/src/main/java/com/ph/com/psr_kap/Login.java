package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.email.EmailHelper;
import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.service.EmailService;
import com.ph.com.psr_kap.utilities.build.BuildConfigManager;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;

import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class Login extends BaseActivity {

    /**
     * Flags
     */
    private String TAG = this.getClass().getSimpleName();

    DatabaseHandler databaseHandler;
    TextView mTxtVersionName;
    EditText txt_Username, txt_Password;
    Button btn_Login, btn_Cancel;
    Typeface tnr_normal, tnr_bold_italic, tnr_bold, monotype;
    //    float font_title;
//    static float font_content;
    private SharedPreferences sharedPreferences;
    //	public static SharedPreferences.Editor sharedPreferencesEditor;
    boolean doubleBackToExitPressedOnce = false;
    String username = "", password = "";
    Calendar calendar;
    Context context;
    FTPClient client = null;

    int counter_Login;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /**Initialize Helper classes*/
        BuildConfigManager.init();

        /**Initialize Views*/
        setupViews();

        // Toast.makeText(context, "asdf", 4).show();
        context = this;
        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }

        try {
            databaseHandler.copyDBFromResource2();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //start service
        executeEmailSender();

        counter_Login = 1;
        // DOWNLOADING DATA=================================================
        // databaseHandler.deleteTablePSR();
        // databaseHandler.deleteTableMD();
        // Async_FTP_Download asyncuser = new Async_FTP_Download(context);
        // asyncuser.execute();

        // ====================================================

        sharedPreferences = getSharedPreferences(
                Constants.SHARED_PREFERENCES_CREATOR, 0);
//		Async_ValidateData.sharedPreferencesEditor = sharedPreferences.edit();

        txt_Username = (EditText) findViewById(R.id.edtUsername);
        txt_Password = (EditText) findViewById(R.id.edtPassword);

        TextView lbl_username = (TextView) findViewById(R.id.lbl_username);
        TextView lbl_password = (TextView) findViewById(R.id.lbl_password);

        // typeface
        tnr_normal = Typeface.createFromAsset(getAssets(),
                "times_new_roman_normal.ttf");
        tnr_bold_italic = Typeface.createFromAsset(getAssets(),
                "times_new_roman_bold_italic.ttf");
        tnr_bold = Typeface.createFromAsset(getAssets(),
                "times_new_roman_bold.ttf");
        monotype = Typeface.createFromAsset(getAssets(), "Monotype.TTF");

//        font_title = getResources().getDimensionPixelSize(R.dimen.font_title);
//        font_content = getResources().getDimensionPixelSize(
//                R.dimen.font_content);
//
//        lbl_username.setTypeface(tnr_bold);
//        lbl_username.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_password.setTypeface(tnr_bold);
//        lbl_password.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//
//        txt_Username.setTypeface(tnr_normal);
//        txt_Username.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        txt_Password.setTypeface(tnr_normal);
//        txt_Password.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);

        btn_Login = (Button) findViewById(R.id.btn_login);
        btn_Cancel = (Button) findViewById(R.id.btn_cancel);

        btn_Cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        btn_Login.setOnClickListener(new OnClickListener() {
            // Cursor cursor;
            @Override
            public void onClick(View v) {
                String username1 = "admin";
                String password1 = "admin";
                username = txt_Username.getText().toString();
                password = txt_Password.getText().toString();
                Constants.username = username;
                Constants.password = password;

                if (username.equals(username1) && password.equals(password1)) {
                    Intent i = new Intent(Login.this, RegistrationPage.class);
                    startActivity(i);
                    finish();

                } else {
                    Toast.makeText(context, "Invalid Credentials", Toast.LENGTH_LONG).show();
                }


//				if (txt_Username.length() == 0 && txt_Password.length() == 0) {
//					Toast.makeText(Login.this,
//							"Please enter a valid username and password.",
//							Toast.LENGTH_LONG).show();
//				} else if (txt_Username.length() == 0) {
//					Toast.makeText(Login.this,
//							"Please enter a valid username.", Toast.LENGTH_LONG)
//							.show();
//				} else if (txt_Password.length() == 0) {
//					Toast.makeText(Login.this,
//							"Please enter a valid password.", Toast.LENGTH_LONG)
//							.show();
//				} else if (databaseHandler.check_PSR_if_Exist(username) == true) {
//
//					if (!username.equals("") && username != null
//							&& !password.equals("") && password != null) {
//						Log.i(Constants.TAG + "1", username);
//						Log.i(Constants.TAG + "1", password);
//
//						Cursor s = databaseHandler.getCursorUsernamePassword(
//								username, password);
//						
//						new Async_ValidateData(context,username, password).execute();
//						
//						
//						
////						if (s.getCount() > 0 && NetworkHelper.isInternetAvailable(Login.this)) {
////							
////						}else{
////							startActivity(new Intent(context, RegistrationPage.class));
////							finish();
////						}
//						
//						
//						
//					} else {
//						Toast.makeText(
//								Login.this,
//								"Login failed. Please enter a correct username and password.",
//								Toast.LENGTH_LONG).show();
//					}
//
//				} else {
//					Log.d("PSR NOT EXIST", username);
//					
//					if (counter_Login < 3) {
//						if (NetworkHelper.isInternetAvailable(Login.this)) {
//							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//									Login.this);
//							alertDialogBuilder.setTitle("Login Error");
//							alertDialogBuilder
//									.setMessage(
//											"Account does not exist. Would you like to check for account update?")
//									.setCancelable(false)
//									.setPositiveButton(
//											"Yes",
//											new DialogInterface.OnClickListener() {
//												public void onClick(
//														DialogInterface dialog,
//														int id) {
//													counter_Login++;
//													Log.i("counter_Login---------------",
//															"" + counter_Login);
//													Async_FTP_Download asyncuser = new Async_FTP_Download(
//															context);
//													asyncuser.execute();
//												}
//											})
//									.setNegativeButton(
//											"No",
//											new DialogInterface.OnClickListener() {
//												public void onClick(
//														DialogInterface dialog,
//														int id) {
//													dialog.cancel();
//												}
//											});
//
//							AlertDialog alertDialog = alertDialogBuilder
//									.create();
//							alertDialog.show();
//						} else {
//							// Toast.makeText(Login.this,
//							// "No Internet Connection", Toast.LENGTH_LONG)
//
//							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//									context);
//							// set title
//							alertDialogBuilder.setTitle("Login Error");
//							// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);
//
//							// set dialog message
//							alertDialogBuilder
//									.setMessage("Account does not exist. Please connect to the internet to check your account data.");
//							alertDialogBuilder.setCancelable(false);
//							alertDialogBuilder.setNeutralButton("OK",
//									new DialogInterface.OnClickListener() {
//										@Override
//										public void onClick(
//												DialogInterface dialog, int id) {
//											// TODO Auto-generated method stub
//											dialog.cancel();
//										}
//									});
//
//							alertDialog = alertDialogBuilder.create();
//							alertDialog.show();
//
//						}
//					} else {
//						Toast.makeText(Login.this, "Account does not exist.",
//								Toast.LENGTH_LONG).show();
//						counter_Login = 1;
//					}
//
//				}
//

            }

        });

//		lbl_password.setText("kabalikat");
//		lbl_username.setText("sklim");
    }

    private void setupViews() {
        mTxtVersionName = (TextView) findViewById(R.id.txtVersionName);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mTxtVersionName.setText(getResources().getString(R.string.label_version_name) +
                " " + BuildConfigManager.getInstance().getVersionName());
    }

//	private class AsyncValidateData extends AsyncTask<Void, Void, Cursor> {
//		private ProgressDialog progressDialog;
//		private String username, password;
//
//		public AsyncValidateData(String username, String password)
//
//		{
//			Log.i(Constants.TAG + 2, username);
//			Log.i(Constants.TAG + 2, password);
//			this.username = username;
//			this.password = password;
//		}
//
//		protected void onPreExecute() {
//			progressDialog = new ProgressDialog(Login.this);
//			progressDialog.setMessage("Validating....");
//			progressDialog.show();
//			progressDialog.setCancelable(false);
//		}
//
//		@Override
//		protected Cursor doInBackground(Void... params) {
//			Log.i(Constants.TAG + 3, username);
//			Log.i(Constants.TAG + 3, password);
//			return databaseHandler.getCursorUsernamePassword(this.username,
//					this.password);
//
//		}
//
//		@Override
//		protected void onProgressUpdate(Void... unused) {
//
//		}
//
//		protected void onPostExecute(Cursor cursor) {
//			Log.i("onPostExecute", "onPostExecute");
//			progressDialog.dismiss();
//			if (cursor != null) {
//				if (cursor.getCount() != 0) {
//					Cursor user_cursor = databaseHandler
//							.get_PSR_Code(this.username);
//					String psr_code = user_cursor.getString(user_cursor
//							.getColumnIndex(Constants.PSR_CODE));
//					Constants.g_psrCode = psr_code;
//					sharedPreferencesEditor.putString(Constants.SP_PSR_CODE,
//							psr_code);
//					sharedPreferencesEditor.commit();
//					Log.i(Constants.TAG, "Successful");
//					startActivity(new Intent(Login.this, RegistrationPage.class));
//					finish();
//				} else {
//					Toast.makeText(Login.this,
//							"Login failed. Username and password don't match.",
//							Toast.LENGTH_LONG).show();
//				}
//			} else {
//				Toast.makeText(Login.this,
//						"Login failed. Username and password don't match.",
//						Toast.LENGTH_LONG).show();
//			}
//
//		}
//	}


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }
        return ret;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit",
                Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void executeEmailSender() {
        AlarmManager alarmManager = null;
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent, pendingIntent2;


//        AlarmManager am=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        Intent i = new Intent(getBaseContext(), AutoEmailReceiver.class);
//        PendingIntent pi = PendingIntent.getBroadcast(getBaseContext(), 0, i, 0);
//        am.setRepeating(AlarmManager.RTC_WAKEUP, 3600000, 1000, pi);
//        am.set(AlarmManager.RTC_WAKEUP, 3600000, pi);

        startService(new Intent(context, EmailService.class));


    }

    public class SendingEmail extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog progressDialog;
        Exception exception = null;
        String _username, _password;

        String dir = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/PSR_Registration_Form/Unsent Registration/";
        String fileName = "UnsentRegistration.txt";
        Context context;

        public SendingEmail(Context context, String username, String password) {
            this._username = "citmobiledev@gmail.com"; // username;
            this._password = "AndroidDev2013"; // password;
            this.context = context;
        }

        protected void onPreExecute() {
        }

        protected Boolean doInBackground(Void... params) {


            File fileToSent = new File(dir + fileName);

            if (fileToSent.exists()) {

                Log.i("EmailService", "fileExist");

                EmailHelper emailSender = new EmailHelper(_username, _password);

                String[] arr_recipient = new String[]{"citmobiledev@gmail.com"};

                emailSender.setTo(arr_recipient);
                emailSender.setFrom(_username);
                emailSender.setSubject("Unsent Registration");
                emailSender.setBody("Please see the attached file/s.");

                try {

                    emailSender.addAttachment(dir + fileName);

                    if (emailSender.send()) {
                        LoggerHelper.log(TAG, "Email sending success!");

                        return true;
                    } else {
                        LoggerHelper.log(TAG, "Email sending failed!");
                        String g_dialog_message = "Failed sending email.";
                        Log.i("Failed", g_dialog_message);
                        return false;
                    }

                } catch (Exception e) {
                    // some other problem
                    e.printStackTrace();
                    String g_dialog_message = "Sending failed.\nWeak internet connection unable to send email\nCan't connect to SMTP HOST\nPlease try again\nTHANK YOU";
                    Log.i("Failed", g_dialog_message);
                    return false;
                }
            } else {
                Log.i("EmailService", "fileNotExist");
                return false;
            }

        }

        protected void onPostExecute(Boolean valid) {

            try {
                if (valid) {
//    				deleteFile(dir, fileName);
//    				showNotification(this.context, "Email sent successfully.");
//    				stopSelf();


                } else {
//    				showNotification(this.context, "Email sending failed.");
                }

                context.stopService(new Intent(context, EmailService.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}