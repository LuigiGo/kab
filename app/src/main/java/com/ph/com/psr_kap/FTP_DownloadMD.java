package com.ph.com.psr_kap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class FTP_DownloadMD extends AsyncTask<String, String, Boolean> {
	String title = "";
	String msg = "";

	Context l_context;

	Timer timer;
	TimerTask timerTask;
	final Handler handler = new Handler();

	ProgressDialog dialog;

	String str_Path;

	/**
	 * @param context
	 * @param isContinueDownloadOutput
	 * @param isFromLogin
	 */
	public FTP_DownloadMD(Context context) {
		l_context = context;

		initDownloadDirectory();

	}

	private void initDownloadDirectory() {
		str_Path = Environment.getExternalStorageDirectory().toString()
				+ "/Kabalikat_Advocacy_Program_2015/MD_Codes";
		File mediaStorageDir = new File(str_Path);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Toast.makeText(l_context, "Failed to create directory",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog = new ProgressDialog(l_context);
		dialog.setMessage("Checking for updates. Please wait...");
		dialog.setCancelable(false);
		dialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {

		// // Start timer. Used to dismiss the dialog.
		 startTimer();

		// Init handler
		FTP_Handler ftp_handler = new FTP_Handler();
		// Connect to FTP Server
		boolean isFTPConnected = ftp_handler.connnectingwithFTP(
				Constants.FTP_ADDRESS, Constants.FTP_USERNAME,
				Constants.FTP_PASSWORD,
				Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES);

		Log.i("isFTPConnected: DownloadMD", String.valueOf(isFTPConnected));

		if (isFTPConnected) {

			String strFilename = "MD_Codes.csv";
			File downloadFile = new File(str_Path, strFilename);
			String remoteFilePath = Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES
					+ "/" + strFilename;

			boolean isDownloaded = ftp_handler.downloadSingleFile(
					ftp_handler.getFTPClient(), remoteFilePath, downloadFile);

			Log.i(downloadFile.getName() + " - isDownloaded",
					String.valueOf(isDownloaded));
			if (isDownloaded == false) {
				// Retry download
				boolean isRetryDownloaded = ftp_handler.downloadSingleFile(
						ftp_handler.getFTPClient(), remoteFilePath,
						downloadFile);
				Log.i(downloadFile.getName() + " - isRetryDownloaded",
						String.valueOf(isRetryDownloaded));
				if (isRetryDownloaded == false) {
					boolean isRetryDownloaded2 = ftp_handler
							.downloadSingleFile(ftp_handler.getFTPClient(),
									remoteFilePath, downloadFile);
					Log.i(downloadFile.getName() + " - isRetryDownloaded2",
							String.valueOf(isRetryDownloaded2));
					if (isRetryDownloaded2 == false) {
						title = "Download Error";
						msg = "An error occurred while downloading records. \nWould you like to try again?";
						return false;
					} else {

						title = "Download";
						msg = "Download complete.";

						return true;

					}
				} else {

					title = "Download";
					msg = "Download complete.";

					return true;

				}
			} else {

				title = "Download";
				msg = "Download complete.";
				return true;

			}
		} else {
			title = "Connection Error";
			msg = "Can't connect to server.\nPlease ensure that you are connected to internet.";
			return false;
		}
		
//		if (updateData() == true) {
//			return true;
//		} else {
//			return false;
//		}
	}


	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);

		if (dialog != null) {
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		}

		if (result == true) {
			if (Constants.isCheckBoth == true) {

				new FTP_SaveBrand(l_context).execute();

			} 
			else if(Constants.isOnline == true){
				new FTP_SaveBrand(l_context).execute();
			}
			
			else {

				new FTP_SaveMD(l_context).execute();
			}

		} else {
			// System.out.println("File not downloaded, RETRY");
			// Log.i("File not downloaded, RETRY",
			// "File not downloaded, RETRY");
			// new FTP_DownloadMD(l_context).execute();

			Toast.makeText(l_context, "Failed to Download Files",
					Toast.LENGTH_LONG).show();

		}
	}
	
	
	
	
		public boolean updateData() {
			
			boolean result;
			try {
				startTimer();
				// DELETE DATA and OVERRIDE THE PRELOADED PSR TABLE DATA


				FTPClient client = new FTPClient();
				FileOutputStream fos = null;

				try {
					client.setConnectTimeout(30000);

					client.connect(Constants.FTP_ADDRESS);
					Log.d("CLIENT-----",
							"Connected. Reply: " + client.getReplyString());

					// client.login(Constants.FTP_USERNAME, Constants.FTP_PASSWORD);
					// Log.d("CLIENT-----", "Logged in");

					result = client.login(Constants.FTP_USERNAME,
							Constants.FTP_PASSWORD);
					
					Log.i("jessmark", "is DowloadMD connected: " + result);

					if (result == true) {
						System.out
								.println("Successfully logged in! -------------------FTP-----------------");

						// client.enterLocalPassiveMode();

						client.setFileType(FTP.BINARY_FILE_TYPE);
						Log.d("CLIENT-----", "Downloading");

						client.changeWorkingDirectory(Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES);

						// Log.i("LOG IN STATUS:", String.valueOf(logged));

						FTPFile[] files = client.listFiles();

						for (FTPFile file : files) {

							if (file.getType() == FTPFile.FILE_TYPE) {

								System.out.println("File Name: " + file.getName());

							}

						}


						String file = "MD_Codes.csv";
						String remoteFile1 = Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES
								+ "/" + file;

						// File where to put downloaded file
						File downloadFile1 = new File(str_Path + "/" + file);


						OutputStream outputStream1 = new BufferedOutputStream(
								new FileOutputStream(downloadFile1));

							client.setBufferSize(1024 * 1024);
							boolean success = client.retrieveFile(
									remoteFile1, outputStream1);

							Log.i("DownloadFile", downloadFile1
									.toString());
							Log.i("RemoteFile", remoteFile1);

							if (success) {
								System.out.println("File #" + downloadFile1
										+ " has been downloaded successfully.");
							} else {
								System.out.println("File #" + remoteFile1
										+ " not downloaded.");
							}
							outputStream1.close();

					} else {
						System.out.println("Login Fail! - doInBackground");
						title = "Connection Error";
						msg = "Can't connect to server.";
						return false;
					}

					return true;

				} catch (ConnectException ce) {
					ce.printStackTrace();

					title = "Connection Error";
					msg = "Can't connect to server.\nPlease ensure that you are connected to internet.";
					return false;

				} catch (IOException e) {

					e.printStackTrace();

					title = "Connection Error";
					msg = "Can't connect to server.\nPlease ensure that you are connected to internet.";
					return false;

				} finally {

					try {

						if (fos != null) {

							fos.close();

						}

						client.disconnect();

					} catch (IOException e) {

						e.printStackTrace();
					}

				}

			} catch (Exception e) {

				e.printStackTrace();

				title = "Connection Error";
				msg = "Can't connect to server.\nPlease ensure that you are connected to internet.";
				return false;

			}

		}
		
		public void startTimer() {
			// set a new Timer
			timer = new Timer();

			// initialize the TimerTask's job
			initializeTimerTask();

			// schedule the timer, after the first 5000ms the TimerTask will run
			// every 10000ms
			timer.schedule(timerTask, 30000, 30000); //
		}
		
		public void initializeTimerTask() {

			timerTask = new TimerTask() {
				public void run() {

					// use a handler to run a toast that shows the current timestamp
					handler.post(new Runnable() {
						public void run() {
							dialog.dismiss();
//							stoptimertask();
//							Toast.makeText(
//									mContext,
//									"There was a problem in checking for updates. Please try again.",
//									Toast.LENGTH_LONG).show();

						}
					});
				}
			};
		}

}
