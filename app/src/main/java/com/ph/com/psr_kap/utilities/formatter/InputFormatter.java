package com.ph.com.psr_kap.utilities.formatter;

public class InputFormatter {

    public static String formatMobileNumber(String mobileNumber) {
        if (mobileNumber != null || !mobileNumber.isEmpty()) {
            if (mobileNumber.substring(0, 2).equals("09")) {
                mobileNumber = mobileNumber.substring(1);
                mobileNumber = "+63" + mobileNumber;
            } else if (mobileNumber.substring(0, 3).equals("639")) {
                mobileNumber = "+" + mobileNumber;
            }
        }

        return mobileNumber;
    }
}
