package com.ph.com.psr_kap.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.ph.com.psr_kap.DatabaseHandler;
import com.ph.com.psr_kap.EmailRecepient;
import com.ph.com.psr_kap.R;
import com.ph.com.psr_kap.SplashScreen;
import com.ph.com.psr_kap.email.EmailHelper;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class AutoEmailReceiver extends BroadcastReceiver{

	 private Calendar alarmTime = Calendar.getInstance();
	 private File fileToSent;
	 String dir;
	 String fileName;
	 DatabaseHandler databaseHandler;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		
		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}
		
//        Toast.makeText(context, "BroadCastReceiver", Toast.LENGTH_LONG).show();
        
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Calendar calendar = Calendar.getInstance();
        String time = sdf.format(calendar.getTime());
        Log.i("time", "time: " + time);
        
        if (time.equals("12:00") || time.equals("18:00")) {
        	Log.i("time to send", "its time to send");
        	
        	dir = Environment.getExternalStorageDirectory().getAbsolutePath()
					+ "/PSR_Registration_Form/Unsent Registration/";
			fileName = "UnsentRegistration.txt";
        	fileToSent = new File(dir + fileName);
        	
        	
			if (fileToSent.exists()) {
				Log.i("AutoEmailReceiver", "file exist");
				if (databaseHandler.isHasRecepient()) {
					ArrayList<String> recipient = databaseHandler.getEmailRecipient();
					Log.i("AutoEmailReceiver", recipient.toString());
					new SendingEmail(context, "citmobiledev@gmail.com", "AndroidDev2013",recipient).execute();
				}else{
					showNotification(context, "No email recepient.");
				}
				
			}else{
				Log.i("AutoEmailReceiver", "file not exist");
			}
        	
//        	showNotification(context, "broadcast");
		}
        
        
		
	}
	
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@SuppressLint("NewApi")
	public void showNotification(Context context, String textContent) {
		
		// Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		// v.vibrate(100);

		// define sound URI, the sound to be played when there's a notification
		Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		// intent triggered, you can add other intent for other actions
		Intent intent = new Intent(context, SplashScreen.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		// this is it, we'll build the notification!
		// in the addAction method, if you don't want any icon, just set the
		// first param to 0
		Notification mNotification = new Notification.Builder(context)

		.setContentTitle("Kabalikat")
				.setContentText(textContent)
				.setSmallIcon(R.drawable.ic_launcher).setContentIntent(pIntent)
				.setSound(soundUri).build();
		 
				
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		// If you want to hide the notification after it was selected, do the
		// code below
		mNotification.flags = Notification.FLAG_AUTO_CANCEL;   
		

		notificationManager.notify(0, mNotification);
	}
	
	
	
	 public class SendingEmail extends AsyncTask<Void, Void, Boolean> {
	    	
	    	private ProgressDialog progressDialog;
	    	Exception exception = null;
	    	String _username, _password;
	    	
	    	ArrayList<String> recipient;
			Context context;

	    	public SendingEmail(Context context, String username, String password, ArrayList<String> recipient) {
	    		this._username = "citmobiledev@gmail.com"; // username;
	    		this._password = "AndroidDev2013"; // password;
	    		this.context = context;
	    		this.recipient = recipient;
	    	}

	    	protected void onPreExecute() {
	    	}

	    	protected Boolean doInBackground(Void... params) {

	    		

	    			EmailHelper emailSender = new EmailHelper(_username, _password);
	    			
	    			Log.i("AutoEmailReceiver", "start background");

	    			String[] arr_recipient = new String[recipient.size()];
	    			arr_recipient = recipient.toArray(arr_recipient);
	    			
	    			for (int i = 0; i < arr_recipient.length; i++) {
						Log.i("AutoEmailReceiver", arr_recipient[i]);
					}

	    			emailSender.setTo(arr_recipient);
	    			emailSender.setFrom(_username);
	    			emailSender.setSubject("Kabalikat Registration");
	    			emailSender.setBody("Please see the attached file/s.");

	    			try {
	    				
	    				emailSender.addAttachment(dir + fileName);

	    				if (emailSender.send()) {
	    					// success
	    					Log.i("SUCCESSS!!!!!!!! ===========", "====SEEEEEENDDD");

	    					return true;
	    				} else {
	    					// failure
	    					Log.i("failed!!!!!!!! ===========", "====FAILED");
	    					String g_dialog_message = "Failed sending email.";
	    					Log.i("Failed", g_dialog_message);
	    					return false;
	    				}

	    			} catch (Exception e) {
	    				// some other problem
	    				e.printStackTrace();
	    				String g_dialog_message = "Sending failed.\nWeak internet connection unable to send email\nCan't connect to SMTP HOST\nPlease try again\nTHANK YOU";
	    				Log.i("Failed", g_dialog_message);
	    				return false;
	    			}
	    		
	    		
	    	}
	    	
	    	protected void onPostExecute(Boolean valid) {

	    		try {
	    			if (valid) {
	    				deleteFile(dir, fileName);
	    				showNotification(this.context, "Unsent SMS sent to email successfully.");
	    				
	    				databaseHandler.deleteUnsentRegistration();
	    				context.stopService(new Intent(context, EmailService.class));
	    				
	    				
	    			} else {
	    				showNotification(this.context, "Email sending failed.");
	    			}
	    			
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    		}
	    	}
	    }
	 
	 private void deleteFile(String inputPath, String inputFile) {
			try {
				// delete the original file
				new File(inputPath + inputFile).delete();

			} catch (Exception e) {
				Log.e("tag", e.getMessage());
			}
		}
}
