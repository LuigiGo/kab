package com.ph.com.psr_kap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class FTP_SaveMD extends AsyncTask<String, Integer, Boolean> {

	protected static final String TAG = FTP_SaveMD.class.getSimpleName();
	ProgressDialog progressDialog;
	Context context;

	DatabaseHandler databaseHandler;
	int totalNoOfItemsToBeUpdated, percentage;
	double itemsUpdated;
	ArrayList<String> listDeliveryNo;

	ArrayList<String> listMD_Name;
	ArrayList<String> listMD_Code;

	AlertDialog alertDialog;
	// ====================
	Calendar calendar;
	String fileName, fname;

	public FTP_SaveMD(Context context) {
		this.context = context;

		listDeliveryNo = new ArrayList<String>();

		listMD_Name = new ArrayList<String>();
		listMD_Code = new ArrayList<String>();

		calendar = Calendar.getInstance();

		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}

	}

	protected void onPreExecute() {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Updating Medical Doctors. Please wait...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		// CSVCompanies

		String CSVMD_Codes = Environment.getExternalStorageDirectory()
				.toString()
				+ "/Kabalikat_Advocacy_Program_2015/MD_Codes/MD_Codes.csv";

		BufferedReader br = null;
		String line = "";
		// REGEX that ignores "," comma within double qoute ""
		String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		// READ CSV Members
		try {
			Log.i("try", "try");
			br = new BufferedReader(new FileReader(CSVMD_Codes));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] MDDetails = line.split(cvsSplitBy);

				listMD_Name.add(MDDetails[0].replace("\"", ""));
				Log.i("MDDetails[0]", MDDetails[0]);// MD_name
				Log.i("MDDetails[1]", MDDetails[1]);// MD_Code

				listMD_Code.add(MDDetails[1]);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		totalNoOfItemsToBeUpdated = listMD_Name.size();
		// Log.i("totalNoOfItemsToBeUpdated--------------",
		// ""+totalNoOfItemsToBeUpdated);
		//
		//

		if (listMD_Code.size() > 0) {
			databaseHandler.deleteTableMD();

		}
		for (int i = 0; i < listMD_Code.size(); i++) {
			if (i >= 1) {

				String mdName = listMD_Name.get(i);
				String mdCode = listMD_Code.get(i);
				ContentValues values = new ContentValues();

				values.put(Constants.MD_NAME, mdName);
				values.put(Constants.MD_CODE, mdCode);

				databaseHandler.updateMDTable(values);

				itemsUpdated++;
				percentage = (int) ((itemsUpdated / totalNoOfItemsToBeUpdated) * 100);
				updateProgress(percentage);

			}

		}

		Cursor md_c = databaseHandler.GetMDTemp();

		if (md_c != null) {
			if (md_c.getCount() > 0) {
				for (md_c.moveToFirst(); !md_c.isAfterLast(); md_c.moveToNext()) {
					String name = md_c.getString(md_c
							.getColumnIndex(Constants.MD_NAME));
					String code = md_c.getString(md_c
							.getColumnIndex(Constants.MD_CODE));

					databaseHandler.SaveMD(code, name.toString().toUpperCase());

				}

			}

		}

		Log.i("updateMDTable-------------------", "updateMDTable");

		return true;
	}

	private void updateProgress(Integer value) {
		if (progressDialog != null && progressDialog.isShowing() == true) {
			progressDialog.setProgress(value);
			Log.i("Percent",
					String.valueOf(value) + " " + String.valueOf(itemsUpdated)
							+ " " + String.valueOf(totalNoOfItemsToBeUpdated));
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progressDialog.dismiss();

		if (result == true) {

			if (Constants.isCheckBoth == true) {
				System.out
						.println("Done Updating Medical Doctor's and Products data.");
				Log.i("Done Updating Medical Doctor's and Products data.",
						"Done Updating Medical Doctor's data.");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				// set title
				alertDialogBuilder.setTitle("Information");
				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);

				// set dialog message
				alertDialogBuilder.setMessage("Update successful.");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setNeutralButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent refresh = new Intent(context,
										RegistrationPage.class);
								context.startActivity(refresh);
								if(!(context instanceof RegistrationPage)){
									Log.i(TAG, "not on registration");
									((Activity) context).finish();
								}
								dialog.cancel();
							}
						});
				alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				Constants.isCheckBoth = false;
//				context.startActivity(new Intent(this.context,
//						RegistrationPage.class));
//
//				((Activity) context).finish();
			} 
			else if(Constants.isOnline == true){
				Constants.isOnline =false;
				Intent refresh = new Intent(context,
						RegistrationPage.class);
				context.startActivity(refresh);
				if(!(context instanceof RegistrationPage)){
					Log.i(TAG, "not on registration");
					((Activity) context).finish();
				}
				
				databaseHandler.updateInitialLogin();
			}
			
			else {
				System.out.println("Done Updating Medical Doctor's data.");
				Log.i("Done Updating Medical Doctor's data.",
						"Done Updating Medical Doctor's data.");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				// set title
				alertDialogBuilder.setTitle("Information");
				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);

				// set dialog message
				alertDialogBuilder.setMessage("Update successful.");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setNeutralButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent refresh = new Intent(context,
										RegistrationPage.class);
								context.startActivity(refresh);
								if(!(context instanceof RegistrationPage)){
									Log.i(TAG, "not on registration");
									((Activity) context).finish();
								}
								dialog.cancel();
							}
						});
				alertDialog = alertDialogBuilder.create();
				alertDialog.show();
			}
		} else {
			// System.out.println("Done Updating Medical Doctor's data.");
			// Log.i("Done Updating Medical Doctor's data.",
			// "Done Updating Medical Doctor's data.");
			//
			// AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			// context);
			// // set title
			// alertDialogBuilder.setTitle("Information");
			// // alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);
			//
			// // set dialog message
			// alertDialogBuilder
			// .setMessage("Medical Doctor's have been successfully updated.");
			// alertDialogBuilder.setCancelable(false);
			// alertDialogBuilder.setNeutralButton("OK",
			// new DialogInterface.OnClickListener() {
			// @Override
			// public void onClick(DialogInterface dialog, int id) {
			// // TODO Auto-generated method stub
			// Intent refresh = new Intent(context,
			// RegistrationPage.class);
			// context.startActivity(refresh);
			// ((Activity) context).finish();
			// dialog.cancel();
			// }
			// });
			// alertDialog = alertDialogBuilder.create();
			// alertDialog.show();
		}

	}
}