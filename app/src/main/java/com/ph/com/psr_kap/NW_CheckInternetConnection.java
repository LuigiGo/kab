package com.ph.com.psr_kap;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NW_CheckInternetConnection {
	public static boolean isConnected(Context _context) {
		boolean l_bln_haveConnectedWifi = false;
		boolean l_bln_haveConnectedMobile = false;

		ConnectivityManager l_cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] l_netInfo = l_cm.getAllNetworkInfo();
		for (NetworkInfo ni : l_netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					l_bln_haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					l_bln_haveConnectedMobile = true;
		}
		return l_bln_haveConnectedWifi || l_bln_haveConnectedMobile;
	}
}
