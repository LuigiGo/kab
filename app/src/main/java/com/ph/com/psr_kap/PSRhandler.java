package com.ph.com.psr_kap;

public class PSRhandler {
	
	private int id;
	private String psr_code, psr_name, username, password;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPsr_code() {
		return psr_code;
	}
	public void setPsr_code(String psr_code) {
		this.psr_code = psr_code;
	}
	public String getPsr_name() {
		return psr_name;
	}
	public void setPsr_name(String psr_name) {
		this.psr_name = psr_name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
