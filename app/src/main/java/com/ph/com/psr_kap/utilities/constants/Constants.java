/**
 * Created by Corporate IT for United Laboratories, Inc.
 * Created by	: Rose Jessica M. Cabigao
 * Date Created	: January 03, 2013
 * Date Updated	: January 03, 2013
 * Description	: For PSR Registration
 */
package com.ph.com.psr_kap.utilities.constants;

import android.os.Environment;

import com.ph.com.psr_kap.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Constants {

    public static int noticationCount = 0;
    public static ArrayList<String> checkPosition = new ArrayList<String>();
    public static ArrayList<String> tempRemove = new ArrayList<String>();
    public static ArrayList<HashMap<String, String>> getMDlist = new ArrayList<HashMap<String, String>>();
    public static ArrayList<HashMap<String, String>> MDlist = new ArrayList<HashMap<String, String>>();
    public static Boolean choosefilterQueryName = false;
    public static Boolean isCheckBoth = false;
    public static Boolean isOnline = false;
    public static String username, password;


    public static String TAG = "PSR Registration";
    // DATABASENAME
    // The Android's default system path of your application database.
    // public static String DATABASE_NAME = "db_PSR.sqlite";
    // public static String DATABASE_PATH = Environment.getDataDirectory() +
    // "/data/ph.com.unilab.psr/databases/";
    // public static final int DATABASE_VERSION = 1;
//	public static String SMS_RECEPIENT = "+639175526881";
    public static String SMS_RECEPIENT = "+639178038805";
    //	public static String SMS_RECEPIENT = "+639261568114";
    public static String TEXT_MESSAGE = "";
    // public static String SMS_RECEPIENT = "+639268942142";

    // LIST OF TABLE
    public static String DATABASE_TABLE_BRANDS = "tbl_Brand";
    public static String DATABASE_TABLE_MD = "tbl_MD";
    public static String DATABASE_TABLE_PATIENTS = "tbl_Patients";
    public static String DATABASE_TABLE_BRANDTRANSACTION = "tbl_BrandTransaction";
    public static String DATABASE_TABLE_PSR = "tbl_PSR";
    public static String DATABASE_TABLE_RECEPIENT = "tbl_Recipient";
    public static String DATABASE_TABLE_MDlistSelected = "tbl_MDlistSelected";
    public static String DATABASE_INITIAL_LOGIN = "tbl_InitialLogin";
    public static String TBL_MDTEMP = "tbl_MD_Temp";
    public static String TABLE_UNSENT = "tbl_unsent_registration";

    public static final String VW_TABLE_PATIENT = "vw_patient_brand";

    // COLUMN NAMES
    public static String COLUMN_ID = "_id";

    public static String COLUMN_ID2 = "rowid";

    // KEY
//	public static String KEY = "LDK";
    public static String KEY = "KAP";
    //
    public static final String SMS_MESSAGE = "sms_message";

    public static final String UNSENT_MESSAGE = "message";


    //vw_patient_brand
    public static final String VW_PATIENTCODE = "PatientCode";
    public static final String VW_PATIENT_FIRSTNAME = "FirstName";
    public static final String VW_PATIENT_LASTNAME = "LastName";
    public static final String VW_PATIENT_MOBILE = "MobileNumber";
    public static final String VW_PATIENT_PSRCODE = "PSR_Code";
    public static final String VW_PATIENT_MDCODE = "MD_Code";
    public static final String VW_PATIENT_DATEREG = "DateReg";
    public static final String VW_PATIENT_DATEUPDATED = "DateUpdated";
    public static final String VW_PATIENT_EXPORTED = "Exported";
    public static final String VW_PATIENT_LANDLINE = "LandlineNumber";
    public static final String VW_PATIENT_BRANDCODE = "BrandCode";
    public static final String VW_PATIENT_COUPONCODE = "CouponCode";
    public static final String VW_PATIENT_BRANDNAME = "BrandName";
    public static final String VW_PATIENT_KEYWORD = "Keyword";
    public static final String VW_PATIENT_MDNAME = "MD_Name";
    public static final String VW_PATIENT_PSRNAME = "PSR_Name";

    //tbl_initial
    public static String INITIAL_LOGIN = "initial_login";

    // tbl_Brand
    public static String BRAND_NAME = "BrandName";
    public static String BRAND_CODE = "BrandCode";
    public static String BRAND_KEYWORD = "Keyword";

    // tbl_MD
    public static String MD_NAME = "MD_Name";
    public static String MD_CODE = "MD_Code";

    // tbl_BrandTransaction
    public static String BT_PATIENT_CODE = "PatientCode";
    public static String BT_BRAND_CODE = "BrandCode";
    public static String BT_COUPON_CODE = "CouponCode";

    // tbl_MD_list
    public static String MDLIST_PSR_CODE = "MDLIST_PSR_CODE";
    public static String MDLIST_NAME = "MDLIST_NAME";
    public static String MDLIST_CODE = "MDLIST_CODE";

    // tbl_PSR
    public static String PSR_CODE = "PSR_Code";
    public static String PSR_NAME = "PSR_Name";
    public static String PSR_USERNAME = "Username";
    public static String PSR_PASSWORD = "Password";

    // tbl_Patient
    public static String PATIENT_CODE = "PatientCode";
    public static String PATIENT_FNAME = "FirstName";
    public static String PATIENT_LNAME = "LastName";
    public static String PATIENT_MD = "MD_Code";
    public static String PATIENT_MOBILE = "MobileNumber";
    public static String PATIENT_LANDLINE = "LandlineNumber";
    public static String PATIENT_REGDATE = "DateReg";
    public static String PATIENT_UPDATE_DATE = "DateUpdated";
    public static String PATIENT_EXPORT = "Exported";

    // tbl_brandtransaction
    public static String COUPON_CODE = "CouponCode";

    // tbl_Recipient
    public static String RECEPIENT_NAME = "Name";
    public static String RECEPIENT_EMAIL_ADDRESS = "EmailAddress";

    // sharedpreferences
    public static final String SHARED_PREFERENCES_CREATOR = "create";
    public static String SP_PSR_CODE = "PSR_Code";
    public static String g_psrCode = "";

    // public static String [] SEARCH_ATTRIBUTE_KEYS = {COLUMN_ID,
    // PARTICIPANTS_FNAME, PARTICIPANTS_TYPE, PARTICIPANTS_HOSPITAL};
    // public static int [] SEARCH_ATTRIBUTES_VIEWS = {R.id.tv_id,
    // R.id._fullname, R.id._type, R.id._hospital};

    public static final int DATE_FROM_DIALOG_ID = 0;
    public static final int DATE_TO_DIALOG_ID = 1;
    public static final int VIEW_DIALOG_ID = 2;

    public static int ADMIN_PATH = 0;
    public static int SEARCH_PATH = 0;
    public static int MDLIST_PATH = 0;
    public static boolean isUpdate = false;
    public static String UpdateID = "";
    public static String BACKUP_REGISTERED_PATIENTS = "Registered_Patients";

    public static String[] PENDING_ATTRIBUTE_KEYS = {COLUMN_ID, PATIENT_FNAME,
            PATIENT_MOBILE, PATIENT_LANDLINE, PSR_NAME, PATIENT_MD, BRAND_NAME};
    public static int[] PENDING_ATTRIBUTE_VIEWS = {R.id._id, R.id.txtName,
            R.id._mobile, R.id._landline, R.id._psr, R.id._md, R.id._brand};

    public static String[] SEARCH_ATTRIBUTE_KEYS = {COLUMN_ID, PATIENT_FNAME,
            PATIENT_MOBILE, PATIENT_MD, PSR_NAME, BRAND_NAME, COUPON_CODE};
    public static int[] SEARCH_ATTRIBUTE_VIEWS = {R.id._id, R.id.txtName,
            R.id._mobile, R.id._mdname, R.id._psrname, R.id._brand,
            R.id._coupon};

    // =============================== NEW FTP SERVER FOR PSR(KABALIKAT)
    // =============================================
    public static String path = Environment.getExternalStorageDirectory()
            .toString() + "/PSR_Registration_Form/Export/" + "Export_";

    public static String pathUsers = Environment.getExternalStorageDirectory()
            .toString() + "/Kabalikat_Advocacy_Program_2015/Users";

    // STORE FILENAME
    public static String TextFileName = "";

    // FTP ADDRESS | USERNAME | PASSWORD
    // 203.177.50.143
    // 172.16.11.27
    // 172.16.11.41
    // 172.16.11.41
    public static String FTP_ADDRESS = "203.177.50.143";
    public static String FTP_USERNAME = "ftpuser7";
    public static String FTP_PASSWORD = "FTPUSER7_BUX5PCP";

    // FTP SERVER FOLDER - FOLDER in SERVER
    public static String FTP_FOLDER_PSR_KABALIKAT_MD_CODES = "/Kabalikat_Advocacy_Program_2015/MD_Codes";
    public static String FTP_FOLDER_PSR_KABALIKAT_PRODUCTS = "/Kabalikat_Advocacy_Program_2015/Products";
    public static String FTP_FOLDER_PSR_KABALIKAT_USERS = "/Kabalikat_Advocacy_Program_2015/Users";

    // ====================== PATH from server =============================
    // C:\inetpub\ftproot\LocalUser\ftpuser7\Kabalikat_Advocacy_Program_2015\MD_Codes
    // C:\inetpub\ftproot\LocalUser\ftpuser7\Kabalikat_Advocacy_Program_2015\Products
    // C:\inetpub\ftproot\LocalUser\ftpuser7\Kabalikat_Advocacy_Program_2015\Users

    // C:\inetpub\ftproot\LocalUser\ftpuser7\Kabalikat_Advocacy_Program_2015\Users

    // =====================================================================================================


    /**
     * Broadcast Receiver Tags
     */
    public static final String SMS_RECEIVER = "com.ph.com.psr_kap.utilities.receivers.sms";
    public static final String SMS_SENT_RECEIVER = "smsSent";
    public static final String SMS_DELIVERED_RECEIVER = "smsDelivered";

    /**
     * Intent Tags
     */
    public static final String INTENT_ACTION_TAG = "actionTag";


}