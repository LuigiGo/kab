package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.CustomAdapterMDlist.AdminAdapter;
import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.dialogs.DialogManager;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminPage extends BaseActivity {

    private final static String TAG = AdminPage.class.getSimpleName();
    public CustomAdapterAdmin pendingDataAdapter;
    DatabaseHandler databaseHandler;
    //    float font_title, font_counter;
//    static float font_content;
//    Typeface tnr_normal, tnr_bold_italic, tnr_bold, monotype;
    TextView lbl_patient_total;
    boolean menu_clicked = false;
    static Button btn_MarkAll;
    Button btn_DateFrom;
    Button btn_DateTo;
    Button btn_ViewSelected;
    Button btn_Cancel;
    Button btn_menu;
    EditText txt_Search;
    // Animation anim_preview, anim_close_preview;
    RelativeLayout dropdown_menu/*, admin_page*/;
    public static ListView lv_admin_patient_list;
    private ArrayList<HashMap<String, String>> arraylist_UnexportedDetails;
    protected static boolean ismarkAll;
    public static ArrayList<String> arraylistRecordIds, arraylistRecordIds_pos,
            arraylistBackup_recordid, arraylistBackup_recordpos,
            arrayListExportedConsumer_RecId,
            arrayListExportedConsumer_RecIdpos;
    private Set<String> exportedConsumer_RecId, exportedConsumer_RecIdpos;
    private int fromYear, fromDay, fromMonth, toYear, toDay, toMonth;
    private StringBuilder fromDate = null, toDate = null;
    private Calendar calendar;

    // REX==============
    static Context context;

    // FAYE==============
    String searchMd;
    int countSelected = 0, countNotSelected = 0;
    static AdminAdapter adapter;
    public static Button btn_DoneSelecting;
    ImageView btn_Search_MD;
    public static ListView lv_mdlist;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    static TextView tvNoData;
    int isVisible;
    EditText txt_Search_MdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        /**Helper classes*/
        DialogManager.init();

        Log.i("Create AGAIN", "oncreate");

        Constants.isCheckBoth = false;
        // typeface
//        tnr_normal = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_normal.ttf");
//        tnr_bold_italic = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_bold_italic.ttf");
//        tnr_bold = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_bold.ttf");
//        monotype = Typeface.createFromAsset(getAssets(), "Monotype.TTF");

//        font_title = getResources().getDimensionPixelSize(font_title);
//        font_content = getResources().getDimensionPixelSize(
//                R.dimen.font_content);
//        font_counter = getResources().getDimensionPixelSize(
//                font_counter);
        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }

        context = this;
        new Async_Admin_Adapter(context, "", "Loading data. Please wait...")
                .execute();
        CastViews();

        // populating the listview with unexported data
        final Cursor cursorUnexported_Data = databaseHandler
                .getPendingDetails(Constants.g_psrCode);
        fillData(cursorUnexported_Data);
        show_patient_count();

        if (lv_admin_patient_list.getCount() == 0) {
            btn_MarkAll.setVisibility(View.INVISIBLE);
            isVisible = 0;
        }

        lv_admin_patient_list.setTextFilterEnabled(true);
        lv_admin_patient_list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position,
                                    long id) {
                final String ID = arraylist_UnexportedDetails.get(position)
                        .get(Constants.COLUMN_ID);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        AdminPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage(
                                "Are you sure you want to delete this record?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        databaseHandler.deleteRecord(ID);
                                        Toast.makeText(getApplicationContext(),
                                                "Deleted", Toast.LENGTH_SHORT)
                                                .show();
                                        refresh();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

        });
    }

    public static void lv_mdlist(ArrayList<HashMap<String, String>> mdList) {
        adapter = new AdminAdapter(context, R.layout.mdlist_item, mdList);
        lv_mdlist.setAdapter(adapter);
        lv_mdlist.setItemsCanFocus(false);
        lv_mdlist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        Log.i("lv_mdlist", "" + mdList.size());
        if (mdList.size() == 0) {
//            lv_mdlist.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }

    }

    // @Override
    // protected void onResume() {
    // // TODO Auto-generated method stub
    // if(adapter!=null){
    // adapter.notifyDataSetChanged();
    // }
    //
    // super.onResume();
    // }

    // private void fillData(Cursor cursor) {
    //
    // arraylist_UnexportedDetails.clear();
    // int unexported_Data = cursor.getCount();
    // Log.i("", String.valueOf(unexported_Data));
    // HashMap<String, String> hashmap = null;
    // if(unexported_Data != 0){
    // List<String> arrayList_brandcode = new ArrayList<String>();
    // ArrayList<String> arrayList_brandname = new ArrayList<String>();
    // for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
    // hashmap = new HashMap<String, String>();
    // arrayList_brandcode.clear();
    // arrayList_brandname.clear();
    //
    // String _id =
    // cursor.getString(cursor.getColumnIndex(Constants.COLUMN_ID));
    // String patient_code =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_CODE));
    // String fname =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_FNAME));
    // String lname =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_LNAME));
    // String mobile =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_MOBILE));
    // String landline =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_LANDLINE));
    // String md_code =
    // cursor.getString(cursor.getColumnIndex(Constants.PATIENT_MD));
    // Log.i(Constants.TAG, md_code);
    // String psr_code =
    // cursor.getString(cursor.getColumnIndex(Constants.PSR_CODE));
    //
    // String psr_name = databaseHandler.get_psr_name(psr_code);
    // String md_name = databaseHandler.get_md_name(md_code);
    // String full_name = lname.toUpperCase() + ", " + fname.toUpperCase();
    //
    // Log.i("==========_id " + full_name, _id);
    //
    // //arrayList_brandcode =
    // databaseHandler.get_brands_prescribed(patient_code);
    // Cursor s = databaseHandler.get_brands_prescribed(patient_code);
    // for (s.moveToFirst(); !s.isAfterLast(); s.moveToNext()) {
    // arrayList_brandcode.add(s.getString(s.getColumnIndex(Constants.BRAND_CODE)));
    // }
    //
    // Log.i("arrayList_brandcode " + full_name,
    // arrayList_brandcode.toString());
    // for (int i = 0; i < arrayList_brandcode.size(); i++) {
    // String brand_name =
    // databaseHandler.get_brand_name(arrayList_brandcode.get(i));
    // arrayList_brandname.add(brand_name);
    // }
    // String brands = arrayList_brandname.toString().replace("[",
    // "").replace("]", "");
    //
    // Log.i("Brand", brands);
    // hashmap.put(Constants.COLUMN_ID, _id);
    // hashmap.put(Constants.PATIENT_FNAME, full_name);
    // hashmap.put(Constants.PATIENT_MOBILE, mobile);
    // hashmap.put(Constants.PATIENT_LANDLINE, landline);
    // hashmap.put(Constants.PSR_NAME, psr_name);
    // hashmap.put(Constants.PATIENT_MD, md_name);
    // hashmap.put(Constants.BRAND_NAME, brands);
    // arraylist_UnexportedDetails.add(hashmap);
    // }
    //
    // cursor.close();
    // pendingDataAdapter = new CustomAdapterAdmin(AdminPage.this,
    // arraylist_UnexportedDetails,
    // R.layout.pending_attributecell, Constants.PENDING_ATTRIBUTE_KEYS,
    // Constants.PENDING_ATTRIBUTE_VIEWS, true);
    // lv_admin_patient_list.setAdapter(pendingDataAdapter);
    // // pendingDataAdapter.notifyDataSetChanged();
    //
    // }
    //
    // }

    private void fillData(Cursor cursor) {

        arraylist_UnexportedDetails.clear();
        int unexported_Data = cursor.getCount();

        List<String> arrayList_brandcode = new ArrayList<String>();
        ArrayList<String> arrayList_brandname = new ArrayList<String>();

        HashMap<String, String> hashmap = null;

        LoggerHelper.log(TAG, String.valueOf(unexported_Data));
        if (unexported_Data != 0) {
            cursor.moveToFirst();

            // get the first patientCode
            // currentPatientCode = cursor.getString(cursor
            // .getColumnIndex(Constants.VW_PATIENTCODE));

            ArrayList<String> arr_patientCode = new ArrayList<String>();
            ArrayList<String> list_brandCode = new ArrayList<String>();


            do {
                String patient_code = cursor.getString(cursor
                        .getColumnIndex(Constants.VW_PATIENTCODE));

                if (!arr_patientCode.contains(patient_code)) {
                    //refresh list_brandCode for different patient code
                    list_brandCode = new ArrayList<String>();

                    arr_patientCode.add(patient_code);

                    hashmap = new HashMap<String, String>();

                    String _id = cursor.getString(cursor
                            .getColumnIndex(Constants.COLUMN_ID));
                    String fname = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_FIRSTNAME));
                    String lname = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_LASTNAME));
                    String mobile = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_MOBILE));
                    String landline = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_LANDLINE));
                    String md_code = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_MDCODE));

                    String psr_code = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_PSRCODE));

                    String psr_name = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_PSRNAME));
                    String md_name = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_MDNAME));
                    String full_name = lname.toUpperCase() + ", "
                            + fname.toUpperCase();

                    String brandCode = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_BRANDCODE));
                    String brandName = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_BRANDNAME));

                    //check for duplicate entries
                    if (!list_brandCode.contains(brandCode)) {
                        list_brandCode.add(brandCode);

//                        Log.i("Brand", brandName);
                        hashmap.put(Constants.COLUMN_ID, _id);
                        hashmap.put(Constants.PATIENT_FNAME, full_name);
                        hashmap.put(Constants.PATIENT_MOBILE, mobile);
                        hashmap.put(Constants.PATIENT_LANDLINE, landline);
                        hashmap.put(Constants.PSR_NAME, psr_name);
                        hashmap.put(Constants.PATIENT_MD, md_name);
                        hashmap.put(Constants.BRAND_NAME, brandName);
                        hashmap.put(Constants.PATIENT_CODE, patient_code);


                        arraylist_UnexportedDetails.add(hashmap);
                    }

                } else {
                    //check for duplicate entries
                    String brandCode = cursor.getString(cursor
                            .getColumnIndex(Constants.VW_PATIENT_BRANDCODE));

                    if (!list_brandCode.contains(brandCode)) {
                        list_brandCode.add(brandCode);

                        int index = arr_patientCode.indexOf(patient_code);
                        hashmap = arraylist_UnexportedDetails.get(index);
                        String brand = hashmap.get(Constants.BRAND_NAME);
                        String newBrand = brand
                                + ", "
                                + cursor.getString(cursor
                                .getColumnIndex(Constants.VW_PATIENT_BRANDNAME));
                        hashmap.put(Constants.BRAND_NAME, newBrand);

                    }


                }

                //
                // arraylist_UnexportedDetails.add(hashmap);

                // }

            } while (cursor.moveToNext());

            cursor.close();

            pendingDataAdapter = new CustomAdapterAdmin(AdminPage.this,
                    arraylist_UnexportedDetails,
                    R.layout.pending_attributecell,
                    Constants.PENDING_ATTRIBUTE_KEYS,
                    Constants.PENDING_ATTRIBUTE_VIEWS, true);


            lv_admin_patient_list.setAdapter(pendingDataAdapter);
            // pendingDataAdapter.notifyDataSetChanged();

        }

    }

    private void CastViews() {
        // anim_preview = AnimationUtils.loadAnimation(this,
        // R.anim.anim_preview);
        // anim_close_preview = AnimationUtils.loadAnimation(this,
        // R.anim.anim_close_preview);

        // FAYE========
        lv_mdlist = (ListView) findViewById(R.id.listview_mdlist);
        txt_Search_MdList = (EditText) findViewById(R.id.txt_Search_MdList);
        TextView lbl_search_md = (TextView) findViewById(R.id.lbl_search_doctor);
        btn_Search_MD = (ImageView) findViewById(R.id.btn_search_md);
        btn_DoneSelecting = (Button) findViewById(R.id.btn_mdlist_done);

        tvNoData = (TextView) findViewById(R.id.tvNoData);

//        txt_Search_MdList.setTypeface(tnr_normal);
//        txt_Search_MdList.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        btn_DoneSelecting.setTypeface(tnr_bold);
//        btn_DoneSelecting.setTextSize(TypedValue.COMPLEX_UNIT_SP, font_title);
//
//        lbl_search_md.setTypeface(tnr_normal);
//        lbl_search_md.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);

        calendar = Calendar.getInstance();
        lv_admin_patient_list = (ListView) findViewById(R.id.lv_admin_patient_list);

        dropdown_menu = (RelativeLayout) findViewById(R.id.dropdown_menu);
//        admin_page = (RelativeLayout) findViewById(R.id.admin_page);

        txt_Search = (EditText) findViewById(R.id.txt_Search);
        txt_Search.addTextChangedListener(filterTextWatcher);

        // textviews
        lbl_patient_total = (TextView) findViewById(R.id.lbl_patient_total);

        TextView lbl_search = (TextView) findViewById(R.id.lbl_search);
        TextView lbl_searchbydate = (TextView) findViewById(R.id.lbl_searchbydate);

        // button btn_DateFrom, btn_DateTo, btn_ViewSelected, btn_Cancel;
        btn_DateFrom = (Button) findViewById(R.id.btn_DateFrom);
        btn_MarkAll = (Button) findViewById(R.id.btn_MarkAll);
        btn_DateTo = (Button) findViewById(R.id.btn_DateTo);
        btn_ViewSelected = (Button) findViewById(R.id.btn_ViewSelected);
        btn_Cancel = (Button) findViewById(R.id.btn_Cancel);
        btn_menu = (Button) findViewById(R.id.btn_menu);

//        txt_Search.setTypeface(tnr_normal);
//        txt_Search.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        tvNoData.setTypeface(tnr_normal);
//        tvNoData.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        btn_DateFrom.setTypeface(tnr_bold);
//        btn_DateFrom.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        btn_MarkAll.setTypeface(tnr_bold);
//        btn_MarkAll.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        btn_DateTo.setTypeface(tnr_bold);
//        btn_DateTo.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        btn_ViewSelected.setTypeface(tnr_bold);
//        btn_ViewSelected.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        btn_Cancel.setTypeface(tnr_bold);
//        btn_Cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//
//        lbl_search.setTypeface(tnr_normal);
//        lbl_search.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        lbl_searchbydate.setTypeface(tnr_normal);
//        lbl_searchbydate.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//
//        lbl_patient_total.setTypeface(tnr_bold);
//        lbl_patient_total.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_counter);

        arraylist_UnexportedDetails = new ArrayList<HashMap<String, String>>();

        arraylistRecordIds = new ArrayList<String>();
        arraylistRecordIds_pos = new ArrayList<String>();
        arraylistBackup_recordid = new ArrayList<String>();
        arraylistBackup_recordpos = new ArrayList<String>();

        arrayListExportedConsumer_RecId = new ArrayList<String>();
        arrayListExportedConsumer_RecIdpos = new ArrayList<String>();

//        admin_page.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Log.i(Constants.TAG, "touch");
//                menu_clicked = false;
//                dropdown_menu.setVisibility(View.GONE);
//            }
//        });

    }

    private void show_patient_count() {
        String queryCount = "SELECT COUNT(*) FROM tbl_Patients WHERE "
                + Constants.PSR_CODE + " = " + Constants.g_psrCode;

        Cursor dataCount = databaseHandler.getCodeCount(queryCount);
        int count = dataCount.getInt(0);
        if (count <= 1) {

            lbl_patient_total.setText("Registered patient: \t"
                    + String.valueOf(lv_admin_patient_list.getCount()));

        } else if (count > 1) {
            lbl_patient_total.setText("Registered patient: \t"
                    + String.valueOf(lv_admin_patient_list.getCount()));
        }

    }

    public void Check_Uncheck() {
        if (!ismarkAll) {
            ismarkAll = true;
            btn_MarkAll.setText("Unmark All");

        } else {
            ismarkAll = false;
            btn_MarkAll.setText("Mark All");
        }

        for (int i = 0; i < arraylist_UnexportedDetails.size(); i++) {
            if (ismarkAll
                    && !arraylistRecordIds.contains(arraylist_UnexportedDetails
                    .get(i).get(Constants.COLUMN_ID).toString())) {
                arraylistRecordIds.add(arraylist_UnexportedDetails.get(i)
                        .get(Constants.COLUMN_ID).toString());
                arraylistRecordIds_pos.add(arraylist_UnexportedDetails.get(i)
                        .get(Constants.PATIENT_CODE).toString());
                Log.i("Main ismarkAll 1", String.valueOf(ismarkAll));
            } else if (!ismarkAll
                    && arraylistRecordIds.contains(arraylist_UnexportedDetails
                    .get(i).get(Constants.COLUMN_ID).toString())) {
                arraylistRecordIds.remove(arraylist_UnexportedDetails.get(i)
                        .get(Constants.COLUMN_ID).toString());
                arraylistRecordIds_pos.remove(arraylist_UnexportedDetails.get(i)
                        .get(Constants.PATIENT_CODE).toString());
                Log.i("Main ismarkAll 2", String.valueOf(ismarkAll));
            }
        }

        pendingDataAdapter = new CustomAdapterAdmin(AdminPage.this,
                arraylist_UnexportedDetails, R.layout.pending_attributecell,
                Constants.PENDING_ATTRIBUTE_KEYS,
                Constants.PENDING_ATTRIBUTE_VIEWS, ismarkAll);
        lv_admin_patient_list.setAdapter(pendingDataAdapter);
    }

    @SuppressWarnings("deprecation")
    public void onButtonClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_mdlist_done:
                Log.i("clicked", "btn_mdlist_done");
                DoneSelectingMDlist();
                // adapter.notifyDataSetChanged();
                // adapter.unCheckAll();

                Constants.checkPosition = new ArrayList<String>();
                Constants.tempRemove = new ArrayList<String>();
                break;

            case R.id.btn_search_md:
                Log.i("clicked", "btn_mdlist_done");

                lv_mdlist.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
                searchMd = txt_Search_MdList.getText().toString();
                if (searchMd == null && Constants.getMDlist == null) {
                    Toast.makeText(AdminPage.this, "Please insert text.",
                            Toast.LENGTH_SHORT).show();
                } else {
                    new Async_Admin_Adapter(context, searchMd,
                            "Searching records. Plese wait...").execute();
                    txt_Search_MdList.setText("");
                }
                adapter.notifyDataSetChanged();
                break;

            case R.id.btn_MarkAll:
                Log.i("clicked", "btn_MarkAll");
                Check_Uncheck();
                break;

            case R.id.btn_DateFrom:
                Log.i("clicked", "btn_DateFrom");
                showDialog(Constants.DATE_FROM_DIALOG_ID);
                break;

            case R.id.btn_DateTo:
                Log.i("clicked", "btn_DateTo");
                showDialog(Constants.DATE_TO_DIALOG_ID);
                break;

            case R.id.btn_ViewSelected:
                Log.i("clicked", "btn_ViewSelected");
                // if(isVisible == 1){

                Log.i(TAG, "record: " + arraylistRecordIds.size());
                Log.i(TAG, "listview count: " + lv_admin_patient_list.getCount());
                if (arraylistRecordIds.size() > 0
                        && lv_admin_patient_list.getCount() > 0) {

                    Export();

                } else {
                    Log.i("jessmark", "1");
                    Toast.makeText(getApplicationContext(),
                            "Please select registered patient to export. ", Toast.LENGTH_SHORT)
                            .show();
                }
                // }else{
                // Log.i("jessmark", "2");
                // Toast.makeText(getApplicationContext(),
                // "Please select registered patient to export. ", 8).show();
                // }

                break;

            case R.id.btn_Cancel:
                Log.i("clicked", "btn_Cancel");
                onBackPressed();
                break;

            case R.id.btn_menu:
                if (menu_clicked == false) {
                    menu_clicked = true;
                    dropdown_menu.setVisibility(View.VISIBLE);
                    // dropdown_menu.startAnimation(anim_preview);
                } else if (menu_clicked == true) {
                    menu_clicked = false;
                    // dropdown_menu.startAnimation(anim_close_preview);
                    // anim_close_preview.setAnimationListener(new
                    // AnimationListener() {
                    //
                    // @Override
                    // public void onAnimationStart(Animation animation) {
                    //
                    // }
                    //
                    // @Override
                    // public void onAnimationRepeat(Animation animation) {
                    //
                    // }
                    //
                    // @Override
                    // public void onAnimationEnd(Animation animation) {
                    // dropdown_menu.setVisibility(View.GONE);
                    // dropdown_menu.clearAnimation();
                    // }
                    // });
                }
                break;

            case R.id.menu_btn_search:
                Intent search = new Intent(AdminPage.this, SearchPage.class);
                startActivity(search);
                finish();
                Constants.SEARCH_PATH = 2;
                break;

            case R.id.menu_btn_update:
//                dialog_for_MenuUpdate();
                DialogManager.getInstance().showUpdateDialog(this);

                break;

            case R.id.menu_btn_mdlist:
                Intent k = new Intent(AdminPage.this, MdlistPage.class);
                startActivity(k);
                finish();
                Constants.MDLIST_PATH = 2;
                break;

            case R.id.menu_btn_register:
                Intent j = new Intent(AdminPage.this, RegistrationPage.class);
                startActivity(j);
                finish();
                break;


            case R.id.menu_btn_updateRecepient:
//                updateRecepientDialog();
                DialogManager.getInstance().showEmailSettingDialog(this, databaseHandler);
                break;

            case R.id.menu_btn_logout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        AdminPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to log out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Intent intent = new Intent(AdminPage.this,
                                                Login.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }

    }

    private void DoneSelectingMDlist() {
        Log.i("AdminPage", "size" + Constants.getMDlist.size());
        // if(Constants.getMDlist.size() == 0){
        // Toast.makeText(AdminPage.this, "Please select Medical Doctors",
        // Toast.LENGTH_SHORT).show();
        // }
        // else{

        boolean isAddedNew = false;

        if (databaseHandler.countSelectedMDList(Constants.g_psrCode) == 0
                && Constants.getMDlist.size() == 0) {
            AlertDialog alertDialog = new AlertDialog.Builder(AdminPage.this)
                    .create();
            alertDialog.setTitle("Information");
            alertDialog
                    .setMessage("There are no changes made. Note: You can select or diselect MDs you prefer by tapping the box beside the MD name.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });
            alertDialog.show();
            Log.i("MessageCollector:", "Already Selected");
        } else {

            boolean isAllCheckedMDIsOnSelectedMDList = false;
            for (int i = 0; i < Constants.getMDlist.size(); i++) {

                if (!databaseHandler.checkIfInMDLIST(Constants.getMDlist.get(i)
                        .get(Constants.MD_NAME), Constants.g_psrCode)) {
                    isAllCheckedMDIsOnSelectedMDList = false;

                } else {
                    isAllCheckedMDIsOnSelectedMDList = true;
                }

                Log.i("jessmark", "isAllCheckedMDIsOnSelectedMDList: "
                        + isAllCheckedMDIsOnSelectedMDList);

            }

            if (Constants.getMDlist.size() != databaseHandler
                    .countSelectedMDList(Constants.g_psrCode)) {
                isAllCheckedMDIsOnSelectedMDList = false;
            }

            if (Constants.getMDlist.size() > databaseHandler
                    .countSelectedMDList(Constants.g_psrCode)) {
                isAddedNew = true;
            }

            // delete all record
            databaseHandler.deleteRecordSelected(Constants.g_psrCode);

            // add new record
            for (int i = 0; i < Constants.getMDlist.size(); i++) {
                String md_code = Constants.getMDlist.get(i).get(
                        Constants.MD_CODE);
                String md_name = Constants.getMDlist.get(i).get(
                        Constants.MD_NAME);
                String md_psr_code = Constants.g_psrCode;

                if (databaseHandler.isMDAlreadySelected(md_code) == true) {
                    countSelected += 1;
                } else {
                    countNotSelected += 1;
                }

                databaseHandler.StoreMDlist(md_psr_code, md_code, md_name);

                // }
            }

            if (countSelected > 0 && countNotSelected == 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        AdminPage.this).create();
                alertDialog.setTitle("Warning");

                if (isAllCheckedMDIsOnSelectedMDList == true) {
                    alertDialog
                            .setMessage("There are no changes made. Note: You can select or diselect MDs you prefer by tapping the box beside the MD name.");
                } else {
                    alertDialog
                            .setMessage("The selected Medical Doctor is already chosen.");

                }

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                new Async_Admin_Adapter(context, "",
                                        "Loading data. Please wait...")
                                        .execute();
                            }
                        });
                alertDialog.show();
                Log.i("MessageCollector:", "Already Selected");
            } else if (countSelected == 0 && countNotSelected > 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        AdminPage.this).create();
                alertDialog.setTitle("Information");

                if (isAllCheckedMDIsOnSelectedMDList == true) {
                    alertDialog
                            .setMessage("There are no changes made. Note: You can select or diselect MDs you prefer by tapping the box beside the MD name.");
                } else {

                    if (isAddedNew) {

                        alertDialog
                                .setMessage("Selected MDs have been successfully added on your MD List.");
                    } else {
                        alertDialog.setMessage("MD List has been updated.");
                    }

                }

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                new Async_Admin_Adapter(context, "",
                                        "Loading data. Please wait...")
                                        .execute();
                            }
                        });
                alertDialog.show();
                Log.i("MessageCollector:", "Successfully Selected");
                // Toast.makeText(AdminPage.this, "Successfully selected!",
                // Toast.LENGTH_SHORT).show();
                // Log.i("MessageCollector:", "Add to database");
            } else if (countSelected > 0 && countNotSelected > 0) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        AdminPage.this).create();
                alertDialog.setTitle("Warning");

                if (isAllCheckedMDIsOnSelectedMDList == true) {
                    alertDialog
                            .setMessage("There are no changes made. Note: You can select or diselect MDs you prefer by tapping the box beside the MD name.");
                } else {
                    alertDialog
                            .setMessage("Some MDs you checked were already selected by another PSR.");

                }

                // alertDialog.setMessage("Some chosen medical doctors are already selected by different PSR.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                new Async_Admin_Adapter(context, "",
                                        "Loading data. Please wait...")
                                        .execute();
                            }
                        });
                alertDialog.show();
                Log.i("MessageCollector:", "Selected and not selected by PSR");
            } else {

                AlertDialog alertDialog = new AlertDialog.Builder(
                        AdminPage.this).create();
                alertDialog.setTitle("Information");

                if (isAddedNew) {

                    alertDialog
                            .setMessage("Selected MDs have been successfully added on your MD List.");
                } else {
                    alertDialog.setMessage("MD List has been updated.");
                }

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                                new Async_Admin_Adapter(context, "",
                                        "Loading data. Please wait...")
                                        .execute();
                            }
                        });
                alertDialog.show();
                Log.i("MessageCollector:", "Already Selected");
                // Toast.makeText(AdminPage.this, "Successfully selected!",
                // Toast.LENGTH_SHORT).show();
                // Log.i("MessageCollector:", "Else condition");
            }

            countSelected = 0;
            countNotSelected = 0;

        }

    }

    // text watcher for the name search
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            filterQueryName(s.toString());
        }
    };

    // used for search
    private void filterQueryName(String _constraint) {
        Cursor c;

        if (_constraint.equals("") || _constraint.equals(null))
            c = databaseHandler.getPendingDetails(Constants.g_psrCode);
        else
            c = databaseHandler.getPendingDetails(_constraint, Constants.g_psrCode);

        // startManagingCursor(c);
        fillData(c);

        /**Temporary code*/
        Log.e(TAG, "filterQueryName: " + (pendingDataAdapter == null));
        try {
            pendingDataAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            LoggerHelper.log(TAG, " " + e.getMessage());
        }

        if (lv_admin_patient_list.getCount() == 0) {
            btn_MarkAll.setVisibility(View.INVISIBLE);
            isVisible = 0;

        } else {
            btn_MarkAll.setVisibility(View.VISIBLE);
            isVisible = 1;
        }
    }

    @SuppressWarnings("deprecation")
    protected Dialog onCreateDialog(int id) {
        DatePickerDialog dialog = null;
        switch (id) {
            case Constants.DATE_FROM_DIALOG_ID:

                calendar = Calendar.getInstance();
                fromYear = calendar.get(Calendar.YEAR);
                fromMonth = calendar.get(Calendar.MONTH);
                fromDay = calendar.get(Calendar.DAY_OF_MONTH);

                dialog = new DatePickerDialog(this, mDateFromSetListener, fromYear,
                        fromMonth, fromDay);

                // dialog.updateDate(calendar.get(Calendar.YEAR),
                // calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.setButton3("Set Default",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                fromDate = null;
                                Cursor cursorNotExported = databaseHandler
                                        .getAllUnexprotedDetails(Constants.g_psrCode);
                                fillData(cursorNotExported);

                                btn_DateFrom.setText(getString(R.string.from));
                                if (lv_admin_patient_list.getCount() == 0) {
                                    Toast.makeText(AdminPage.this,
                                            "No data found.", Toast.LENGTH_SHORT)
                                            .show();
                                    btn_MarkAll.setVisibility(View.INVISIBLE);
                                    isVisible = 0;
                                } else {
                                    btn_MarkAll.setVisibility(View.VISIBLE);
                                    isVisible = 1;

                                }

                            }
                        });

                return dialog;

            case Constants.DATE_TO_DIALOG_ID:

                calendar = Calendar.getInstance();
                toYear = calendar.get(Calendar.YEAR);
                toMonth = calendar.get(Calendar.MONTH);
                toDay = calendar.get(Calendar.DAY_OF_MONTH);

                dialog = new DatePickerDialog(this, mDateToSetListener, toYear,
                        toMonth, toDay);
                calendar = Calendar.getInstance();
                // dialog.updateDate(calendar.get(Calendar.YEAR),
                // calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.setButton3("Set Default",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                toDate = null;
                                Cursor cursorNotExported = databaseHandler
                                        .getAllUnexprotedDetails(Constants.g_psrCode);
                                fillData(cursorNotExported);
                                btn_DateTo.setText(getString(R.string.to));
                                if (lv_admin_patient_list.getCount() == 0) {
                                    Toast.makeText(AdminPage.this,
                                            "No data found.", Toast.LENGTH_SHORT)
                                            .show();
                                    btn_MarkAll.setVisibility(View.INVISIBLE);
                                    isVisible = 0;
                                } else {
                                    btn_MarkAll.setVisibility(View.VISIBLE);
                                    isVisible = 1;

                                }

                            }
                        });
                return dialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateFromSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            fromYear = year;
            fromMonth = monthOfYear;
            fromDay = dayOfMonth;
            updateDisplay(btn_DateFrom);

            if (lv_admin_patient_list.getCount() == 0) {
                Toast.makeText(AdminPage.this, "No data found.",
                        Toast.LENGTH_SHORT).show();
                btn_MarkAll.setVisibility(View.INVISIBLE);
                isVisible = 0;
            } else {
                pendingDataAdapter.notifyDataSetChanged();
                btn_MarkAll.setVisibility(View.VISIBLE);
                isVisible = 1;
            }
        }
    };

    private DatePickerDialog.OnDateSetListener mDateToSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            toYear = year;
            toMonth = monthOfYear;
            toDay = dayOfMonth;
            updateDisplay(btn_DateTo);

            if (lv_admin_patient_list.getCount() == 0) {
                Toast.makeText(AdminPage.this, "No data found.",
                        Toast.LENGTH_SHORT).show();
                btn_MarkAll.setVisibility(View.INVISIBLE);
                isVisible = 0;
            } else {
                pendingDataAdapter.notifyDataSetChanged();
                btn_MarkAll.setVisibility(View.VISIBLE);
                isVisible = 1;
            }
        }
    };

    private void updateDisplay(Button button) {
        Cursor cursorFilterDate = null;

        if (button.getId() == R.id.btn_DateFrom) {
            fromDate = new StringBuilder();
            // Month is 0 based so add 1
            fromDate.append(fromYear).append("-");
            if (fromMonth < 9) {
                fromDate.append("0").append(fromMonth + 1).append("-");
            } else {
                fromDate.append(fromMonth + 1).append("-");
            }

            if (fromDay < 10) {
                fromDate.append("0").append(fromDay).append("");
            } else {
                fromDate.append(fromDay).append("");
            }
            button.setText(fromDate);
        } else if (button.getId() == R.id.btn_DateTo) {
            toDate = new StringBuilder();
            // Month is 0 based so add 1
            toDate.append(toYear).append("-");
            if (toMonth < 9) {
                toDate.append("0").append(toMonth + 1).append("-");
            } else {
                toDate.append(toMonth + 1).append("-");
            }

            if (toDay < 10) {
                toDate.append("0").append(toDay).append("");
            } else {
                toDate.append(toDay).append("");
            }
            button.setText(toDate);
        }

        if (fromDate != null && toDate != null) {
            cursorFilterDate = databaseHandler
                    .getCursorFilterDateFromAndTo(fromDate.toString(),
                            toDate.toString(), Constants.g_psrCode);
            filterQueryDate(cursorFilterDate);
        } else if (fromDate != null && toDate == null) {
            cursorFilterDate = databaseHandler.getCursorFilterDateFrom(
                    fromDate.toString(), Constants.g_psrCode);
            filterQueryDate(cursorFilterDate);
        } else if (fromDate == null && toDate != null) {
            cursorFilterDate = databaseHandler.getCursorFilterDateTo(
                    toDate.toString(), Constants.g_psrCode);
            filterQueryDate(cursorFilterDate);
        }

    }

    @SuppressWarnings("deprecation")
    private void filterQueryDate(Cursor date) {
        Cursor c;

        if (date == null) {
            c = databaseHandler.getPendingDetails(Constants.g_psrCode);
        } else {
            c = date;
        }

        startManagingCursor(c);
        fillData(c);

        String startDate = btn_DateFrom.getText().toString();
        String endDate = btn_DateTo.getText().toString();

        boolean correctDate = checkDates(startDate, endDate);
        Log.i("Result", String.valueOf(correctDate));

        if (!startDate.equals("From") && !endDate.equals("To")) {
            if (correctDate == false)
                Toast.makeText(getApplicationContext(),
                        "Invalid date parameter.", Toast.LENGTH_LONG).show();
        }

    }

    private boolean checkDates(String date1, String date2) {
        boolean b = false;
        try {
            String myFormatString = "yyyy-MM-dd"; // for example
            SimpleDateFormat df = new SimpleDateFormat(myFormatString);
            Date startDate = df.parse(date1);
            Date endDate = df.parse(date2);

            if (startDate.before(endDate)) {
                b = true; // If start date is before end date
            } else if (startDate.equals(endDate)) {
                b = true; // If two dates are equal
            } else {
                b = false; // If start date is after the end date
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return b;
    }

    @SuppressWarnings("unused")
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        if (Constants.ADMIN_PATH == 1) {
            Intent i = new Intent(AdminPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        } else if (Constants.ADMIN_PATH == 2) {
            Intent search = new Intent(AdminPage.this, SearchPage.class);
            startActivity(search);
            finish();
        } else if (Constants.ADMIN_PATH == 3) {
            Intent mdlist = new Intent(AdminPage.this, MdlistPage.class);
            startActivity(mdlist);
            finish();
        } else {
            Intent i = new Intent(AdminPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        }

        Constants.checkPosition = new ArrayList<String>();
        Constants.tempRemove = new ArrayList<String>();

    }

    private void Export() {
        final int sdkVersion = Build.VERSION.SDK_INT;

        boolean hasSDCard = false;
        boolean sdCardIsWriteable = true;
        String sdCardState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(sdCardState))
            hasSDCard = true;
        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(sdCardState))
            sdCardIsWriteable = true;

        if (sdkVersion < Build.VERSION_CODES.HONEYCOMB) {
            Log.i("HONEYCOMB", "" + sdkVersion);
            new AsyncCreateBackup().execute();
        }

        if (hasSDCard == true && sdCardIsWriteable == true) {
            new AsyncCreateBackup().execute();
        } else {
            Toast.makeText(AdminPage.this, getString(R.string.error_sdcard),
                    Toast.LENGTH_SHORT).show();
        }
    }

    // =======================================================================================
    private class AsyncCreateBackup extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;

        protected void onPreExecute() {
            progressDialog = new ProgressDialog(AdminPage.this);
            progressDialog.setMessage("Exporting data...");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(Void... unused) {
            return createBackup();
        }

        @Override
        protected void onProgressUpdate(Void... unused) {
        }

        @Override
        protected void onPostExecute(String result) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (result == null) {
                Toast.makeText(AdminPage.this,
                        getString(R.string.success_backup), Toast.LENGTH_SHORT)
                        .show();
                // populating the listview with unexported data
                refresh();
            } else {
                Toast.makeText(AdminPage.this,
                        getString(R.string.error_backup), Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    private String createBackup() {
        try {
            String column_header = "First Name," + "Last Name,"
                    + "Mobile Number," + "Landline Number," + "MD's Name,"
                    + "PSR Name," + "Brands," + "Coupon Code";


            createCSV(Constants.BACKUP_REGISTERED_PATIENTS, column_header,
                    getRegisteredDetails(), true);

            Log.i("jessmark", getRegisteredDetails());


            if (arraylistRecordIds != null) {
                int l = 0;
                while (arraylistRecordIds.size() > l) {
                    String recordId = arraylistRecordIds.get(l);
                    Log.i("recordId", recordId);
                    databaseHandler.updateExportedmark(recordId);
                    l++;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return null;
    }

//	private String getRegisteredDetails(
//			ArrayList<String> _arraylistRegistered_Id) {
//		StringBuilder _details = new StringBuilder();
//		if (_arraylistRegistered_Id != null) {
//			List<String> arrayList_brandcode = new ArrayList<String>();
//			ArrayList<String> arrayList_brandname = new ArrayList<String>();
//			List<String> arrayList_couponcode = new ArrayList<String>();
//			int i = 0;
//			while (_arraylistRegistered_Id.size() > i) {
//				arrayList_brandcode.clear();
//				arrayList_brandname.clear();
//				arrayList_couponcode.clear();
//				String _id = _arraylistRegistered_Id.get(i).toString();
//				Log.d("getRegisteredDetails", String.valueOf(_id));
//				Cursor cursorDetails = databaseHandler
//						.getCursorDetailsById(_id);
//
//				String _patientcode = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_CODE));
//				String _lastname = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_LNAME));
//				String _firstname = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_FNAME));
//				String _mobile = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_MOBILE));
//				String _landline = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_LANDLINE));
//				String _mdcode = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PATIENT_MD));
//				String _psrcode = cursorDetails.getString(cursorDetails
//						.getColumnIndex(Constants.PSR_CODE));
//
//				String _psrname = '"' + databaseHandler.get_psr_name(_psrcode) + '"';
//				String _mdname = '"' + databaseHandler.get_md_name(_mdcode) + '"';
//
//				// arrayList_brandcode =
//				// databaseHandler.get_brands_prescribed(_patientcode);
//
//				// arrayList_brandcode =
//				// databaseHandler.get_brands_prescribed(patient_code);
//				Cursor s = databaseHandler.get_brands_prescribed(_patientcode);
//				int s_count = s.getCount();
//				Log.i("s_count", String.valueOf(s_count));
//				for (s.moveToFirst(); !s.isAfterLast(); s.moveToNext()) {
//					arrayList_brandcode.add(s.getString(s
//							.getColumnIndex(Constants.BRAND_CODE)));
//				}
//
//				for (int j = 0; j < arrayList_brandcode.size(); j++) {
//					String brand_name = databaseHandler
//							.get_brand_name(arrayList_brandcode.get(j));
//
//					arrayList_brandname.add(brand_name);
//				}
//				String brands = arrayList_brandname.toString().replace("[", "")
//						.replace("]", "").replace(",", "/");
//				arrayList_couponcode = databaseHandler
//						.get_coupon_code(_patientcode);
//				String _couponcode = arrayList_couponcode.toString()
//						.replace("[", "").replace("]", "").replace(",", "/");
//
//				String column_header = "First Name," + "Last Name,"
//						+ "Mobile Number," + "Landline Number" + "MD's Name,"
//						+ "PSR Name," + "Brands," + "Coupon Code";
//
//				_details.append("\n" + checkIfNull(_firstname) + ","
//						+ checkIfNull(_lastname) + "," + checkIfNull(_mobile)
//						+ "," + checkIfNull(_landline) + ","
//						+ checkIfNull(_mdname) + "," + checkIfNull(_psrname)
//						+ "," + checkIfNull(brands) + ","
//						+ checkIfNull(_couponcode));
//				i++;
//			}
//		}
//
//		return _details.toString();
//	}

    //editting...
    private String getRegisteredDetails() {
        ArrayList<HashMap<String, String>> listToBeExported = new ArrayList<HashMap<String, String>>();
        StringBuilder _details = new StringBuilder();

        Cursor cursorDetails = databaseHandler
                .getRegisteredPatientDetails(Constants.g_psrCode);

        cursorDetails.moveToFirst();

        ArrayList<String> arr_patientCode = new ArrayList<String>();
        ArrayList<String> list_brandCode = new ArrayList<String>();
        ArrayList<String> list_couponCode = new ArrayList<String>();

        String _lastname = "", _firstname = "", _mobile = "", _landline = "",
                _mdcode = "", _psrcode = "", _psrname = "", _mdname = "",
                brandCode = "", brands = "", couponCode = "";

        do {
            String _patientcode = cursorDetails.getString(cursorDetails
                    .getColumnIndex(Constants.VW_PATIENTCODE));
            String isExported = cursorDetails.getString(cursorDetails
                    .getColumnIndex(Constants.VW_PATIENT_EXPORTED));


            // check if patientCode is checked
            if (pendingDataAdapter.getarraylistSelected()
                    .contains(_patientcode) || isExported.equals("Yes")) {


                HashMap<String, String> map = null;
                if (!arr_patientCode.contains(_patientcode)) {

                    map = new HashMap<String, String>();
                    arr_patientCode.add(_patientcode);

                    // refresh brandcode and couponCode list for different
                    // patient
                    list_brandCode = new ArrayList<String>();
                    list_couponCode = new ArrayList<String>();

                    _lastname = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_LASTNAME));
                    _firstname = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_FIRSTNAME));
                    _mobile = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_MOBILE));
                    _landline = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_LANDLINE));
                    _mdcode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_MDCODE));
                    _psrcode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_PSRCODE));

                    _psrname = '"' + cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_PSRNAME)) + '"';
                    _mdname = '"' + cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_MDNAME)) + '"';

                    brandCode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_BRANDCODE));
                    brands = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_BRANDNAME));
                    couponCode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_COUPONCODE));

                    // check for duplicate data
                    if (!list_brandCode.contains(brandCode)
                            && !list_couponCode.contains(couponCode)) {
                        list_brandCode.add(brandCode);
                        list_couponCode.add(couponCode);


                        map.put(Constants.VW_PATIENT_LASTNAME, _lastname);
                        map.put(Constants.VW_PATIENT_FIRSTNAME, _firstname);
                        map.put(Constants.VW_PATIENT_MOBILE, _mobile);
                        map.put(Constants.VW_PATIENT_LANDLINE, _landline);
                        map.put(Constants.VW_PATIENT_MDCODE, _mdcode);
                        map.put(Constants.VW_PATIENT_PSRCODE, _psrcode);
                        map.put(Constants.VW_PATIENT_PSRNAME, _psrname);
                        map.put(Constants.VW_PATIENT_MDNAME, _mdname);
                        map.put(Constants.VW_PATIENT_BRANDCODE, brandCode);
                        map.put(Constants.VW_PATIENT_BRANDNAME, brands);
                        map.put(Constants.VW_PATIENT_COUPONCODE, couponCode);

                        listToBeExported.add(map);
                    }


                } else {

                    String newBrandCode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_BRANDCODE));
                    String newBrandName = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_BRANDNAME));
                    String newCouponCode = cursorDetails.getString(cursorDetails
                            .getColumnIndex(Constants.VW_PATIENT_COUPONCODE));

                    // check for duplicate data
                    if (!list_brandCode.contains(newBrandCode)
                            && !list_couponCode.contains(newCouponCode)) {
                        list_brandCode.add(newBrandCode);
                        list_couponCode.add(newCouponCode);

                        int index = arr_patientCode.indexOf(_patientcode);
                        map = listToBeExported.get(index);

                        String currentBrandName = map.get(Constants.VW_PATIENT_BRANDNAME);
                        String currentCoupondCode = map.get(Constants.VW_PATIENT_COUPONCODE);

                        map.put(Constants.VW_PATIENT_BRANDNAME, currentBrandName + "/ " + newBrandName);
                        map.put(Constants.VW_PATIENT_COUPONCODE, currentCoupondCode + "/ " + newCouponCode);

                    }

                }


            }

        } while (cursorDetails.moveToNext());


        //convert the extracted data into String
        for (int i = 0; i < listToBeExported.size(); i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            map = listToBeExported.get(i);
            _firstname = map.get(Constants.VW_PATIENT_FIRSTNAME);
            _lastname = map.get(Constants.VW_PATIENT_LASTNAME);
            _mobile = map.get(Constants.VW_PATIENT_MOBILE);
            _landline = map.get(Constants.VW_PATIENT_LANDLINE);
            _mdname = map.get(Constants.VW_PATIENT_MDNAME);
            _psrname = map.get(Constants.VW_PATIENT_PSRNAME);
            brands = map.get(Constants.VW_PATIENT_BRANDNAME);
            couponCode = map.get(Constants.VW_PATIENT_COUPONCODE);

            if (_details.equals("")) {
                _details.append(checkIfNull(_firstname) + ","
                        + checkIfNull(_lastname) + "," + checkIfNull(_mobile)
                        + "," + checkIfNull(_landline) + ","
                        + checkIfNull(_mdname) + "," + checkIfNull(_psrname)
                        + "," + checkIfNull(brands) + ","
                        + checkIfNull(couponCode));
            } else {
                _details.append("\n" + checkIfNull(_firstname) + ","
                        + checkIfNull(_lastname) + "," + checkIfNull(_mobile)
                        + "," + checkIfNull(_landline) + ","
                        + checkIfNull(_mdname) + "," + checkIfNull(_psrname)
                        + "," + checkIfNull(brands) + ","
                        + checkIfNull(couponCode));
            }


        }

        return _details.toString();
    }

    private File createCSV(String fileName, String columnString,
                           String dataString, boolean isBackUp) {

        String combinedString = columnString + dataString;
        Log.i("combinedString", "combinedString " + combinedString);
        File file = null;
        File root = Environment.getExternalStorageDirectory();
        File dir = null;
        if (root.canWrite()) {
            String date = String.valueOf(calendar.get(Calendar.MONTH) + 1)
                    + "-" + String.valueOf(calendar.get(Calendar.DATE)) + "-"
                    + String.valueOf(calendar.get(Calendar.YEAR));

            if (isBackUp == true) {
                dir = new File(root.getAbsolutePath()
                        + "/PSR_Registration_Form/Export/");
            } else {
                dir = new File(root.getAbsolutePath()
                        + "/PSR_Registration_Form/Export/" + "Export_" + date);
            }
            dir.mkdirs();
            file = new File(dir, fileName + ".csv");

            LoggerHelper.log(TAG, dir.getPath());

            // REX============================ GET FILE AND SAVE TO CONSTANTS
            Constants.TextFileName = fileName + ".csv";

            Log.i("FILENAME ADMINPAGE",
                    Constants.TextFileName);
            Log.i("FILE ADMINPAGE", Uri.fromFile(file)
                    .toString());

            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try {
                out.write(combinedString.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return file;
    }

    private String checkIfNull(String stringToCheck) {

        String result = "";
        if (stringToCheck == null || stringToCheck.equals("")) {
            result = "";
        } else {
            result = stringToCheck;
        }
        return result;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
                if (dropdown_menu.getVisibility() == View.VISIBLE) {
                    dropdown_menu.setVisibility(View.GONE);
                    // dropdown_menu.startAnimation(anim_close_preview);
                }
            }
        }
        return ret;
    }

    private void refresh() {
        Intent i = new Intent(AdminPage.this, AdminPage.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        menu_clicked = false;
        dropdown_menu.setVisibility(View.GONE);

//		finish();

    }

    public void dialog_for_MenuUpdate() {
        final Dialog dialog = new Dialog(AdminPage.this);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int height2 = (displayMetrics.heightPixels * 2) / 4;
        int width2 = (displayMetrics.widthPixels * 2) / 5;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update);
        dialog.getWindow().setLayout(width2, height2);

        // Castings
        final CheckBox checkBox_MD = (CheckBox) dialog
                .findViewById(R.id.checkBox_MD);
        final CheckBox checkBox_BrandProduct = (CheckBox) dialog
                .findViewById(R.id.checkBox_BrandProduct);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Button btn_sync = (Button) dialog.findViewById(R.id.btn_sync);

        dialog.show();

        // CANCEL BUTTON
        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });

        // SYNC BUTTON
        btn_sync.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // 1. PAG HINDI GUMANA YUNG TRUE, GAWIN PURO IF CONDITION AND
                // TRY
                // 2. Lagay sa unang if you may && then else if na yung tig
                // isang check lang - WORKING

                if (NetworkHelper.isInternetAvailable(AdminPage.this)) {
                    if (checkBox_MD.isChecked() == true
                            && checkBox_BrandProduct.isChecked() == true) {
                        Constants.isCheckBoth = true;
                        Log.i("checkBox_MD and checkBox_BrandProduct",
                                "BOTH CHECKED");
                        // IF BOTH CHECKED MIRROR ASYCH FOR MD AND PRODUCTS CALL
                        // Async_FTP_Download_BrandProduct_Mirror a2 = new
                        // Async_FTP_Download_BrandProduct_Mirror(
                        // context);
                        // a2.execute();
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                    } else if (checkBox_BrandProduct.isChecked()) {
                        Log.i("checkBox_BrandProduct---------------", "CHECKED");
                        // Async_FTP_Download_BrandProduct asyncuser = new
                        // Async_FTP_Download_BrandProduct(
                        // context);
                        // asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();

                    } else if (checkBox_MD.isChecked()) {
                        Log.i("checkBox_MD---------------", "CHECKED");
                        // Async_FTP_Download_MD asyncuser = new
                        // Async_FTP_Download_MD(
                        // context);
                        // asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadMD md_codes = new FTP_DownloadMD(AdminPage.this);
                        md_codes.execute();

                    } else {
                        Log.i("PLEASE SELECT---------------", "SELECT FIRST");
                        Toast.makeText(AdminPage.this,
                                "Please select an item to update.",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(AdminPage.this, "No Internet Connection",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private void updateRecepientDialog() {

        LayoutInflater factory = LayoutInflater.from(context);
        final View dialogView = factory.inflate(R.layout.dialog_email_settings, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);


        // cast
        final EditText txt_emailRecipient = (EditText) dialog.findViewById(R.id.txt_emailRecipient);
        TextView lbl_header_settings = (TextView) dialog.findViewById(R.id.lbl_header_settings);
        Button btn_addEmail = (Button) dialog.findViewById(R.id.btn_addEmail);
        final ListView lv_recipients = (ListView) dialogView
                .findViewById(R.id.lv_recipients);

        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, recipient);
        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lv_recipients.setAdapter(recipientAdapter);


        lv_recipients.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                final String emailAdd = (String) arg0.getItemAtPosition(position);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        AdminPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this email address?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Log.i("adsf", emailAdd);
                                        databaseHandler.deleteRecipient(emailAdd);

                                        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                                        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
                                        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        lv_recipients.setAdapter(recipientAdapter);
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

//        // SET FONTS
//        txt_emailRecipient.setTypeface(tnr_normal);
//        btn_addEmail.setTypeface(tnr_normal);
//        lbl_header_settings.setTypeface(tnr_bold);

        btn_addEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("btn_addEmail", "btn_addEmail");
                String emailAdd = txt_emailRecipient.getText().toString();

                if (emailAdd.length() == 0) {
                    Toast.makeText(context, "Please enter email address.", 8).show();

                } else {
                    // Invalid Email Address
                    if (!isValidEmail(emailAdd)) {

                        Toast.makeText(context, "Invalid email address.", 8).show();

                    } else {

                        if (!databaseHandler.checkIfEmailExist(emailAdd)) {
                            databaseHandler.addRecepient(emailAdd);

                            ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                            ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);

                            lv_recipients.setAdapter(recipientAdapter);

                            txt_emailRecipient.setText("");

                            Toast.makeText(context, "Email address successfully added.", 8).show();


                        } else {
                            Toast.makeText(context, "Email address already exist.", 8).show();
                        }


                    }
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Log.i("Pattern", " " + pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}
