package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.dialogs.DialogManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MdlistPage extends BaseActivity {

    DatabaseHandler databaseHandler;
    boolean menu_clicked = false;
    ListView lv_mdlist;
    Button btn_mdlist_cancel;
    Button btn_menu;
    TextView tv_display;
    //	Typeface tnr_bold,tnr_normal;
//	static float font_content;
    public static int listMdDetailSize;
    //	Animation anim_preview, anim_close_preview;
    RelativeLayout dropdown_menu, mdlist_page;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_md_lists);
        /**Helper classes*/
        DialogManager.init();

        Constants.isCheckBoth = false;
        CastViews();
        context = this;
        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }


        DisplaySelectedMD();

        mdlist_page.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(Constants.TAG, "touch");
                menu_clicked = false;
                dropdown_menu.setVisibility(View.GONE);
            }
        });

    }

    private void CastViews() {
//		tnr_bold = Typeface.createFromAsset(getAssets(),
//				"times_new_roman_bold.ttf");
//		tnr_normal = Typeface.createFromAsset(getAssets(),
//				"times_new_roman_normal.ttf");
//		font_content = getResources().getDimensionPixelSize(
//				font_content);


//		anim_preview = AnimationUtils.loadAnimation(this, R.anim.anim_preview);
//		anim_close_preview = AnimationUtils.loadAnimation(this,
//				R.anim.anim_close_preview);

        dropdown_menu = (RelativeLayout) findViewById(R.id.dropdown_menu);
        mdlist_page = (RelativeLayout) findViewById(R.id.mdlist_page);

//		tv_display = (TextView) findViewById(R.id.tv_mdlistDisplay);
        btn_mdlist_cancel = (Button) findViewById(R.id.btn_mdlist_cancel);
        lv_mdlist = (ListView) findViewById(R.id.lv_mdlistPage);

//		btn_mdlist_cancel.setTypeface(tnr_bold);
//		btn_mdlist_cancel.setTextSize(TypedValue.COMPLEX_UNIT_DIP, font_content);

    }

    private void DisplaySelectedMD() {


        ArrayList<HashMap<String, String>> listMdDetails = databaseHandler.get_MD_List(Constants.g_psrCode);
        Log.i("listMdDetails", "" + listMdDetails.size());
        MdlistAdapter adapter = new MdlistAdapter(listMdDetails);
        lv_mdlist.setAdapter(adapter);
        lv_mdlist.setItemsCanFocus(false);
        lv_mdlist.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }


    public class MdlistAdapter extends BaseAdapter {

        private static final String TAG = "MDListAdapter";
        ArrayList<HashMap<String, String>> objects;

        public MdlistAdapter(ArrayList<HashMap<String, String>> objects) {
            this.objects = objects;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return objects.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return objects.get(position);
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return arg0;
        }

        @Override
        public View getView(int position, View view, ViewGroup container) {
            LayoutInflater inflater = LayoutInflater.from(MdlistPage.this);
            view = inflater.inflate(R.layout.mdlist_display, container, false);
            TextView mdName = (TextView) view.findViewById(R.id.tv_mdlistDisplay);
//			mdName.setTypeface(tnr_normal);
//			mdName.setTextSize(TypedValue.COMPLEX_UNIT_DIP, font_content);
            HashMap<String, String> data = objects.get(position);
            String name = data.get(Constants.MDLIST_NAME);
            Log.i(TAG, "name : " + name);
            mdName.setText(name);
            return view;
        }


    }


    @SuppressWarnings("deprecation")
    public void onButtonClicked(View view) {
        switch (view.getId()) {

            case R.id.btn_mdlist_cancel:
                Log.i("clicked", "btn_Cancel");
                onBackPressed();
                break;

            case R.id.btn_menu:
                if (menu_clicked == false) {
                    menu_clicked = true;
                    dropdown_menu.setVisibility(View.VISIBLE);
//				dropdown_menu.startAnimation(anim_preview);
                } else if (menu_clicked == true) {
                    menu_clicked = false;
//				dropdown_menu.startAnimation(anim_close_preview);
//				anim_close_preview
//						.setAnimationListener(new AnimationListener() {
//
//							@Override
//							public void onAnimationStart(Animation animation) {
//
//							}
//
//							@Override
//							public void onAnimationRepeat(Animation animation) {
//
//							}
//
//							@Override
//							public void onAnimationEnd(Animation animation) {
//								dropdown_menu.setVisibility(View.GONE);
//								dropdown_menu.clearAnimation();
//							}
//						});
                }
                break;

            case R.id.menu_btn_search:
                Intent search = new Intent(MdlistPage.this, SearchPage.class);
                startActivity(search);
                finish();
                Constants.SEARCH_PATH = 3;
                break;

            case R.id.menu_btn_admin:
                Intent l = new Intent(MdlistPage.this, AdminPage.class);
                startActivity(l);
                finish();
                Constants.ADMIN_PATH = 3;
                break;

            case R.id.menu_btn_register:
                Intent m = new Intent(MdlistPage.this, RegistrationPage.class);
                startActivity(m);
                finish();
                break;

            case R.id.menu_btn_update:
//			dialog_for_MenuUpdate();
                DialogManager.getInstance().showUpdateDialog(this);
                break;

            case R.id.menu_btn_updateRecepient:
                DialogManager.getInstance().showEmailSettingDialog(this, databaseHandler);

                break;

            case R.id.menu_btn_logout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MdlistPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to log out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Intent intent = new Intent(MdlistPage.this,
                                                Login.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub

        if (Constants.MDLIST_PATH == 1) {
            Intent i = new Intent(MdlistPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        } else if (Constants.MDLIST_PATH == 2) {
            Intent search = new Intent(MdlistPage.this, AdminPage.class);
            startActivity(search);
            finish();
        } else if (Constants.MDLIST_PATH == 3) {
            Intent admin = new Intent(MdlistPage.this, SearchPage.class);
            startActivity(admin);
            finish();
        } else {
            Intent i = new Intent(MdlistPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        menu_clicked = false;
        dropdown_menu.setVisibility(View.GONE);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);


        if (dropdown_menu.getVisibility() == View.VISIBLE) {
            dropdown_menu.setVisibility(View.GONE);
//					dropdown_menu.startAnimation(anim_close_preview);
        }


        return ret;
    }

    public void dialog_for_MenuUpdate() {
        final Dialog dialog = new Dialog(MdlistPage.this);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int height2 = (displayMetrics.heightPixels * 2) / 4;
        int width2 = (displayMetrics.widthPixels * 2) / 5;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update);
        dialog.getWindow().setLayout(width2, height2);

        // Castings
        final CheckBox checkBox_MD = (CheckBox) dialog
                .findViewById(R.id.checkBox_MD);
        final CheckBox checkBox_BrandProduct = (CheckBox) dialog
                .findViewById(R.id.checkBox_BrandProduct);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Button btn_sync = (Button) dialog.findViewById(R.id.btn_sync);

        dialog.show();

        // CANCEL BUTTON
        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });

        // SYNC BUTTON
        btn_sync.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // 1. PAG HINDI GUMANA YUNG TRUE, GAWIN PURO IF CONDITION AND
                // TRY
                // 2. Lagay sa unang if you may && then else if na yung tig
                // isang check lang - WORKING

                if (NetworkHelper.isInternetAvailable(MdlistPage.this)) {
                    if (checkBox_MD.isChecked() == true
                            && checkBox_BrandProduct.isChecked() == true) {
                        Constants.isCheckBoth = true;
                        Log.i("checkBox_MD and checkBox_BrandProduct",
                                "BOTH CHECKED");
                        // IF BOTH CHECKED MIRROR ASYCH FOR MD AND PRODUCTS CALL
//						Async_FTP_Download_BrandProduct_Mirror a2 = new Async_FTP_Download_BrandProduct_Mirror(
//								context);
//						a2.execute();
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                    } else if (checkBox_BrandProduct.isChecked()) {
                        Log.i("checkBox_BrandProduct---------------", "CHECKED");
//						Async_FTP_Download_BrandProduct asyncuser = new Async_FTP_Download_BrandProduct(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();


                    } else if (checkBox_MD.isChecked()) {
                        Log.i("checkBox_MD---------------", "CHECKED");
//						Async_FTP_Download_MD asyncuser = new Async_FTP_Download_MD(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadMD md_codes = new FTP_DownloadMD(context);
                        md_codes.execute();


                    } else {
                        Log.i("PLEASE SELECT---------------", "SELECT FIRST");
                        Toast.makeText(MdlistPage.this,
                                "Please select an item to update.",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MdlistPage.this,
                            "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private void updateRecepientDialog() {

        LayoutInflater factory = LayoutInflater.from(context);
        final View dialogView = factory.inflate(R.layout.dialog_email_settings, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);


        // cast
        final EditText txt_emailRecipient = (EditText) dialog.findViewById(R.id.txt_emailRecipient);
        TextView lbl_header_settings = (TextView) dialog.findViewById(R.id.lbl_header_settings);
        Button btn_addEmail = (Button) dialog.findViewById(R.id.btn_addEmail);
        final ListView lv_recipients = (ListView) dialogView
                .findViewById(R.id.lv_recipients);

        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lv_recipients.setAdapter(recipientAdapter);


        lv_recipients.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                final String emailAdd = (String) arg0.getItemAtPosition(position);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        MdlistPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this email address?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Log.i("adsf", emailAdd);
                                        databaseHandler.deleteRecipient(emailAdd);

                                        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                                        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
                                        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        lv_recipients.setAdapter(recipientAdapter);
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

//		// SET FONTS
//		txt_emailRecipient.setTypeface(tnr_normal);
//		btn_addEmail.setTypeface(tnr_normal);
//		lbl_header_settings.setTypeface(tnr_bold);

        btn_addEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("btn_addEmail", "btn_addEmail");
                String emailAdd = txt_emailRecipient.getText().toString();

                if (emailAdd.length() == 0) {
                    Toast.makeText(context, "Please enter email address.", 8).show();

                } else {
                    // Invalid Email Address
                    if (!isValidEmail(emailAdd)) {

                        Toast.makeText(context, "Invalid email address.", 8).show();

                    } else {

                        if (!databaseHandler.checkIfEmailExist(emailAdd)) {
                            databaseHandler.addRecepient(emailAdd);

                            ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                            ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);

                            lv_recipients.setAdapter(recipientAdapter);

                            txt_emailRecipient.setText("");

                            Toast.makeText(context, "Email address successfully added.", 8).show();


                        } else {
                            Toast.makeText(context, "Email address already exist.", 8).show();
                        }


                    }
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Log.i("Pattern", " " + pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}