package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.dialogs.DialogManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchPage extends BaseActivity {
    DatabaseHandler databaseHandler;
    CustomAdapterSearch searchAdapter;
    boolean menu_clicked = false;
    //	float font_title, font_counter;
//	static float font_content;
    Typeface tnr_normal, tnr_bold_italic, tnr_bold, monotype;
    //	Animation anim_preview, anim_close_preview;
    RelativeLayout dropdown_menu, search_page;
    Button btn_cancel, btn_menu;
    ListView lv_all_patient;
    EditText txt_Search;
    TextView lbl_search;
    private ArrayList<HashMap<String, String>> arraylist_AllDetails;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchpage);

        /**Helper classes*/
        DialogManager.init();

        Constants.isCheckBoth = false;
        //typeface
        tnr_normal = Typeface.createFromAsset(getAssets(), "times_new_roman_normal.ttf");
        tnr_bold_italic = Typeface.createFromAsset(getAssets(), "times_new_roman_bold_italic.ttf");
        tnr_bold = Typeface.createFromAsset(getAssets(), "times_new_roman_bold.ttf");
        monotype = Typeface.createFromAsset(getAssets(), "Monotype.TTF");

//		font_title = getResources().getDimensionPixelSize(font_title);
//		font_content = getResources().getDimensionPixelSize(R.dimen.font_content);
//		font_counter = getResources().getDimensionPixelSize(R.dimen.font_counter);
        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }
        CastViews();
        context = this;
        Cursor cursorAllDetails = databaseHandler.getAllDetails(Constants.g_psrCode);
        fillData(cursorAllDetails);
    }

    private void fillData(Cursor cursorAllDetails) {
        arraylist_AllDetails.clear();
        int _AllData = cursorAllDetails.getCount();
        HashMap<String, String> hashmap = null;
        if (_AllData != 0) {

            ArrayList<String> listPatientCode = new ArrayList<String>();

            ArrayList<String> listBrandCode = new ArrayList<String>();
            ArrayList<String> listCouponCode = new ArrayList<String>();

            for (cursorAllDetails.moveToFirst(); !cursorAllDetails.isAfterLast(); cursorAllDetails.moveToNext()) {
                hashmap = new HashMap<String, String>();

                String patient_code = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENTCODE));
                if (!listPatientCode.contains(patient_code)) {
                    //refresh listBrandCode and listCouponCode for different patient code
                    listBrandCode = new ArrayList<String>();
                    listCouponCode = new ArrayList<String>();

                    listPatientCode.add(patient_code);
                    String _id = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.COLUMN_ID));

                    String fname = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_FIRSTNAME));
                    String lname = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_LASTNAME));
                    String mobile = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_MOBILE));
                    String md_code = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_MDCODE));

                    String psr_code = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_PSRCODE));

                    String psr_name = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_PSRNAME));
                    String md_name = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_MDNAME));
                    String full_name = lname.toUpperCase() + ", " + fname.toUpperCase();


                    String brandCode = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_BRANDCODE));
                    String brands = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_BRANDNAME));
                    String coupons = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_COUPONCODE));

                    //check for duplicates
                    if (!listBrandCode.contains(brandCode) && !listCouponCode.contains(coupons)) {
                        listBrandCode.add(brandCode);
                        listCouponCode.add(coupons);

                        hashmap.put(Constants.COLUMN_ID, _id);
                        hashmap.put(Constants.PATIENT_FNAME, full_name);
                        hashmap.put(Constants.PATIENT_MOBILE, mobile);
                        hashmap.put(Constants.PATIENT_MD, md_name);
                        hashmap.put(Constants.PSR_NAME, psr_name);
                        hashmap.put(Constants.BRAND_NAME, brands);
                        hashmap.put(Constants.COUPON_CODE, coupons);

                        arraylist_AllDetails.add(hashmap);
                    }


                } else {
                    String brandCode = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_BRANDCODE));
                    String couponCode = cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_COUPONCODE));
                    //check for duplicates
                    if (!listBrandCode.contains(brandCode) && !listCouponCode.contains(couponCode)) {
                        listBrandCode.add(brandCode);
                        listCouponCode.add(couponCode);

                        int index = listPatientCode.indexOf(patient_code);
                        hashmap = arraylist_AllDetails.get(index);

                        String brands = hashmap.get(Constants.VW_PATIENT_BRANDNAME);
                        String coupons = hashmap.get(Constants.VW_PATIENT_COUPONCODE);

                        hashmap.put(Constants.BRAND_NAME, brands + ", " + cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_BRANDNAME)));
                        hashmap.put(Constants.COUPON_CODE, coupons + ", " + cursorAllDetails.getString(cursorAllDetails.getColumnIndex(Constants.VW_PATIENT_COUPONCODE)));

                    }

                }


            }

            searchAdapter = new CustomAdapterSearch(SearchPage.this, arraylist_AllDetails,
                    R.layout.search_attributecell, Constants.SEARCH_ATTRIBUTE_KEYS, Constants.SEARCH_ATTRIBUTE_VIEWS, true);
            lv_all_patient.setAdapter(searchAdapter);
        }

    }

    private void CastViews() {
//		anim_preview = AnimationUtils.loadAnimation(this, R.anim.anim_preview);
//		anim_close_preview = AnimationUtils.loadAnimation(this, R.anim.anim_close_preview);

        search_page = (RelativeLayout) findViewById(R.id.search_page);
        dropdown_menu = (RelativeLayout) findViewById(R.id.dropdown_menu);
        lv_all_patient = (ListView) findViewById(R.id.lv_all_patient);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);

        lbl_search = (TextView) findViewById(R.id.lbl_search);
        txt_Search = (EditText) findViewById(R.id.txt_Search);
        txt_Search.addTextChangedListener(filterTextWatcher);

//		btn_cancel.setTypeface(tnr_bold);
//		btn_cancel.setTextSize(TypedValue.COMPLEX_UNIT_SP, font_title);
//		lbl_search.setTypeface(tnr_bold);	lbl_search.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//		txt_Search.setTypeface(tnr_normal);	txt_Search.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);

        arraylist_AllDetails = new ArrayList<HashMap<String, String>>();

        search_page.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(Constants.TAG, "touch");
                menu_clicked = false;
                dropdown_menu.setVisibility(View.GONE);

            }
        });

//		lv_all_patient.setTextFilterEnabled(true);
//		lv_all_patient.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View v, int position,
//					long id) {
//				final String ID  = arraylist_AllDetails.get(position).get(Constants.COLUMN_ID);
//				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//						SearchPage.this);
//				alertDialogBuilder.setTitle("Please Confirm");
//				alertDialogBuilder
//						.setMessage(
//								"Do you want to update this record?")
//						.setCancelable(false)
//						.setPositiveButton("Yes",
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,
//											int id) {
//										Constants.UpdateID = ID;
//										Intent intent = new Intent(SearchPage.this,RegistrationPage.class);
//										Constants.isUpdate = true;
//										startActivity(intent);
//										finish();
//									}
//								})
//						.setNegativeButton("No",
//								new DialogInterface.OnClickListener() {
//									public void onClick(DialogInterface dialog,
//											int id) {
//										dialog.cancel();
//									}
//								});
//
//				AlertDialog alertDialog = alertDialogBuilder.create();
//				alertDialog.show();
//			}
//		});

    }

    // text watcher for the name search
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            filterQueryName(s.toString().replace("'", "\'"));
        }
    };

    // used for search
    private void filterQueryName(String _constraint) {
        Cursor c;

        if (_constraint.equals("") || _constraint.equals(null))
            c = databaseHandler.getAllDetails(Constants.g_psrCode);
        else
            c = databaseHandler.getAllDetails(_constraint, Constants.g_psrCode);

        fillData(c);
    }

    @SuppressWarnings("deprecation")
    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                Log.i("clicked", "btn_Cancel");
                onBackPressed();
                break;

            case R.id.btn_menu:
                if (menu_clicked == false) {
                    menu_clicked = true;
                    dropdown_menu.setVisibility(View.VISIBLE);
//				 dropdown_menu.startAnimation(anim_preview);
                } else if (menu_clicked == true) {
                    menu_clicked = false;
//				 dropdown_menu.startAnimation(anim_close_preview);
//				 anim_close_preview.setAnimationListener(new AnimationListener() {
//					
//					@Override
//					public void onAnimationStart(Animation animation) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void onAnimationRepeat(Animation animation) {
//						// TODO Auto-generated method stub
//						
//					}
//					
//					@Override
//					public void onAnimationEnd(Animation animation) {
//						dropdown_menu.setVisibility(View.GONE);
//						dropdown_menu.clearAnimation();
//					}
//				});
                }
                break;

            case R.id.menu_btn_admin:
                Intent i = new Intent(SearchPage.this, AdminPage.class);
                startActivity(i);
                finish();
                Constants.ADMIN_PATH = 2;
                break;

            case R.id.menu_btn_register:
                Intent j = new Intent(SearchPage.this, RegistrationPage.class);
                startActivity(j);
                finish();
                break;

            case R.id.menu_btn_update:
//                dialog_for_MenuUpdate();
                DialogManager.getInstance().showUpdateDialog(this);
                break;

            case R.id.menu_btn_mdlist:
                Intent k = new Intent(SearchPage.this, MdlistPage.class);
                startActivity(k);
                finish();
                Constants.MDLIST_PATH = 2;
                break;

            case R.id.menu_btn_updateRecepient:
                DialogManager.getInstance().showEmailSettingDialog(this, databaseHandler);

                break;

            case R.id.menu_btn_logout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SearchPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage(
                                "Are you sure you want to log out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Intent intent = new Intent(SearchPage.this, Login.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
                if (dropdown_menu.getVisibility() == View.VISIBLE) {
                    dropdown_menu.setVisibility(View.GONE);
//					dropdown_menu.startAnimation(anim_close_preview);
                }
            }
        }
        return ret;
    }

    @Override
    public void onBackPressed() {
        if (Constants.SEARCH_PATH == 1) {
            Intent i = new Intent(SearchPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        } else if (Constants.SEARCH_PATH == 2) {
            Intent search = new Intent(SearchPage.this, AdminPage.class);
            startActivity(search);
            finish();
        } else if (Constants.SEARCH_PATH == 3) {
            Intent search = new Intent(SearchPage.this, MdlistPage.class);
            startActivity(search);
            finish();
        } else {
            Intent i = new Intent(SearchPage.this, RegistrationPage.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        menu_clicked = false;
        dropdown_menu.setVisibility(View.GONE);
    }

    public void onDestroy() {
        super.onDestroy();
        txt_Search.removeTextChangedListener(filterTextWatcher);
        System.gc();
    }


    public void dialog_for_MenuUpdate() {
        final Dialog dialog = new Dialog(SearchPage.this);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int height2 = (displayMetrics.heightPixels * 2) / 4;
        int width2 = (displayMetrics.widthPixels * 2) / 5;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update);
        dialog.getWindow().setLayout(width2, height2);

        // Castings
        final CheckBox checkBox_MD = (CheckBox) dialog
                .findViewById(R.id.checkBox_MD);
        final CheckBox checkBox_BrandProduct = (CheckBox) dialog
                .findViewById(R.id.checkBox_BrandProduct);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Button btn_sync = (Button) dialog.findViewById(R.id.btn_sync);

        dialog.show();

        // CANCEL BUTTON
        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });

        // SYNC BUTTON
        btn_sync.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // 1. PAG HINDI GUMANA YUNG TRUE, GAWIN PURO IF CONDITION AND
                // TRY
                // 2. Lagay sa unang if you may && then else if na yung tig
                // isang check lang - WORKING

                if (NetworkHelper.isInternetAvailable(SearchPage.this)) {
                    if (checkBox_MD.isChecked() == true
                            && checkBox_BrandProduct.isChecked() == true) {
                        Constants.isCheckBoth = true;
                        Log.i("checkBox_MD and checkBox_BrandProduct",
                                "BOTH CHECKED");
                        // IF BOTH CHECKED MIRROR ASYCH FOR MD AND PRODUCTS CALL
//						Async_FTP_Download_BrandProduct_Mirror a2 = new Async_FTP_Download_BrandProduct_Mirror(
//								context);
//						a2.execute();
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                    } else if (checkBox_BrandProduct.isChecked()) {
                        Log.i("checkBox_BrandProduct---------------", "CHECKED");
//						Async_FTP_Download_BrandProduct asyncuser = new Async_FTP_Download_BrandProduct(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();


                    } else if (checkBox_MD.isChecked()) {
                        Log.i("checkBox_MD---------------", "CHECKED");
//						Async_FTP_Download_MD asyncuser = new Async_FTP_Download_MD(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadMD md_codes = new FTP_DownloadMD(context);
                        md_codes.execute();


                    } else {
                        Log.i("PLEASE SELECT---------------", "SELECT FIRST");
                        Toast.makeText(SearchPage.this,
                                "Please select an item to update.",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(SearchPage.this,
                            "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private void updateRecepientDialog() {

        LayoutInflater factory = LayoutInflater.from(context);
        final View dialogView = factory.inflate(R.layout.dialog_email_settings, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);


        // cast
        final EditText txt_emailRecipient = (EditText) dialog.findViewById(R.id.txt_emailRecipient);
        TextView lbl_header_settings = (TextView) dialog.findViewById(R.id.lbl_header_settings);
        Button btn_addEmail = (Button) dialog.findViewById(R.id.btn_addEmail);
        final ListView lv_recipients = (ListView) dialogView
                .findViewById(R.id.lv_recipients);

        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lv_recipients.setAdapter(recipientAdapter);


        lv_recipients.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                final String emailAdd = (String) arg0.getItemAtPosition(position);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        SearchPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this email address?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Log.i("adsf", emailAdd);
                                        databaseHandler.deleteRecipient(emailAdd);

                                        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                                        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
                                        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        lv_recipients.setAdapter(recipientAdapter);
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

        // SET FONTS
        txt_emailRecipient.setTypeface(tnr_normal);
        btn_addEmail.setTypeface(tnr_normal);
        lbl_header_settings.setTypeface(tnr_bold);

        btn_addEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("btn_addEmail", "btn_addEmail");
                String emailAdd = txt_emailRecipient.getText().toString();

                if (emailAdd.length() == 0) {
                    Toast.makeText(context, "Please enter email address.", 8).show();

                } else {
                    // Invalid Email Address
                    if (!isValidEmail(emailAdd)) {

                        Toast.makeText(context, "Invalid email address.", 8).show();

                    } else {

                        if (!databaseHandler.checkIfEmailExist(emailAdd)) {
                            databaseHandler.addRecepient(emailAdd);

                            ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                            ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);

                            lv_recipients.setAdapter(recipientAdapter);

                            txt_emailRecipient.setText("");

                            Toast.makeText(context, "Email address successfully added.", 8).show();


                        } else {
                            Toast.makeText(context, "Email address already exist.", 8).show();
                        }


                    }
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Log.i("Pattern", " " + pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }


}