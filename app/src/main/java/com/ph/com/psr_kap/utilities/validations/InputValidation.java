package com.ph.com.psr_kap.utilities.validations;

import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidation {


    public static boolean isMobileNumberValid(String mobileNumber) {
        Log.e("InputValidation", "isMobileNumberValid: " +
                mobileNumber.substring(0, 2).toString() +
                " " + mobileNumber.length());

        if (mobileNumber.substring(0, 2).equals("09") && mobileNumber.length() == 11
                || mobileNumber.substring(0, 3).equals("639") && mobileNumber.length() == 12) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isEmailValid(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Log.i("Pattern", " " + pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }

}
