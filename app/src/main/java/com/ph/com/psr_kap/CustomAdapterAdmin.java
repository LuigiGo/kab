package com.ph.com.psr_kap;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.ph.com.psr_kap.utilities.constants.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomAdapterAdmin extends SimpleAdapter{

	String TAG = this.getClass().getSimpleName();

	private Context context;
	public DatabaseHandler databaseHandler;
	private final ArrayList<HashMap<String, String>> mData;
	private ArrayList<HashMap<String, String>> unfilteredValues;
	private boolean chosenValues, ismarkAll = true;
	private int resource;
	private String[] from;
	private int[] to;
	private SimpleFilter mFilter;
	private ArrayList<String> arraylistRecId;
	private ArrayList<String> arraylistSelected;


	private final boolean[] mCheckedState;

	public CustomAdapterAdmin(Context context,
			ArrayList<HashMap<String, String>> data, int resource,
			String[] from, int[] to, boolean chosenValues) {
		super(context, data, resource, from, to);

		mCheckedState = new boolean[data.size()];

		this.context = context;
		mData = data;
		this.unfilteredValues = mData;
		this.resource = resource;
		this.from = from;
		this.to = to;
		this.arraylistRecId = new ArrayList<String>();
		this.arraylistSelected = new ArrayList<String>();
		this.chosenValues = chosenValues;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		final int pos = position;
		//View rowView = null;
		try {
		//	if(convertView == null){
				convertView = inflater.inflate(resource, null, false);

				//COLUMN_ID, PATIENT_FNAME, PATIENT_MOBILE, PSR_NAME, PATIENT_MD, BRAND_NAME

				TextView tv_id 	  		 = (TextView) convertView.findViewById(to[0]);
				TextView tv_name	  	 = (TextView) convertView.findViewById(to[1]);
				TextView tv_mobile 		 = (TextView) convertView.findViewById(to[2]);
				TextView tv_landline 	 = (TextView) convertView.findViewById(to[3]);
				TextView tv_psr 		 = (TextView) convertView.findViewById(to[4]);
				TextView tv_md 	 		 = (TextView) convertView.findViewById(to[5]);
				TextView tv_brand 	  	 = (TextView) convertView.findViewById(to[6]);


				final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkboxSelect);

				// getting the value in hashmap
				final String _id 	  = unfilteredValues.get(position).get(from[0]).toString();
				String _name	  	  = unfilteredValues.get(position).get(from[1]).toString();
				String _mobile		  = unfilteredValues.get(position).get(from[2]).toString();
				String _landline	  = unfilteredValues.get(position).get(from[3]).toString();
				String _psr 		  = unfilteredValues.get(position).get(from[4]).toString();
				String _md 		  	  = unfilteredValues.get(position).get(from[5]).toString();
				final String _brand   = unfilteredValues.get(position).get(from[6]).toString();
				final String patientCode = unfilteredValues.get(position).get(Constants.PATIENT_CODE).toString();

				checkBox.setTag(pos);

//				// setting or putting the value to the widgets
				tv_id.setText(_id);
				tv_name.setText(_name);
				tv_mobile.setText(_mobile);
				tv_landline.setText(_landline);
				tv_psr.setText(_psr);
				tv_md.setText(_md);
				tv_brand.setText(_brand);

			Log.e(TAG, "getView: " + tv_id.getText());
			Log.e(TAG, "getView: " + tv_name.getText());
			Log.e(TAG, "getView: " + tv_mobile.getText());
			Log.e(TAG, "getView: " + tv_landline.getText());
			Log.e(TAG, "getView: " + tv_psr.getText());
			Log.e(TAG, "getView: " + tv_md.getText());
			Log.e(TAG, "getView: " + tv_brand.getText());

				// Setting the ID
				tv_id.setId(position);
				tv_name.setId(position);
				tv_mobile.setId(position);
				tv_landline.setId(position);
				tv_psr.setId(position);
				tv_md.setId(position);
				tv_brand.setId(position);

				arraylistRecId = AdminPage.arraylistRecordIds;
				arraylistSelected = AdminPage.arraylistRecordIds_pos;


				if (ismarkAll && !chosenValues)
				{
					if (!chosenValues)
					{
						checkBox.setChecked(false);
						arraylistRecId.remove(_id);
						arraylistSelected.remove(patientCode);
					}
				}

				if (arraylistSelected.contains(patientCode)) {
					checkBox.setChecked(true);
				} else {
					checkBox.setChecked(false);
				}

				checkBox.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int arraylistSize = AdminPage.lv_admin_patient_list.getCount();
						Log.i("arraylistSize", String.valueOf(arraylistSize));
						if (checkBox.isChecked()) {
							arraylistSelected.add(patientCode);
							arraylistRecId.add(_id);

						} else if (!checkBox.isChecked()) {
							arraylistSelected.remove(patientCode);
							arraylistRecId.remove(_id);

						}

						if (arraylistSize == arraylistSelected.size()) {
							AdminPage.ismarkAll = true;
							ismarkAll = true;
							AdminPage.btn_MarkAll.setText("Unmark All");
						} else {
							AdminPage.ismarkAll = false;
							ismarkAll = false;
							AdminPage.btn_MarkAll.setText("Mark All");
						}
						Log.i("inner isMarkAll", String.valueOf(ismarkAll));
						AdminPage.arraylistRecordIds = arraylistRecId;
						AdminPage.arraylistRecordIds_pos = arraylistSelected;

					}
				});

		//	}



		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError E) {
			E.printStackTrace();
		}



		return convertView;
	}

	public ArrayList<String> getarraylistSelected() {
		return this.arraylistSelected;
	}

	public ArrayList<String> getArrayListRecordId() {
		return this.arraylistRecId;
	}

	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new SimpleFilter();
		}
		return mFilter;
	}

	public int getCount() {
		return unfilteredValues.size();
	}


	private class SimpleFilter extends Filter {
		@SuppressWarnings("unchecked")
		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			FilterResults results = new FilterResults();

			String prefixString = null == prefix ? null : prefix.toString()
					.toLowerCase();

			ArrayList<HashMap<String, String>> unfilteredValues;
			if (null != prefixString && prefixString.length() > 0) {

				synchronized (mData) {
					unfilteredValues = (ArrayList<HashMap<String, String>>) mData
							.clone();
				}

				for (int i = unfilteredValues.size() - 1; i >= 0; --i) {
					HashMap<String, String> h = unfilteredValues.get(i);

					String str = (String) h.get(from[0]).toString();
					if (!str.toLowerCase().startsWith(prefixString)) {
						unfilteredValues.remove(i);
					}

				}

				results.values = unfilteredValues;
				results.count = unfilteredValues.size();

			} else {
				synchronized (mData) {
					results.values = mData;
					results.count = mData.size();
				}
			}

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			unfilteredValues = (ArrayList<HashMap<String, String>>) results.values;
			notifyDataSetChanged();

		}

	}
}

