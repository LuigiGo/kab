package com.ph.com.psr_kap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class Async_FTP_User_Save extends AsyncTask<String, Integer, Boolean> {

	ProgressDialog progressDialog;
	Context context;

	DatabaseHandler databaseHandler;
	int totalNoOfItemsToBeUpdated, percentage;
	double itemsUpdated;
	ArrayList<String> listDeliveryNo;

	ArrayList<String> listUsername;
	ArrayList<String> listPassword;
	ArrayList<String> listPsrCode;
	ArrayList<String> listPsrName;

	AlertDialog alertDialog;
	// ====================
	Calendar calendar;
	String fileName, fname;

	public Async_FTP_User_Save(Context context) {
		this.context = context;

		listDeliveryNo = new ArrayList<String>();

		listUsername = new ArrayList<String>();
		listPassword = new ArrayList<String>();
		listPsrCode = new ArrayList<String>();
		listPsrName = new ArrayList<String>();

		calendar = Calendar.getInstance();

		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}
	}

	protected void onPreExecute() {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage("Updating PSR account. Please wait...");
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {
		// TODO Auto-generated method stub
		// CSVCompanies
		databaseHandler.deleteTablePSR();

		String CSVUsers = Environment.getExternalStorageDirectory().toString()
				+ "/Kabalikat_Advocacy_Program_2015/Users/Users.csv";

		Log.i("CSVUsers--------------", CSVUsers);

		BufferedReader br = null;
		String line = "";
		// REGEX that ignores "," comma within double qoute ""
		String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		// READ CSV Members
		try {
			Log.i("try", "try");
			br = new BufferedReader(new FileReader(CSVUsers));
			while ((line = br.readLine()) != null) {

				// use tab as separator
				String[] UserDetails = line.split(cvsSplitBy);

				listUsername.add(UserDetails[0]);
				Log.i("UserDetails[0]", UserDetails[0]);// username
				Log.i("UserDetails[1]", UserDetails[1]);// password
				Log.i("UserDetails[2]", UserDetails[2]);// code
				Log.i("UserDetails[3]", UserDetails[3]);// name
				Log.i("listUsername.add--------------", listUsername.toString());

				listPassword.add(UserDetails[1]);

				listPsrCode.add(UserDetails[2]);

				// REMOVE DOUBLE QUOTE and COMMA FROM SERVER AND THEN SAVE TO
				// DB.
				listPsrName.add(UserDetails[3].replace("\"", ""));

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		totalNoOfItemsToBeUpdated = listUsername.size();

		// Log.i("totalNoOfItemsToBeUpdated--------------",
		// ""+totalNoOfItemsToBeUpdated);
		//
		//
		for (int i = 0; i < listUsername.size(); i++) {
			if (i >= 1) {

				String userName = listUsername.get(i);
				String userPass = listPassword.get(i);
				String userPSRCode = listPsrCode.get(i);
				String userPSRName = listPsrName.get(i);
				ContentValues values = new ContentValues();

				values.put(Constants.PSR_CODE, userPSRCode);
				values.put(Constants.PSR_NAME, userPSRName);
				values.put(Constants.PSR_USERNAME, userName);
				values.put(Constants.PSR_PASSWORD, userPass);

				databaseHandler.updatePSRTable(values);

				itemsUpdated++;
				percentage = (int) ((itemsUpdated / totalNoOfItemsToBeUpdated) * 100);
				updateProgress(percentage);

				Log.i("updatePSRTable-------------------", "updatePSRTable");

			}

		}
		
		return true;
	}

	private void updateProgress(Integer value) {
		if (progressDialog != null && progressDialog.isShowing() == true) {
			progressDialog.setProgress(value);
			Log.i("Percent",
					String.valueOf(value) + " " + String.valueOf(itemsUpdated)
							+ " " + String.valueOf(totalNoOfItemsToBeUpdated));
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		progressDialog.dismiss();

		// if (result) {
		// Log.i("RESULT VALID---------------", "RESULT VALID---------------");
		// }

		if (NW_Helper.isInternetAvailable(context)) {
			if (result == true) {
				
				new Async_ValidateData(context, Constants.username, Constants.password).execute();
//				System.out.println("Done Updating User data.");
//				Log.i("Done Updating User data.", "Done Updating User data.");
//
//				// Async_FTP_MD_Save a = new Async_FTP_MD_Save(context);
//				// a.execute();
//
//				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//						context);
//				// set title
//				alertDialogBuilder.setTitle("Information");
//				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);
//
//				// set dialog message
//				alertDialogBuilder
//						.setMessage("PSR accounts have been successfully updated.");
//				alertDialogBuilder.setCancelable(false);
//				alertDialogBuilder.setNeutralButton("OK",
//						new DialogInterface.OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog, int id) {
//								// TODO Auto-generated method stub
//								// Intent refresh = new Intent(context,
//								// Login.class);
//								// context.startActivity(refresh);
//								// ((Activity) context).finish();
//								dialog.cancel();
//							}
//						});
//				alertDialog = alertDialogBuilder.create();
//				alertDialog.show();
				

			} else {
				System.out
						.println("File not updating Async_FTP_User_Save, RETRY");
				Log.i("File not updating Async_FTP_User_Save, RETRY",
						"File not updating Async_FTP_User_Save, RETRY");

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				// set title
				alertDialogBuilder.setTitle("Server Error");
				// alertDialogBuilder.setIcon(R.drawable.ic_send_success_two);

				// set dialog message
				alertDialogBuilder
						.setMessage("There was a problem updating the data. Would you like to try again.");
				alertDialogBuilder.setCancelable(false);
				alertDialogBuilder.setNeutralButton("Update",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
//								Async_FTP_User_Save a = new Async_FTP_User_Save(
//										context);
//								a.execute();
								Async_FTP_Download asyncuser = new Async_FTP_Download(
										context);
								asyncuser.execute();
								dialog.cancel();
							}
						});
	    		alertDialogBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int id) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
				
				alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				
				//=======
//	        	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//	    		// set title
//	    		alertDialogBuilder.setTitle("Log Out");
////	    		alertDialogBuilder.setIcon(R.drawable.question_mark);
//
//	    		// set dialog message
//	    		alertDialogBuilder
//	    				.setMessage("Are you sure you want to log out?");
//	    		alertDialogBuilder.setCancelable(true);
//	    		alertDialogBuilder.setPositiveButton("Yes",
//	    				new DialogInterface.OnClickListener() {
//	    					@Override
//	    					public void onClick(DialogInterface dialog, int id) {
//	    						// TODO Auto-generated method stub
//	    						Intent it = new Intent(MainActivity_SMS.this,
//	    								Login_SMS.class);
//	    						startActivity(it);
//	    						overridePendingTransition(R.anim.slide_in_right,
//	    								R.anim.slide_out_right);
//
//	    						finish();
//	    					}
//	    				});
//	    		alertDialogBuilder.setNegativeButton("No",
//	    				new DialogInterface.OnClickListener() {
//	    					@Override
//	    					public void onClick(DialogInterface dialog, int id) {
//	    						// TODO Auto-generated method stub
//	    						// _hapticFeedback.vibrate(35);
//	    						dialog.cancel();
//	    					}
//	    				});
//
//	    		alertDialog = alertDialogBuilder.create();
//	    		alertDialog.show();
			}
		} else {
			Toast.makeText(context, "Please check your internet connection.",
					Toast.LENGTH_LONG).show();
		}
	}
}