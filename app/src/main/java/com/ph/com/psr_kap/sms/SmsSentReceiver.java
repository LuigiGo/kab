package com.ph.com.psr_kap.sms;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.DatabaseHandler;
import com.ph.com.psr_kap.R;
import com.ph.com.psr_kap.RegistrationPage;
import com.ph.com.psr_kap.UnsentRegistration;
import com.ph.com.psr_kap.service.EmailService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SmsSentReceiver extends BroadcastReceiver {

	private static final String TAG = SmsSentReceiver.class.getSimpleName();
	private Context context;
	private DatabaseHandler databaseHandler;

	@Override
	public void onReceive(Context context, Intent arg1) {
		Log.i(TAG, "sms data recieved");
		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}

		this.context = context;
		switch (getResultCode()) {
		case Activity.RESULT_OK:
			//			if(!isSend)
			dialogForSMSStatus("SMS succesfully sent", null);
			Log.i(TAG, "RESULT_OK");

			break;
		case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
			//			if(!isSend)

			dialogForSMSStatus("SMS Failed","Unable to send sms. Your load balance is not enough to send a text.");
			saveUnsentSMS(Constants.TEXT_MESSAGE);
			break;
		case SmsManager.RESULT_ERROR_NO_SERVICE:
			//                        Toast.makeText(getBaseContext(), "No service", 
			//                                Toast.LENGTH_SHORT).show();
			//			if(!isSend)
			Log.i(TAG, "RESULT_ERROR_NO_SERVICE");
			dialogForSMSStatus("SMS Failed","Unable to send sms. No network detected.");
			saveUnsentSMS(Constants.TEXT_MESSAGE);
			break;
		case SmsManager.RESULT_ERROR_NULL_PDU:
			//                        Toast.makeText(getBaseContext(), "Null PDU", 
			//                                Toast.LENGTH_SHORT).show();
			//			if(!isSend)
			Log.i(TAG, "RESULT_ERROR_NULL_PDU");
			dialogForSMSStatus("SMS Failed","Unable to send sms. Network transmission error.");
			saveUnsentSMS(Constants.TEXT_MESSAGE);

			break;
		case SmsManager.RESULT_ERROR_RADIO_OFF:
			//                        Toast.makeText(getBaseContext(), "Radio off", 
			//                                Toast.LENGTH_SHORT).shw();
			//			if(!isSend)
			dialogForSMSStatus("SMS Failed","Unable to send sms. No network detected.");
			Log.i(TAG, "RESULT_ERROR_NULL_PDU");
			saveUnsentSMS(Constants.TEXT_MESSAGE);
			break; 
		}
	}

	public void dialogForSMSStatus(String title, String message){

		Intent notificationIntent = new Intent(context, RegistrationPage.class);  
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,   
				PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_ONE_SHOT);
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(context)
		.setSmallIcon(0)
		.setContentTitle("Kabalikat : "  +title)
		.setContentText(message);
		mBuilder.setDefaults(Notification.DEFAULT_SOUND);
		mBuilder.setAutoCancel(true);
		mBuilder.setSmallIcon(R.drawable.ic_launcher);
		mBuilder.setContentIntent(contentIntent); 
		NotificationManager mNotificationManager =
				(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Constants.noticationCount++;
		mNotificationManager.notify(Constants.noticationCount, mBuilder.build());
		Log.i(TAG, "display notificatnio " + title + " message " + message );
	}



	private void saveUnsentSMS(String text){

		if (!databaseHandler.checkIfMessageExist(text)) {
			databaseHandler.insertUnsentRegistration(text);
		}


		ArrayList<UnsentRegistration> listUnsent = databaseHandler.getAllUnsentRegistration();
		String output = "";
		for (int i = 0; i < listUnsent.size(); i++) {
			if (output.equals("")) {
				output = listUnsent.get(i).getMessage();
			}else{
				output += "\n" + listUnsent.get(i).getMessage();
			}

		}




		String textFileName = "UnsentRegistration.txt";
		String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ "/PSR_Registration_Form/Unsent Registration/";
		File dir = new File(dirPath);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				Toast.makeText(context, "Failed to create directory.", Toast.LENGTH_LONG).show();
			}
		}

		File fileUnsentText = new File(dir, textFileName);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(fileUnsentText);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

		try {
			out.write(output.getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		context.startService(new Intent(context, EmailService.class));
	}

}