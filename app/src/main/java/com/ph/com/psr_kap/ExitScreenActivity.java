package com.ph.com.psr_kap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.service.EmailService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ExitScreenActivity extends BaseActivity {

    Button btn_MainPage;
    Context context;

    DatabaseHandler databaseHandler;
    private boolean isSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit_screen);

        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        context = this;

        casting();
        changeFont();

        btn_MainPage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Intent i = new Intent(ExitScreenActivity.this,
                        RegistrationPage.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });

//		String sent = "SMS_SENT";
//		String DELIVERED = "SMS_DELIVERED";
//		final PendingIntent sentPI = PendingIntent.getBroadcast(context,
//				0, new Intent(sent), 0);
//
//		PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
//				new Intent(DELIVERED), 0);
//
//		//		//---when the SMS has been sent---
//		context.registerReceiver(new BroadcastReceiver(){
//			@Override
//			public void onReceive(Context arg0, Intent arg1) {
//				switch (getResultCode())
//				{
//				case Activity.RESULT_OK:
////					if(!isSend)
//						dialogForSMSStatus("SMS succesfully sent", null);
//
//					break;
//				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
////					if(!isSend)
//						dialogForSMSStatus("SMS Failed","Unable to send sms. Your load balance is not enough to send a text.");
//					saveUnsentSMS(Constants.TEXT_MESSAGE);
//					break;
//				case SmsManager.RESULT_ERROR_NO_SERVICE:
//					//                        Toast.makeText(getBaseContext(), "No service", 
//					//                                Toast.LENGTH_SHORT).show();
////					if(!isSend)
//						dialogForSMSStatus("SMS Failed","Unable to send sms. No network detected.");
//					saveUnsentSMS(Constants.TEXT_MESSAGE);
//					break;
//				case SmsManager.RESULT_ERROR_NULL_PDU:
//					//                        Toast.makeText(getBaseContext(), "Null PDU", 
//					//                                Toast.LENGTH_SHORT).show();
////					if(!isSend)
//					dialogForSMSStatus("SMS Failed","Unable to send sms. Network transmission error.");
//					saveUnsentSMS(Constants.TEXT_MESSAGE);
//					break;
//				case SmsManager.RESULT_ERROR_RADIO_OFF:
//					//                        Toast.makeText(getBaseContext(), "Radio off", 
//					//                                Toast.LENGTH_SHORT).shw();
////					if(!isSend)
//					dialogForSMSStatus("SMS Failed","Unable to send sms. No network detected.");
//					saveUnsentSMS(Constants.TEXT_MESSAGE);
//					break; 
//				}
//			}
//		}, new IntentFilter(sent));

//		//---when the SMS has been delivered---
//		registerReceiver(new BroadcastReceiver(){
//			@Override
//			public void onReceive(Context arg0, Intent arg1) {
//				switch (getResultCode())
//				{
//				case Activity.RESULT_OK:
//					Toast.makeText(getBaseContext(), "SMS delivered", 
//							Toast.LENGTH_SHORT).show();
//					break;
//				case Activity.RESULT_CANCELED:
//					Toast.makeText(getBaseContext(), "SMS not delivered", 
//							Toast.LENGTH_SHORT).show();
//					break;                        
//				}
//			}
//		}, new IntentFilter(DELIVERED));

        //        SmsManager sms = SmsManager.getDefault();
        //		sms.sendTextMessage(Constants.SMS_RECEPIENT, null,
        //				Constants.TEXT_MESSAGE, sentPI, deliveredPI);

    }

    private void casting() {
        btn_MainPage = (Button) findViewById(R.id.btnNext);

    }

    public void onBackPressed() {

    }

    private void changeFont() {
//		Typeface tf = Typeface.createFromAsset(getAssets(), "times_new_roman_bold.ttf");
//
//		TextView congrats = (TextView) findViewById(R.id.tv_Congrats);
//		TextView completed = (TextView) findViewById(R.id.tv_Congrats);
//		TextView thankYou = (TextView)findViewById(R.id.tv_ThankYou);
//
////		btn_MainPage.setTypeface(tf);
////		congrats.setTypeface(tf);
////		completed.setTypeface(tf);
////		thankYou.setTypeface(tf);
    }


    public void dialogForSMSStatus(String title, String message) {
        isSend = true;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                ExitScreenActivity.this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setCancelable(true)
                .setNeutralButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                            }
                        });
        if (message != null && message.length() > 0)
            alertDialogBuilder.setMessage(message);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void saveUnsentSMS(String text) {

        if (!databaseHandler.checkIfMessageExist(text)) {
            databaseHandler.insertUnsentRegistration(text);
        }


        ArrayList<UnsentRegistration> listUnsent = databaseHandler.getAllUnsentRegistration();
        String output = "";
        for (int i = 0; i < listUnsent.size(); i++) {
            if (output.equals("")) {
                output = listUnsent.get(i).getMessage();
            } else {
                output += "\n" + listUnsent.get(i).getMessage();
            }

        }


        String textFileName = "UnsentRegistration.txt";
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/PSR_Registration_Form/Unsent Registration/";
        File dir = new File(dirPath);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Toast.makeText(context, "Failed to create directory.", Toast.LENGTH_LONG).show();
            }
        }

        File fileUnsentText = new File(dir, textFileName);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(fileUnsentText);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            out.write(output.getBytes());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        startService(new Intent(context, EmailService.class));

    }

}
