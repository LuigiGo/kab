package com.ph.com.psr_kap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.utilities.constants.Constants;

public class Async_FTP_Download_MD extends AsyncTask<String, String, Boolean> {

	private static final int DOWNLOAD_FILES_REQUEST = 1;

	private ProgressDialog progressDialog;

	String _userID, _message;

	Context mContext;

	String error = "", typeOfError = "";

	String ReportResponseCode = "", ReportResponseMessage = "";

	FTPClient client = new FTPClient();

	FileInputStream fis = null;

	// boolean result;
	boolean result = true;

	private static final String TAG_SUCCESS = "success";

	File _uploadFile;

	ArrayList<String> listRemoteFile;
	ArrayList<File> listDownloadFile;
	String str_Path;
	DatabaseHandler databaseHandler;
	
	// ==============
	// TIMER for text output
	Timer timer;
	TimerTask timerTask;
	// we are going to use a handler to be able to run in our TimerTask
	final Handler handler = new Handler();

	public Async_FTP_Download_MD(Context context) {

		this.mContext = context;

		listRemoteFile = new ArrayList<String>();
		listDownloadFile = new ArrayList<File>();
		// create directory
		str_Path = Environment.getExternalStorageDirectory().toString()
				+ "/Kabalikat_Advocacy_Program_2015/MD_Codes";

		File mediaStorageDir = new File(str_Path);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("MyCameraApp", "failed to create directory");
			}
		}

		databaseHandler = new DatabaseHandler(context);
		if (databaseHandler != null) {
			databaseHandler.close();
			databaseHandler.createDB();
		}

	}

	protected void onPreExecute() {

		progressDialog = new ProgressDialog(mContext);

		progressDialog.setMessage("Checking for updates. Please wait...");

		progressDialog.setCancelable(false);

		progressDialog.show();

	}

	@SuppressWarnings("resource")
	@Override
	protected Boolean doInBackground(String... params) {
		if (updateData() == true) {
			return true;
		} else {
			return false;
		}
	}

	protected void onPostExecute(Boolean valid) {

		try {

			progressDialog.dismiss();

			Log.i("RESULT", String.valueOf(valid));

			if (NW_Helper.isInternetAvailable(mContext)) {
				if (result == true) {
					databaseHandler.deleteTableMD();
					
					
					
					
					System.out.println("File md downloaded");
					Log.i("File md downloaded", "File md downloaded");

					Async_FTP_MD_Save a2 = new Async_FTP_MD_Save(mContext);
					a2.execute();

				} else {
					System.out.println("File not downloaded, RETRY");
					Log.i("File not downloaded, RETRY",
							"File not downloaded, RETRY");
					Async_FTP_Download_MD a1 = new Async_FTP_Download_MD(
							mContext);
					a1.execute();
				}
			} else {
				Toast.makeText(mContext,
						"Please check your internet connection.",
						Toast.LENGTH_LONG).show();
			}

			// if (valid) {
			// Log.i("RESULT VALID---------------",
			// "RESULT VALID---------------");
			// // Async_FTP_User_Save a = new Async_FTP_User_Save(mContext);
			// // a.execute();
			// } else {
			// Log.i("RESULT ELSE---------------",
			// "RESULT ELSE---------------");
			// Toast.makeText(mContext,
			// "Please check your internet connection.",
			// Toast.LENGTH_LONG).show();
			// }

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	public boolean updateData() {
		try {
			startTimer();
			
			// DELETE DATA and OVERRIDE THE PRELOADED PSR TABLE DATA


			FTPClient client = new FTPClient();
			FileOutputStream fos = null;
			boolean status;

			try {
				client.setConnectTimeout(30000);

				client.connect(Constants.FTP_ADDRESS);
				Log.d("CLIENT-----",
						"Connected. Reply: " + client.getReplyString());

				// client.login(Constants.FTP_USERNAME, Constants.FTP_PASSWORD);
				// Log.d("CLIENT-----", "Logged in");

				result = client.login(Constants.FTP_USERNAME,
						Constants.FTP_PASSWORD);

				if (result == true) {
					System.out
							.println("Successfully logged in! -------------------FTP-----------------");
					// client.enterLocalPassiveMode();

					client.setFileType(FTP.BINARY_FILE_TYPE);
					Log.d("CLIENT-----", "Downloading");

					client.changeWorkingDirectory(Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES);

					// Log.i("LOG IN STATUS:", String.valueOf(logged));

					FTPFile[] files = client.listFiles();

					for (FTPFile file : files) {

						if (file.getType() == FTPFile.FILE_TYPE) {

							System.out.println("File Name: " + file.getName());

						}

					}

					Log.i("fg", "asdfasdf");

					String filename1 = "MD_Codes.csv";
					String file = "MD_Codes.csv";
					String remoteFile1 = Constants.FTP_FOLDER_PSR_KABALIKAT_MD_CODES
							+ "/" + file;

					listRemoteFile.add(remoteFile1);
					Log.d("listRemoteFile-----", listRemoteFile.toString());

					// File where to put downloaded file
					File downloadFile1 = new File(str_Path + "/" + file);

					listDownloadFile.add(downloadFile1);
					Log.d("listDownloadFile-----", listDownloadFile.toString());
					int counter = 0;
					for (int i = 0; i < listRemoteFile.size(); i++) {
						counter++;
						OutputStream outputStream1 = new BufferedOutputStream(
								new FileOutputStream(listDownloadFile.get(i)));

						client.setBufferSize(1024 * 1024);
						boolean success = client.retrieveFile(
								listRemoteFile.get(i), outputStream1);

						Log.i("DownloadFile", listDownloadFile.get(i)
								.toString());
						Log.i("RemoteFile", listRemoteFile.get(i));

						if (success) {
							System.out.println("File #" + listRemoteFile.get(i)
									+ " has been downloaded successfully.");
							if(counter == listRemoteFile.size()){
								return true;
							}
							
						} else {
							System.out.println("File #" + listRemoteFile.get(i)
									+ " not downloaded.");
						}
						outputStream1.close();
					}
				}

				// OutputStream outputStream1 = new BufferedOutputStream(new
				// FileOutputStream(downloadFile1));
				//
				// client.setBufferSize(1024 * 1024);
				// boolean success = client.retrieveFile(listRemoteFile.get(i),
				// outputStream1);
				//
				// Log.i("DownloadFile", listDownloadFile.get(i).toString());
				// Log.i("RemoteFile", listRemoteFile.get(i));
				//
				// if (success) {
				// System.out.println("File #" + listRemoteFile.get(i)
				// + " has been downloaded successfully.");
				// }
				// else{
				// System.out.println("File #" + listRemoteFile.get(i)
				// + " not downloaded.");
				// }
				// outputStream1.close();

				return true;

			} catch (ConnectException ce) {
				ce.printStackTrace();

				return false;

			} catch (IOException e) {

				e.printStackTrace();

				return false;

			} finally {

				try {

					if (fos != null) {

						fos.close();

					}

					client.disconnect();

				} catch (IOException e) {

					e.printStackTrace();
				}

			}

		} catch (Exception e) {

			e.printStackTrace();

			return false;

		}

	}
	
	public void startTimer() {
		// set a new Timer
		timer = new Timer();

		// initialize the TimerTask's job
		initializeTimerTask();

		// schedule the timer, after the first 5000ms the TimerTask will run
		// every 10000ms
		timer.schedule(timerTask, 30000, 30000); //
	}

	public void stoptimertask() {
		// stop the timer, if it's not already null
		if (timer != null) {
			timer.cancel();
			timer = null;
		}
	}

	public void initializeTimerTask() {

		timerTask = new TimerTask() {
			public void run() {

				// use a handler to run a toast that shows the current timestamp
				handler.post(new Runnable() {
					public void run() {
						progressDialog.dismiss();
						stoptimertask();
//						Toast.makeText(
//								mContext,
//								"There was a problem in checking for updates. Please try again.",
//								Toast.LENGTH_LONG).show();

					}
				});
			}
		};
	}

}
