package com.ph.com.psr_kap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import android.util.Log;
import android.widget.Toast;

public class FTP_Handler {

	FTPClient mFtpClient;

	/**
	 * 
	 * @param <T>
	 * @param ip
	 * @param userName
	 * @param pass
	 */
	public boolean connnectingwithFTP(String ip, String userName, String pass,
			String workingDirectory) {

		boolean status = false;
		try {
			mFtpClient = new FTPClient();
			mFtpClient.setConnectTimeout(10 * 1000);
			mFtpClient.connect(InetAddress.getByName(ip));
//			mFtpClient.enterLocalPassiveMode();
			status = mFtpClient.login(userName, pass);
			Log.e("isFTPConnected", String.valueOf(status));

			if (status == true) {
				mFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
				if (workingDirectory != null) {
					mFtpClient.changeWorkingDirectory(workingDirectory);
				}
			} 

			// if (FTPReply.isPositiveCompletion(mFtpClient.getReplyCode())) {
			// mFtpClient.setFileType(FTP.ASCII_FILE_TYPE);
			// mFtpClient.enterLocalPassiveMode();
			// FTPFile[] mFileArray = mFtpClient.listFiles();
			// Log.e("Size", String.valueOf(mFileArray.length));
			// }
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return status;
	}

	public FTPClient getFTPClient() {
		return mFtpClient;
	}

	/**
	 * @param ftpClient
	 *            FTPclient object
	 * @param remoteFilePath
	 *            FTP server file path
	 * @param downloadFile
	 *            local file path where you want to save after download
	 * @return status of downloaded file
	 */
	public boolean downloadSingleFile(FTPClient ftpClient,
			String remoteFilePath, File downloadFile) {
		File parentDir = downloadFile.getParentFile();
		if (!parentDir.exists())
			parentDir.mkdir();
		OutputStream outputStream = null;
		try {
			outputStream = new BufferedOutputStream(new FileOutputStream(
					downloadFile));
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			return ftpClient.retrieveFile(remoteFilePath, outputStream);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	// public boolean downloadDirectory(FTPClient ftpClient, String
	// remoteDirPath,
	// String savePath) {
	//
	// FTPFile[] subFiles = null;
	// try {
	// subFiles = ftpClient.listFiles();
	// Log.i("subFiles.length", String.valueOf(subFiles.length));
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// if (subFiles != null && subFiles.length > 0) {
	// int count = subFiles.length;
	// int counter = 0;
	// for (FTPFile aFile : subFiles) {
	//
	// counter++;
	//
	// String currentFileName = aFile.getName();
	// Log.i("currentFileName", currentFileName);
	//
	// File downloadFile = new File(savePath, currentFileName);
	// String remoteFilePath = remoteDirPath + "/" + currentFileName;
	//
	// boolean isDownloaded = downloadSingleFile(mFtpClient,
	// remoteFilePath, downloadFile);
	//
	// Log.i(downloadFile.getName() + " - isDownloaded",
	// String.valueOf(isDownloaded));
	// if (isDownloaded == false) {
	// // Retry download
	// boolean isRetryDownloaded = downloadSingleFile(mFtpClient,
	// remoteFilePath, downloadFile);
	// Log.i(downloadFile.getName() + " - isRetryDownloaded",
	// String.valueOf(isRetryDownloaded));
	// if (isRetryDownloaded == false) {
	// boolean isRetryDownloaded2 = downloadSingleFile(
	// mFtpClient, remoteFilePath, downloadFile);
	// Log.i(downloadFile.getName() + " - isRetryDownloaded2",
	// String.valueOf(isRetryDownloaded2));
	// if (isRetryDownloaded2 == false) {
	// break;
	// }
	// }
	// } else {
	// if (counter == count) {
	// return true;
	// }
	// }
	// }
	// }
	//
	// return false;
	// }

	/**
	 * 
	 * @param ftpClient
	 *            FTPclient object
	 * @param downloadFile
	 *            local file which need to be uploaded.
	 */
	public void uploadFile(FTPClient ftpClient, File downloadFile,
			String serverfilePath) {
		try {
			FileInputStream srcFileStream = new FileInputStream(downloadFile);
			boolean status = ftpClient.storeFile("remote ftp path",
					srcFileStream);
			Log.e("Status", String.valueOf(status));
			srcFileStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
