package com.ph.com.psr_kap.utilities.build;


import com.ph.com.psr_kap.BuildConfig;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;

public class BuildConfigManager {

    private String TAG = this.getClass().getSimpleName();
    private static BuildConfigManager sBuildConfigManager;

    public static void init() {
        if (sBuildConfigManager == null) {
            sBuildConfigManager = new BuildConfigManager();
        }
    }

    public static BuildConfigManager getInstance() {
        if (sBuildConfigManager == null) {
            throw new IllegalStateException("Call BuildConfigManager init() method");
        }
        return sBuildConfigManager;
    }

    public String getVersionName() {
        LoggerHelper.log(TAG, "Getting version name: " + BuildConfig.VERSION_NAME);
        return BuildConfig.VERSION_NAME;
    }

    public String getVersionCode() {
        return String.valueOf(BuildConfig.VERSION_CODE);
    }
}
