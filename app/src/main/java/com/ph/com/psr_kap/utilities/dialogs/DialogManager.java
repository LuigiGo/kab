package com.ph.com.psr_kap.utilities.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.DatabaseHandler;
import com.ph.com.psr_kap.FTP_DownloadBrand;
import com.ph.com.psr_kap.FTP_DownloadMD;
import com.ph.com.psr_kap.NetworkHelper;
import com.ph.com.psr_kap.R;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;
import com.ph.com.psr_kap.utilities.validations.InputValidation;

import java.util.ArrayList;

public class DialogManager {

    private static DialogManager sDialogManager;
    private AlertDialog mAlertDialog;
    private String TAG = this.getClass().getSimpleName();

    public static void init() {
        if (sDialogManager == null) {
            sDialogManager = new DialogManager();
        }
    }

    public static DialogManager getInstance() {
        if (sDialogManager == null) {
            throw new IllegalStateException("Call DialogManager init()");
        }
        return sDialogManager;
    }

    public void showUpdateDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        builder.setTitle("");
        View view = inflater.inflate(R.layout.dialog_update, null);
        builder.setView(view);

        final CheckBox checkBox_MD = (CheckBox) view.findViewById(R.id.checkBox_MD);
        final CheckBox checkBox_BrandProduct = (CheckBox) view.findViewById(R.id.checkBox_BrandProduct);
        Button btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
        Button btn_sync = (Button) view.findViewById(R.id.btn_sync);

        // CANCEL BUTTON
        btn_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mAlertDialog.cancel();
            }
        });

        // SYNC BUTTON
        btn_sync.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // 1. PAG HINDI GUMANA YUNG TRUE, GAWIN PURO IF CONDITION AND
                // TRY
                // 2. Lagay sa unang if you may && then else if na yung tig
                // isang check lang - WORKING

                if (NetworkHelper.isInternetAvailable(context)) {
                    if (checkBox_MD.isChecked() == true
                            && checkBox_BrandProduct.isChecked() == true) {
                        Constants.isCheckBoth = true;
                        LoggerHelper.log(TAG, "MD and BrandProduct checked");
                        // IF BOTH CHECKED MIRROR ASYCH FOR MD AND PRODUCTS CALL
//						Async_FTP_Download_BrandProduct_Mirror a2 = new Async_FTP_Download_BrandProduct_Mirror(
//								context);
//						a2.execute();
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                        mAlertDialog.dismiss();

                    } else if (checkBox_BrandProduct.isChecked()) {
                        LoggerHelper.log(TAG, "BrandProduct checked");
//						Async_FTP_Download_BrandProduct asyncuser = new Async_FTP_Download_BrandProduct(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                        mAlertDialog.dismiss();

                    } else if (checkBox_MD.isChecked()) {
                        LoggerHelper.log(TAG, "MD checked");
//						Async_FTP_Download_MD asyncuser = new Async_FTP_Download_MD(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadMD md_codes = new FTP_DownloadMD(context);
                        md_codes.execute();
                        mAlertDialog.dismiss();

                    } else {
                        LoggerHelper.log(TAG, "Select first");
                        Toast.makeText(context, "Please select an item to update.",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }
        });

        mAlertDialog = builder.create();

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
        mAlertDialog.show();

    }

    public void showEmailSettingDialog(final Context context, final DatabaseHandler dbHandler) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        builder.setTitle("");
        View view = inflater.inflate(R.layout.dialog_email_settings, null);
        builder.setView(view);

        // cast
        final EditText txt_emailRecipient = (EditText) view.findViewById(R.id.txt_emailRecipient);
        TextView lbl_header_settings = (TextView) view.findViewById(R.id.lbl_header_settings);
        Button btn_addEmail = (Button) view.findViewById(R.id.btn_addEmail);
        final ListView lv_recipients = (ListView) view
                .findViewById(R.id.lv_recipients);

        ArrayList<String> recipient = dbHandler.getEmailRecipient();
        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, recipient);
        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lv_recipients.setAdapter(recipientAdapter);


        lv_recipients.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                final String emailAdd = (String) arg0.getItemAtPosition(position);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this email address?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Log.i("adsf", emailAdd);
                                        dbHandler.deleteRecipient(emailAdd);

                                        ArrayList<String> recipient = dbHandler.getEmailRecipient();
                                        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
                                        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        lv_recipients.setAdapter(recipientAdapter);
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

        btn_addEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String emailAdd = txt_emailRecipient.getText().toString();

                if (emailAdd.length() == 0) {
                    LoggerHelper.toast(context, "Please enter valid email");

                } else {
                    // Invalid Email Address
                    if (!InputValidation.isEmailValid(emailAdd)) {

                        LoggerHelper.toast(context, "Invalid email address");
                    } else {

                        if (!dbHandler.checkIfEmailExist(emailAdd)) {
                            dbHandler.addRecepient(emailAdd);

                            ArrayList<String> recipient = dbHandler.getEmailRecipient();
                            ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);

                            lv_recipients.setAdapter(recipientAdapter);

                            txt_emailRecipient.setText("");

                            LoggerHelper.toast(context, "Email address successfully added");

                        } else {
                            LoggerHelper.toast(context, "Email already exist");
                        }


                    }
                }

            }
        });

        mAlertDialog = builder.create();

        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
        mAlertDialog.show();

    }

}
