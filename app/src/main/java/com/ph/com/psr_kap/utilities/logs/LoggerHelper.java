package com.ph.com.psr_kap.utilities.logs;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


public class LoggerHelper {

    public static void log(String tag, String message) {
        Log.e(tag, " " + message);
    }

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
