package com.ph.com.psr_kap;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomAdapterSearch extends SimpleAdapter{
	
	private Context context;
	public DatabaseHandler databaseHandler;
	private final ArrayList<HashMap<String, String>> mData;
	private ArrayList<HashMap<String, String>> unfilteredValues;
	private boolean chosenValues, ismarkAll = true;
	private int resource;
	private String[] from;
	private int[] to;
	private SimpleFilter mFilter;
	private ArrayList<String> arraylistRecId;
	private ArrayList<String> arraylistSelected;
	
	
	private final boolean[] mCheckedState;

	public CustomAdapterSearch(Context context,
			ArrayList<HashMap<String, String>> data, int resource,
			String[] from, int[] to, boolean chosenValues) {
		super(context, data, resource, from, to);
		
		mCheckedState = new boolean[data.size()];
		
		this.context = context;
		mData = data;
		this.unfilteredValues = mData;
		this.resource = resource;
		this.from = from;
		this.to = to;
		this.arraylistRecId = new ArrayList<String>();
		this.arraylistSelected = new ArrayList<String>();
		this.chosenValues = chosenValues;
	
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		final int pos = position;
		//View rowView = null;
		try {
			//if(convertView == null){
				convertView = inflater.inflate(resource, null, true);

				TextView tv_id 	  	= (TextView) convertView.findViewById(to[0]);
				TextView tv_name 	= (TextView) convertView.findViewById(to[1]);
				TextView tv_mobile 	= (TextView) convertView.findViewById(to[2]);
				TextView tv_md 		= (TextView) convertView.findViewById(to[3]);
				TextView tv_psr 	= (TextView) convertView.findViewById(to[4]);
				TextView tv_brand 	= (TextView) convertView.findViewById(to[5]);
				TextView tv_couponcode 	= (TextView) convertView.findViewById(to[6]);

				// getting the value in hashmap
				final String _id 		  = unfilteredValues.get(position).get(from[0]).toString();
				String _name 		  	  = unfilteredValues.get(position).get(from[1]).toString();
				String _mobile  		  = unfilteredValues.get(position).get(from[2]).toString();
				String _md			  	  = unfilteredValues.get(position).get(from[3]).toString();
				final String _psr 	  	  = unfilteredValues.get(position).get(from[4]).toString();
				final String _brand	  	  = unfilteredValues.get(position).get(from[5]).toString();
				final String _couponcode  = unfilteredValues.get(position).get(from[6]).toString();

				tv_id.setText(_id);
				tv_name.setText(_name);
				tv_mobile.setText(_mobile);
				tv_md.setText(_md);
				tv_psr.setText(_psr);
				tv_brand.setText(_brand);
				tv_couponcode.setText(_couponcode);
				
				// Setting the ID
				tv_id.setId(position);
				tv_name.setId(position);
				tv_mobile.setId(position);
				tv_md.setId(position);
				tv_psr.setId(position);
				tv_brand.setId(position);
				tv_couponcode.setId(position);
				
			//}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (OutOfMemoryError E) {
			E.printStackTrace();
		}

		return convertView;
	}

	public ArrayList<String> getarraylistSelected() {
		return this.arraylistSelected;
	}

	public ArrayList<String> getArrayListRecordId() {
		return this.arraylistRecId;
	}

	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new SimpleFilter();
		}
		return mFilter;
	}

	public int getCount() {
		return unfilteredValues.size();
	}


	private class SimpleFilter extends Filter {
		@SuppressWarnings("unchecked")
		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			FilterResults results = new FilterResults();

			String prefixString = null == prefix ? null : prefix.toString()
					.toLowerCase();

			ArrayList<HashMap<String, String>> unfilteredValues;
			if (null != prefixString && prefixString.length() > 0) {

				synchronized (mData) {
					unfilteredValues = (ArrayList<HashMap<String, String>>) mData
							.clone();
				}

				for (int i = unfilteredValues.size() - 1; i >= 0; --i) {
					HashMap<String, String> h = unfilteredValues.get(i);

					String str = (String) h.get(from[0]).toString();
					if (!str.toLowerCase().startsWith(prefixString)) {
						unfilteredValues.remove(i);
					}

				}

				results.values = unfilteredValues;
				results.count = unfilteredValues.size();

			} else {
				synchronized (mData) {
					results.values = mData;
					results.count = mData.size();
				}
			}

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			unfilteredValues = (ArrayList<HashMap<String, String>>) results.values;
			notifyDataSetChanged();

		}
	}
}
