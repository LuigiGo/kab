package com.ph.com.psr_kap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.ProgressBar;

public class SplashScreen extends Activity {

	protected final static int RUNTIME = 5000;
	ProgressBar progressSplash1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen_layout);
		
		progressSplash1 = (ProgressBar)findViewById(R.id.prb_splash1);
		
		Thread splashThread = new Thread(){
			@Override 
			public void run() {
				// TODO Auto-generated method stub
				
				for(int l=0; l<3; l++){
					int progressStatus1 = 0;
					
					try {
												
						if (progressSplash1.getProgress() == 0){
							while(progressStatus1 < RUNTIME)
							{
								sleep(250);
								progressStatus1 += 1000;
								updateProgress(progressStatus1);
																
							}					
						}
						
					} catch (InterruptedException e) {
						// TODO: handle exception
					}										
				}
				finish();			
				Intent intent = new Intent(getApplicationContext(), Login.class);
				startActivity(intent);
				
			}
		};
		splashThread.start();
	}

public void updateProgress(final int timePassed) {
        
        //final int progress = progressSplash1.getMax() * timePassed / RUNTIME;
	 		final int progress1 = progressSplash1.getMax() * timePassed / RUNTIME;
	 		
        progressSplash1.setProgress(progress1);
        
}
}
