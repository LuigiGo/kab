package com.ph.com.psr_kap;
/**
 * Created by Corporate IT for United Laboratories, Inc.
 * Date Created: October 1, 2013
 * Created by: Rose Jessica Cabigao
 * 				Jess Mark Panadero
 * 				Leo Paul Marcellana
 * Short Description
 * Date updated
 **/
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
public class BrandListAdapter extends BaseAdapter
{
	Typeface tfR;
	//static boolean isSelectAll = false;
	public static ArrayList<String> mListItems;
	private LayoutInflater mInflater;
	private TextView mSelectedItems;
	public static int selectedCount = 0;
	public static int clickHolder = 1;
	private static String firstSelected = "";
	public ViewHolder holder;
	private static String selected = "";	//shortened selected values representation
	static boolean ismarkAll = false;
	public static boolean index1Holder = false;
	//public static boolean[] brandSelected;
	public static String getSelected() 
	{
		return selected;
	}

	public void setSelected(String selected) 
	{
		BrandListAdapter.selected = selected;
	}

	public BrandListAdapter(Context context, ArrayList<String> items,
			TextView tv) 
	{
		mListItems = new ArrayList<String>();
		mListItems.addAll(items);
		mInflater = LayoutInflater.from(context);
		mSelectedItems = tv;
		RegistrationPage.brandSelected = new boolean[items.size()];
		selectedCount = selectedCount + RegistrationPage.BrandCount;
		for (int i = 0; i < RegistrationPage.checkSelected.length; i++) {
			Log.d("checkSelected " + i , String.valueOf(RegistrationPage.checkSelected[i]));
		}
	
	}

	@Override
	public int getCount() 
	{
		return mListItems.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		
		if (convertView == null) {
			
			convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
			holder = new ViewHolder();
			holder.tv = (TextView) convertView.findViewById(R.id.SelectOption);
			holder.chkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
			holder.tv.setTypeface(tfR);
//			holder.tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,RegistrationPage.font_content);
			
		//	holder.row = (ViewGroup)RegistrationPage.list.getChildAt(0);
		//	holder.check = (CheckBox)holder.row.findViewById(R.DropDownList.checkbox);
			
			

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		//holder.chkbox.setChecked(brandSelected[position]);
		holder.tv.setText(mListItems.get(position));

		final int position1 = position;
		
		//whenever the checkbox is clicked the selected values textview is updated with new selected values
		holder.chkbox.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				
				if (position1 == 0 && clickHolder == 1)
				{
					selectedCount = 0;
					clickHolder++;
					CheckAll();
				}else if (position1 == 0 && clickHolder == 2){
					clickHolder--;
					index1Holder = false;
					UncheckAll();
				}else{
					
					if(RegistrationPage.brandSelected[position] = true){
						RegistrationPage.brandSelected[position] = false;
						
					}else{
						RegistrationPage.brandSelected[position] = true;
						
					}
					int totalBrands = RegistrationPage.checkSelected.length;
					boolean isSelectAll = true;
					for (int i = 0; i < totalBrands; i++) {
						if (RegistrationPage.brandSelected[i] == false) {
							isSelectAll = false;
						}
					}
					if(isSelectAll == false){
						RegistrationPage.brandSelected[0] = false;
						index1Holder = false;
						try{
							holder.row = (ViewGroup)RegistrationPage.list.getChildAt(0);
							holder.check = (CheckBox)holder.row.findViewById(R.id.checkbox);
							holder.textview = (TextView)holder.row.findViewById(R.id.SelectOption);
							holder.textview.setText("Select All");
							clickHolder = 1;
							holder.check.setChecked(false);
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}
					
						
				}
				
				setText(position1);	
			}

		});
		
		if(RegistrationPage.checkSelected[0] = true && clickHolder == 2){
			RegistrationPage.checkSelected[0] = false;
		}
		
		if(index1Holder == true){
			RegistrationPage.checkSelected[0] = true;
				holder.chkbox.setChecked(true);
		}else{
			RegistrationPage.checkSelected[0] = false;
			holder.chkbox.setChecked(false);
		}
		
		int totalBrands = RegistrationPage.checkSelected.length - 1;
		
		Log.i("total brands", String.valueOf(totalBrands));
		Log.i("selectedCount", String.valueOf(selectedCount));
		
		if(selectedCount <  totalBrands){
			RegistrationPage.checkSelected[0] = false;
			holder.chkbox.setChecked(false);
			clickHolder = 1;
			
		}else if(selectedCount >=  totalBrands){
			RegistrationPage.checkSelected[0] = true;
			holder.chkbox.setChecked(true);
			
		}
		
		Log.d("value of index 0", "brandSelected: " + String.valueOf(RegistrationPage.brandSelected[0]) + ",checkSelectedSelected: " + String.valueOf(RegistrationPage.checkSelected[0]));
		if(RegistrationPage.brandSelected[position] || RegistrationPage.checkSelected[position])
			holder.chkbox.setChecked(true);
		else
			holder.chkbox.setChecked(false);
		
			
		return convertView;
	}


	/*
	 * Function which updates the selected values display and information(checkSelected[])
	 * */
	private void setText(int position1)
	{
		Log.i("selectedCount 1st", String.valueOf(selectedCount));
		Log.i("BrandCount", String.valueOf(RegistrationPage.BrandCount));
		
		if (!RegistrationPage.checkSelected[position1]) {
			
				RegistrationPage.checkSelected[position1] = true;
				Log.i("true pos", String.valueOf(position1));
				if(position1 != 0){
					selectedCount++;
				}
				
			
		} else {
			
				RegistrationPage.checkSelected[position1] = false;
				Log.i("false pos", String.valueOf(position1));
				if(position1 != 0){
					selectedCount--;
				}
				
			
		}

		Log.i("sss", String.valueOf(RegistrationPage.checkSelected[0]));
		Log.i("sss selectedCount", String.valueOf(selectedCount));
		
		if (RegistrationPage.checkSelected[0] = true && selectedCount == 0) {
			mSelectedItems.setText("Select Brand");
			RegistrationPage.BrandCount = 0;

		} else if (selectedCount == 1) {
			for (int i = 0; i < RegistrationPage.checkSelected.length; i++) 
			{
				if (RegistrationPage.checkSelected[i] == true) {
					firstSelected = mListItems.get(i);
					break;
				}
			}
			mSelectedItems.setText(firstSelected);
			setSelected(firstSelected);
		}else if (selectedCount >= 2) 
			{
				Log.i("index 0", String.valueOf(RegistrationPage.checkSelected[0]));
				Log.i("selected", String.valueOf(selectedCount));
				for (int i = 0; i < RegistrationPage.checkSelected.length; i++) {
					if (RegistrationPage.checkSelected[i] == true) {
						firstSelected = mListItems.get(i);
						break;
					}
				}
				Log.e("tag", firstSelected + " & " + (selectedCount - 1)
						+ " more");
				RegistrationPage.lbl_BrandSelected.setText(firstSelected + " & " + (selectedCount - 1)
						+ " more");
	

		}
		
	
	}
	private class ViewHolder {
		TextView tv, textview;
		ViewGroup row, row01;
		CheckBox chkbox,check;
	}
	

	public void CheckAll(){
		int count = RegistrationPage.list.getChildCount();
		try{
			for (int i = 0; i < count; i++) 
			{
				holder.row = (ViewGroup)RegistrationPage.list.getChildAt(i);
				holder.row01 = (ViewGroup)RegistrationPage.list.getChildAt(0);
				holder.check = (CheckBox)holder.row.findViewById(R.id.checkbox);
				holder.textview = (TextView)holder.row01.findViewById(R.id.SelectOption);
				holder.check.setChecked(true);
				holder.textview.setText("Unselect All");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		for (int i = 0; i < RegistrationPage.brandSelected.length; i++) {
			RegistrationPage.brandSelected[i] = true;
			RegistrationPage.checkSelected[i] = true;
			selectedCount++;
		}
		selectedCount = selectedCount - 1;
	}
	
	public void UncheckAll()
	{
		int count = RegistrationPage.list.getChildCount();
		try{
			for (int i = 0; i < count; i++) 
			{
				holder.row = (ViewGroup)RegistrationPage.list.getChildAt(i);
				holder.row01 = (ViewGroup)RegistrationPage.list.getChildAt(0);
				holder.check = (CheckBox)holder.row.findViewById(R.id.checkbox);
				holder.textview = (TextView)holder.row01.findViewById(R.id.SelectOption);
				holder.textview.setText("Select All");
				holder.check.setChecked(false);
			}
		}catch(Exception e){
				e.printStackTrace();
		}
		for (int i = 0; i < RegistrationPage.brandSelected.length; i++) {
			RegistrationPage.brandSelected[i] = false;
			RegistrationPage.checkSelected[i] = false;
			selectedCount--;
		}
		
		selectedCount = 0;
		RegistrationPage.BrandCount = 0;
	}
}
