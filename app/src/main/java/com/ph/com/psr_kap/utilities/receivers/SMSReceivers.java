package com.ph.com.psr_kap.utilities.receivers;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.ph.com.psr_kap.DatabaseHandler;
import com.ph.com.psr_kap.R;
import com.ph.com.psr_kap.RegistrationPage;
import com.ph.com.psr_kap.UnsentRegistration;
import com.ph.com.psr_kap.service.EmailService;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SMSReceivers extends BroadcastReceiver {

    private String TAG = this.getClass().getSimpleName();
    private DatabaseHandler databaseHandler;

    @Override
    public void onReceive(Context context, Intent intent) {
        LoggerHelper.log(TAG, intent.getAction() + " " + intent.getStringExtra(Constants.INTENT_ACTION_TAG));

        String strActionType = intent.getStringExtra(Constants.INTENT_ACTION_TAG);
        databaseHandler = new DatabaseHandler(context);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }

        switch (strActionType) {
            case Constants.SMS_SENT_RECEIVER:
                LoggerHelper.log(TAG, strActionType + "-" + getResultCode());
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        LoggerHelper.log(TAG, "Sms sent");
                        dialogForSMSStatus(context, "SMS succesfully sent", null);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        LoggerHelper.log(TAG, "Generic failure cause");
                        dialogForSMSStatus(context, "SMS Failed", "Unable to send sms. Your load balance is not enough to send a text.");
                        saveUnsentSMS(context, Constants.TEXT_MESSAGE);
                        break;

                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        LoggerHelper.log(TAG, "Failed because radio was explicitly turned off");
                        dialogForSMSStatus(context, "SMS Failed", "Unable to send sms. No network detected.");
                        saveUnsentSMS(context, Constants.TEXT_MESSAGE);
                        break;

                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        LoggerHelper.log(TAG, "Failed because no pdu provided");
                        dialogForSMSStatus(context, "SMS Failed", "Unable to send sms. Network transmission error.");
                        saveUnsentSMS(context, Constants.TEXT_MESSAGE);
                        break;

                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        LoggerHelper.log(TAG, "Failed because service is currently unavailable");
                        dialogForSMSStatus(context, "SMS Failed", "Unable to send sms. No network detected.");
                        saveUnsentSMS(context, Constants.TEXT_MESSAGE);
                        break;

                }
                break;

//            case Constants.SMS_DELIVERED_RECEIVER:
//                LoggerHelper.log(TAG, String.valueOf(getResultCode()));
//
//                switch (getResultCode()) {
//                    case Activity.RESULT_OK:
//                        LoggerHelper.log(TAG, "SMS delivered");
//                        break;
//                    case Activity.RESULT_CANCELED:
//                        LoggerHelper.log(TAG, "SMS not delivered");
//                        break;
//                }
//                break;
        }
    }

    public void dialogForSMSStatus(Context context, String title, String message) {

        Intent notificationIntent = new Intent(context, RegistrationPage.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(0)
                        .setContentTitle("Kabalikat : " + title)
                        .setContentText(message);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setAutoCancel(true);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentIntent(contentIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Constants.noticationCount++;
        mNotificationManager.notify(Constants.noticationCount, mBuilder.build());
        Log.i(TAG, "display notificatnio " + title + " message " + message);
    }

    private void saveUnsentSMS(Context context, String text) {
        if (!databaseHandler.checkIfMessageExist(text)) {
            databaseHandler.insertUnsentRegistration(text);
        }

        ArrayList<UnsentRegistration> listUnsent = databaseHandler.getAllUnsentRegistration();
        String output = "";
        for (int i = 0; i < listUnsent.size(); i++) {
            if (output.equals("")) {
                output = listUnsent.get(i).getMessage();
            } else {
                output += "\n" + listUnsent.get(i).getMessage();
            }
        }

        String textFileName = "UnsentRegistration.txt";
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/PSR_Registration_Form/Unsent Registration/";
        File dir = new File(dirPath);
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Toast.makeText(context, "Failed to create directory.", Toast.LENGTH_LONG).show();
            }
        }

        File fileUnsentText = new File(dir, textFileName);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(fileUnsentText);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            out.write(output.getBytes());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        context.startService(new Intent(context, EmailService.class));
    }
}
