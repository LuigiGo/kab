/****************************************************************************
 * App Name	:		KABALIKAT Mobile Registration						*
 * *
 * Description :		Mobile app that will allow PSRs to register their   *
 * patients using mobile devices.						*
 * *
 * Date Created:		January 8, 2013										*
 * *
 * Author		: 		Rose Jessica M. Cabigao								*
 * *
 * Date Updated:		February 3, 2013									*
 * *
 * Updates		:		Modify the database.								*
 ****************************************************************************/
package com.ph.com.psr_kap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.com.psr_kap.features.base.BaseActivity;
import com.ph.com.psr_kap.utilities.constants.Constants;
import com.ph.com.psr_kap.utilities.dialogs.DialogManager;
import com.ph.com.psr_kap.utilities.formatter.InputFormatter;
import com.ph.com.psr_kap.utilities.logs.LoggerHelper;
import com.ph.com.psr_kap.utilities.validations.InputValidation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationPage extends BaseActivity {
    public static final String TAG = RegistrationPage.class.getSimpleName();
//    Typeface tnr_normal, tnr_bold_italic, tnr_bold, monotype;
    TextView lbl_main, lbl_info;
    EditText txt_psr_name, txt_fname, txt_lname, txt_mobile,
            txt_landline;
    Spinner txt_psr_code;
    AutoCompleteTextView txt_md;
    DatabaseHandler databaseHandler;
    ListView lbl_Selectedbrand;
    static TextView lbl_BrandSelected;
    //    float font_title;
//    static float font_content;
    private boolean expanded;
    ArrayList<String> brands = new ArrayList<String>();
    public static boolean[] checkSelected;
    public static boolean[] brandSelected;
    private PopupWindow pw;
    public static ListView list;
    public static int BrandCount = 0;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferencesEditor;
    String message = "", psr_code = "", psr_name = "";
    ArrayList<String> arraySelectedBrand;
    boolean doubleBackToExitPressedOnce = false;
    String md_code = "", patient_code = "", patient_lname = "",
            patient_fname = "", patient_mobile = "", patient_landline = "";
    Button btn_register, btn_clear, btn_admin, btn_update, menu_btn_Update;
    Button btn_menu;
    boolean menu_clicked = false, isToDelete = false;
    List<String> brand;
    RelativeLayout dropdown_menu, registration_page;
//	Animation anim_preview, anim_close_preview;

    AlertDialog alertDialogEmail;

    Context context;


    //FAYE=======

    Cursor c_mdlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**Initialize helper classes*/
        DialogManager.init();

        setContentView(R.layout.activity_registration);
        Constants.isCheckBoth = false;
        sharedPreferences = getSharedPreferences(
                Constants.SHARED_PREFERENCES_CREATOR, 0);
        sharedPreferencesEditor = sharedPreferences.edit();

//        // typeface
//        tnr_normal = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_normal.ttf");
//        tnr_bold_italic = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_bold_italic.ttf");
//        tnr_bold = Typeface.createFromAsset(getAssets(),
//                "times_new_roman_bold.ttf");
//        monotype = Typeface.createFromAsset(getAssets(), "Monotype.TTF");

//        font_title = getResources().getDimensionPixelSize(R.dimen.font_title);
//        font_content = getResources().getDimensionPixelSize(
//                R.dimen.font_content);

        context = this;


        databaseHandler = new DatabaseHandler(this);
        if (databaseHandler != null) {
            databaseHandler.close();
            databaseHandler.createDB();
        }

        CastViews();
        initializeBrand();
        initializePSR();
        c_mdlist = databaseHandler._mdLIST(Constants.g_psrCode);


//		 c_mdlist2 = databaseHandler._searchName();

        if (Constants.isUpdate == true) {
            btn_register.setVisibility(View.GONE);
            btn_update.setVisibility(View.VISIBLE);

            show_details(Constants.UpdateID);
        } else {
            clear_entry2();

        }


    }

    private void show_details(String updateID) {
        Cursor _details = databaseHandler.getDetailstoUpdate(updateID);
        List<String> arrayList_brandcode = new ArrayList<String>();
        ArrayList<String> arrayList_brandpos = new ArrayList<String>();
        if (_details.getCount() != 0) {

            arrayList_brandcode.clear();
            arrayList_brandpos.clear();

            patient_code = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_CODE));
            String _fname = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_FNAME));
            String _lname = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_LNAME));
            String _mobile = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_MOBILE));
            String _landline = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_LANDLINE));

            String md_code = _details.getString(_details
                    .getColumnIndex(Constants.PATIENT_MD));
            String md_name = databaseHandler.get_md_name(md_code);

            Log.i("patient_code", patient_code);

            Cursor s = databaseHandler.get_brands_prescribed(patient_code);
            for (s.moveToFirst(); !s.isAfterLast(); s.moveToNext()) {
                arrayList_brandcode.add(s.getString(s
                        .getColumnIndex(Constants.BRAND_CODE)));
            }

            for (int i = 0; i < arrayList_brandcode.size(); i++) {
                String brand_pos = databaseHandler
                        .get_brand_id(arrayList_brandcode.get(i));
                arrayList_brandpos.add(brand_pos);
            }

            brand.remove(0);
            for (int i = 0; i < arrayList_brandpos.size(); i++) {
                checkSelected[Integer.parseInt(arrayList_brandpos.get(i))] = true;
                if (i == 0) {
                    int pos = (Integer.parseInt(arrayList_brandpos.get(i)) - 1);
                    if (arrayList_brandpos.size() > 1) {
                        lbl_BrandSelected
                                .setText(brand.get(pos)
                                        + " & "
                                        + String.valueOf(arrayList_brandpos
                                        .size() - 1));
                    } else {
                        lbl_BrandSelected.setText(brand.get(pos));
                    }
                }
            }
            BrandListAdapter.selectedCount = arrayList_brandpos.size();

            txt_fname.setText(_fname);
            txt_lname.setText(_lname);
            txt_mobile.setText(_mobile.replace("+", ""));
            txt_landline.setText(_landline);
            txt_md.setText(md_name);

        }

    }

    private void initializePSR() {
//		psr_code = sharedPreferences.getString(Constants.SP_PSR_CODE, "");
//		Cursor c = databaseHandler.get_PSR_details(psr_code);
//		psr_name = c.getString(c.getColumnIndex(Constants.PSR_NAME));

//		Log.i(Constants.TAG, psr_code);
//		txt_psr_code.setText(psr_code);
//		txt_psr_name.setText(psr_name);
    }

    private void initializeBrand() {
        brand = new ArrayList<String>();
        brand = databaseHandler.setItemOnBrandSpinner();
        brands.addAll(brand);

        Log.d("BRANDS: ", brand.toString());
        Log.d("count", String.valueOf(brand.size()));
        checkSelected = new boolean[brands.size()];

        lbl_BrandSelected.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("pop1", "expanded");
                if (!expanded) {
                    // display all selected values
                    String selected = "";
                    int flag = 0;
                    for (int i = 0; i < brands.size(); i++) {
                        if (checkSelected[i] == true) {
                            selected += brands.get(i);
                            selected += ", ";
                            flag = 1;
                        }
                    }
                    if (flag == 1)
                        lbl_BrandSelected.setText(selected);

                    expanded = true;
                } else {
                    // display shortened representation of selected values
                    lbl_BrandSelected.setText(BrandListAdapter.getSelected());
                    expanded = false;
                }
            }
        });

        // onClickListener to initiate the dropDown list
        lbl_BrandSelected.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                Log.i("pop", "initiatepop up");
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        initiatePopUp(brands, lbl_BrandSelected);

                    }
                }, 200);


            }
        });

    }

    @SuppressWarnings("deprecation")
    void initiatePopUp(ArrayList<String> brands, TextView tv_Brand) {
        LayoutInflater inflater = (LayoutInflater) RegistrationPage.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get the pop-up window i.e. drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(
                R.layout.pop_up_window,
                (ViewGroup) findViewById(R.id.PopUpView));


        // get the view to which drop-down layout is to be anchored
        RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.rel_brand);
        pw = new PopupWindow(layout, LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT, true);

        // Pop-up window background cannot be null if we want the pop-up to
        // listen touch events outside its window
        pw.setBackgroundDrawable(new BitmapDrawable());
        pw.setTouchable(true);

        // let pop-up be informed about touch events outside its window. This
        // should be done before setting the content of pop-up
        pw.setOutsideTouchable(true);
        int w = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                600, context.getResources().getDisplayMetrics());
        pw.setWidth(w);
        pw.setHeight(LayoutParams.WRAP_CONTENT);


        // dismiss the pop-up i.e. drop-down when touched anywhere outside the
        // pop-up
        pw.setTouchInterceptor(new OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        // provide the source layout for drop-down
        pw.setContentView(layout);

        // anchor the drop-down to bottom-left corner of 'layout1'
        pw.showAsDropDown(layout1);

        // populate the drop-down list
        list = (ListView) layout
                .findViewById(R.id.dropDownListInfoType);
        BrandListAdapter adapter = new BrandListAdapter(this, brands, tv_Brand);
        // list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        list.setAdapter(adapter);

    }

    private void CastViews() {

        // btn_admin = (Button)findViewById(R.id.menu_btn_admin);
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_update = (Button) findViewById(R.id.btn_update);

        dropdown_menu = (RelativeLayout) findViewById(R.id.dropdown_menu);
        registration_page = (RelativeLayout) findViewById(R.id.registration_page);
//		anim_preview = AnimationUtils.loadAnimation(this, R.anim.anim_preview);
//		anim_close_preview = AnimationUtils.loadAnimation(this,
//				R.anim.anim_close_preview);

        // Button
        btn_menu = (Button) findViewById(R.id.btn_menu);

        // EditText
        txt_psr_code = (Spinner) findViewById(R.id.txt_psrcode);
        txt_psr_name = (EditText) findViewById(R.id.txt_psrName);
        txt_fname = (EditText) findViewById(R.id.txt_fname);
        txt_lname = (EditText) findViewById(R.id.txt_lname);
        txt_mobile = (EditText) findViewById(R.id.txt_mobile);
        lbl_main = (TextView) findViewById(R.id.lbl_main);
        lbl_info = (TextView) findViewById(R.id.lbl_info);
        int mobileLength = txt_mobile.getText().toString().length();
        txt_mobile.setSelection(mobileLength);
        txt_landline = (EditText) findViewById(R.id.txt_landline);
        int landlineLength = txt_landline.getText().toString().length();
        txt_landline.setSelection(landlineLength);

        // spinner
        txt_md = (AutoCompleteTextView) findViewById(R.id.txt_md1);
        txt_md.addTextChangedListener(searchName);
        // textView
        lbl_BrandSelected = (TextView) findViewById(R.id.lbl_Selectedbrand);

        TextView lbl_psr_code = (TextView) findViewById(R.id.lbl_psrcode);
        TextView lbl_psrname = (TextView) findViewById(R.id.lbl_psrname);
        TextView lbl_fname = (TextView) findViewById(R.id.lbl_fname);
        TextView lbl_lname = (TextView) findViewById(R.id.lbl_lname);
        TextView lbl_mobile = (TextView) findViewById(R.id.lbl_mobile);
        TextView lbl_landline = (TextView) findViewById(R.id.lbl_landline);
        TextView lbl_brand = (TextView) findViewById(R.id.lbl_brand);
        TextView lbl_md = (TextView) findViewById(R.id.lbl_md);

//        btn_register.setTypeface(tnr_bold);
//        btn_register.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        btn_clear.setTypeface(tnr_bold);
//        btn_clear.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        btn_update.setTypeface(tnr_bold);
//        btn_update.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//
//        txt_md.setTypeface(tnr_normal);
//        txt_md.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
////		txt_psr_code.setTypeface(tnr_normal);
////		txt_psr_code.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        lbl_main.setTypeface(tnr_bold_italic);
//        lbl_info.setTypeface(tnr_bold_italic);
//        txt_psr_name.setTypeface(tnr_normal);
//        txt_psr_name.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        txt_fname.setTypeface(tnr_normal);
//        txt_fname.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        txt_lname.setTypeface(tnr_normal);
//        txt_lname.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        txt_mobile.setTypeface(tnr_normal);
//        txt_mobile.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        txt_landline.setTypeface(tnr_normal);
//        txt_landline.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//        lbl_BrandSelected.setTypeface(tnr_normal);
//        lbl_BrandSelected.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_content);
//
//        lbl_psr_code.setTypeface(tnr_bold);
//        lbl_psr_code.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_psrname.setTypeface(tnr_bold);
//        lbl_psrname.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_fname.setTypeface(tnr_bold);
//        lbl_fname.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_lname.setTypeface(tnr_bold);
//        lbl_lname.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_mobile.setTypeface(tnr_bold);
//        lbl_mobile.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_landline.setTypeface(tnr_bold);
//        lbl_landline.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_brand.setTypeface(tnr_bold);
//        lbl_brand.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);
//        lbl_md.setTypeface(tnr_bold);
//        lbl_md.setTextSize(TypedValue.COMPLEX_UNIT_PX, font_title);

        final ArrayList<PSRhandler> arr_psr = databaseHandler.getPSR();
        ArrayList<String> psr_spin = new ArrayList<String>();
        ArrayList<String> psr = new ArrayList<String>();
        if (arr_psr.size() > 0) {
            for (PSRhandler item : arr_psr) {
                psr_spin.add(item.getPsr_name().toUpperCase());
                psr.add(item.getPsr_code());
            }
        }
        ArrayAdapter<String> adapter_spin = new ArrayAdapter<String>(context,
                R.layout.spinner_item, psr_spin);
        adapter_spin.setDropDownViewResource(android.R.layout.simple_list_item_1);
        txt_psr_code.setAdapter(adapter_spin);

        txt_psr_code.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                Constants.g_psrCode = arr_psr.get(position).getPsr_code().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        registration_page.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(Constants.TAG, "touch");
                menu_clicked = false;
                dropdown_menu.setVisibility(View.GONE);

            }
        });

//		txt_mobile.setOnKeyListener(new OnKeyListener() {
//
//			@Override
//			public boolean onKey(View v, int keyCode, KeyEvent event) {
//				if (keyCode == event.KEYCODE_DEL) {
//					Log.i("KEYCODE", String.valueOf(keyCode));
//					if ((txt_mobile.getText().toString() == "639" || txt_mobile
//							.getText().toString().length() == 3)) {
//						return true;
//					}
//
//				}
//				return false;
//			}
//		});

        txt_landline.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int landline = txt_landline.getText().toString().length();

                if (landline != 0) {

                    String firstCharacter = txt_landline.getText().toString()
                            .substring(0, 1);
                    Log.e("firstChar", firstCharacter);

                    if (landline == 5) {
                        if (txt_landline.getText().toString().contains("(")
                                || txt_landline.getText().toString()
                                .contains(")")) {

                            String eChar = txt_landline.getText().toString()
                                    .replace("(", "");
                            String _eChar = eChar.replace(")", "");
                            String eChar_ = _eChar.replace(" ", "");

                            txt_landline.removeTextChangedListener(this);
                            txt_landline.setText(eChar_);
                            int length = txt_landline.getText().toString()
                                    .length();
                            txt_landline.setSelection(length);
                            txt_landline.addTextChangedListener(this);

                        }

                    } else if (landline == 8) {

                        String eChar = txt_landline.getText().toString()
                                .replace("(", "");
                        String _eChar = eChar.replace(")", "");
                        String eChar_ = _eChar.replace(" ", "");

                        txt_landline.removeTextChangedListener(this);
                        txt_landline.setText(eChar_);
                        int length = txt_landline.getText().toString().length();
                        txt_landline.setSelection(length);
                        txt_landline.addTextChangedListener(this);

                    } else if (landline == 9) {

                        if (!firstCharacter.equals("(")) {

                            String landline4 = txt_landline.getText()
                                    .toString();

                            String _firstCharacter = landline4.substring(0, 2);
                            String secondCharacter = landline4.substring(2, 5);
                            String thirdCharacter = landline4.substring(5, 9);

                            txt_landline.removeTextChangedListener(this);
                            txt_landline.setText("(" + _firstCharacter + ") "
                                    + secondCharacter + " - " + thirdCharacter);
                            int length = txt_landline.getText().toString()
                                    .length();
                            txt_landline.setSelection(length);
                            txt_landline.addTextChangedListener(this);
                            Log.i("if", txt_landline.getText().toString());
                        } else {
                            Log.i("else", "else");
                        }

                    } else if (landline == 15) {

                        String landline1 = txt_landline.getText().toString()
                                .replace("(", "");
                        String landline2 = landline1.replace(")", "");
                        String landline3 = landline2.replace(" ", "");
                        String landline4 = landline3.replace("-", "");

                        String _firstCharacter = landline4.substring(0, 2);
                        String secondCharacter = landline4.substring(2, 5);
                        String thirdCharacter = landline4.substring(5, 9);

                        txt_landline.removeTextChangedListener(this);
                        txt_landline.setText("(" + _firstCharacter + ") "
                                + secondCharacter + " - " + thirdCharacter);
                        int length = txt_landline.getText().toString().length();
                        txt_landline.setSelection(length);
                        txt_landline.addTextChangedListener(this);

                    } else if (landline == 16) {

                        String landline1 = txt_landline.getText().toString()
                                .replace("(", "");
                        String landline2 = landline1.replace(")", "");
                        String landline3 = landline2.replace(" ", "");
                        String landline4 = landline3.replace("-", "");

                        String _firstCharacter = landline4.substring(0, 3);
                        String secondCharacter = landline4.substring(3, 6);
                        String thirdCharacter = landline4.substring(6, 10);

                        txt_landline.removeTextChangedListener(this);
                        txt_landline.setText("(" + _firstCharacter + ") "
                                + secondCharacter + " - " + thirdCharacter);
                        int length = txt_landline.getText().toString().length();
                        txt_landline.setSelection(length);
                        txt_landline.addTextChangedListener(this);

                    }

                }
            }
        });
    }

    private TextWatcher searchName = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

//			AsyncLoadMD async = new AsyncLoadMD(s.toString());
//			async.execute();
//			
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if (txt_md.length() >= 1) {
                filterQueryName(s.toString());
            }

        }


    };

    private void filterQueryName(String _constraint) {
        Cursor c;

        if (c_mdlist.getCount() == 0) {
            Constants.choosefilterQueryName = false;
            c = databaseHandler._searchName(_constraint);
        } else {
            Constants.choosefilterQueryName = true;
            c = databaseHandler._searchName(_constraint, Constants.g_psrCode);
        }
        autoCompleteName(c);

//		Log.i("ELI", "" + txt_md.getText().length());
//		if (_constraint.equals("") || _constraint.equals(null) || txt_md.getText().length() <= 3){
//			
//			Constants.choosefilterQueryName = false;
//			c = databaseHandler._searchName();
//			Log.i("wala","" + c.getCount());
//		}
//		else{
//			Constants.choosefilterQueryName = true;
//			c = (Cursor) databaseHandler._searchName(_constraint,Constants.g_psrCode);
//			Log.i("may laman","" + c.getCount());
//			
////			autoCompleteNameA(c);
//			
//		}
        autoCompleteName(c);
    }

    @SuppressWarnings("null")
    private void autoCompleteName(Cursor c) {
        // TODO Auto-generated method stub
        int unexported_Data = c.getCount();
        Log.i("", String.valueOf(unexported_Data));
        List<String> _names = new ArrayList<String>();

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            if (Constants.choosefilterQueryName == false) {
                String temp_name = c.getString(c.getColumnIndex(Constants.MD_NAME));
                if (!_names.contains(temp_name)) {
                    _names.add(temp_name);
                }
            } else {
                String temp_name = c.getString(c.getColumnIndex(Constants.MDLIST_NAME));
                if (!_names.contains(temp_name)) {
                    _names.add(temp_name);
                }
            }

        }

        Collections.sort(_names);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                RegistrationPage.this, R.layout.spinnerattributecell, _names) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
//                ((TextView) v).setTypeface(tnr_normal);
//                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX,
//                        font_content);

                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
//                ((TextView) v).setTypeface(tnr_normal);
//                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX,
//                        font_content);

                return v;
            }
        };
        dataAdapter.setDropDownViewResource(R.layout.spinnerdropdownitem);
        txt_md.setThreshold(1);
        txt_md.setAdapter(dataAdapter);
        dataAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w
                    .getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
                if (dropdown_menu.getVisibility() == View.VISIBLE) {
                    dropdown_menu.setVisibility(View.GONE);
//					dropdown_menu.startAnimation(anim_close_preview);

                }


            }
        }
        return ret;
    }

    public void onButtonClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                LoggerHelper.log(TAG, "Registering");

                Constants.isUpdate = false;
                arraySelectedBrand = new ArrayList<String>();
                if (Check_Entries() == true) {
                    Log.i("", "no error");
                    GenerateRecordID();
                    Log.d("=========PATIENT CODE", patient_code);
                    // get md_code given the Md_name
                    // check if the md name is in the database, if yes get the md
                    // code. if not, generate unique md code
                    String mobile = txt_mobile.getText().toString();
                    if (InputValidation.isMobileNumberValid(mobile)) {
                        Cursor check_md;
                        if (Constants.choosefilterQueryName == false) {
                            check_md = databaseHandler.get_mdFalse(txt_md.getText()
                                    .toString().trim());

                            if (check_md.getCount() != 0) {
                                Log.i("MD in Database", "YES");
                                md_code = check_md.getString(check_md
                                        .getColumnIndex(Constants.MD_CODE));
                            } else {
                                Log.i("MD in Database", "NO");

                                try {
                                    String query = "SELECT MAX(rowid) AS _id FROM "
                                            + Constants.DATABASE_TABLE_MD;
                                    Cursor cursorReportCode = databaseHandler
                                            .getLastRecordId(query);
                                    String _id = cursorReportCode
                                            .getString(cursorReportCode
                                                    .getColumnIndex("_id"));

                                    Log.i("===============", "GenerateCode");
                                    Log.i("Previous: MD_Code", _id);
                                    int new_value = Integer.parseInt(_id) + 1;
                                    md_code = String.valueOf(new_value);


                                    Log.i("New Created Update: ", String.valueOf(new_value));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            check_md = databaseHandler.get_mdTrue(txt_md.getText()
                                    .toString().trim());

                            if (check_md.getCount() != 0) {
                                Log.i("MD in Database", "YES");
                                md_code = check_md.getString(check_md
                                        .getColumnIndex(Constants.MDLIST_CODE));
                            } else {
                                Log.i("MD in Database", "NO");

                                try {
                                    String query = "SELECT MAX(rowid) AS _id FROM "
                                            + Constants.DATABASE_TABLE_MD;
                                    Cursor cursorReportCode = databaseHandler
                                            .getLastRecordId(query);
                                    String _id = cursorReportCode
                                            .getString(cursorReportCode
                                                    .getColumnIndex("_id"));

                                    Log.i("===============", "GenerateCode");
                                    Log.i("Previous: MD_Code", _id);

                                    int new_value = Integer.parseInt(_id) + 1;
                                    md_code = String.valueOf(new_value);


                                    Log.i("New Created Update: ", md_code);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        // get the selected brand name+
                        arraySelectedBrand.clear();
                        try {
                            for (int j = 0; j < checkSelected.length; j++) {
                                checkSelected[0] = false;
                                if (checkSelected[j] == true) {
                                    String brand_name = BrandListAdapter.mListItems
                                            .get(j);
                                    Cursor c_code = databaseHandler
                                            .get_brand_Code(brand_name);
                                    String brand_code = c_code.getString(c_code
                                            .getColumnIndex(Constants.BRAND_CODE));
                                    arraySelectedBrand.add(brand_code);
                                }
                            }
                            Log.i("Selected Brands", arraySelectedBrand.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //for checking if MD is valid
                        if (databaseHandler.isValidDoctor(txt_md.getText().toString(), Constants.choosefilterQueryName)) {
                            new Saving().execute();
                        } else {
                            Toast.makeText(context, "MD does not exist.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        LoggerHelper.toast(this, "Please check your mobile number");

                    }


                } else {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_clear:
                clear_entry();
                break;

            case R.id.menu_btn_Update:
                DialogManager.getInstance().showUpdateDialog(this);
                Constants.isUpdate = false;

                break;

            case R.id.btn_menu:
                if (menu_clicked == false) {
                    menu_clicked = true;
                    dropdown_menu.setVisibility(View.VISIBLE);
//				dropdown_menu.startAnimation(anim_preview);
                } else if (menu_clicked == true) {
                    menu_clicked = false;
                    dropdown_menu.setVisibility(View.GONE);
//				dropdown_menu.startAnimation(anim_close_preview);
//				anim_close_preview
//				.setAnimationListener(new AnimationListener() {
//
//					@Override
//					public void onAnimationStart(Animation animation) {
//
//					}
//
//					@Override
//					public void onAnimationRepeat(Animation animation) {
//
//					}
//
//					@Override
//					public void onAnimationEnd(Animation animation) {
//						dropdown_menu.setVisibility(View.GONE);
//						dropdown_menu.clearAnimation();
//					}
//				});
                }
                break;

            case R.id.menu_btn_admin:
                menu_clicked = false;
                Constants.ADMIN_PATH = 1;
                dropdown_menu.setVisibility(View.GONE);
                Intent admin = new Intent(RegistrationPage.this, AdminPage.class);
                startActivity(admin);
                finish();
                clear_entry();
                btn_register.setVisibility(View.VISIBLE);
                btn_update.setVisibility(View.GONE);
                Constants.isUpdate = false;
                break;

            case R.id.menu_btn_mdlist:
                Constants.MDLIST_PATH = 1;
                Intent mdlist = new Intent(RegistrationPage.this, MdlistPage.class);
                startActivity(mdlist);
                finish();
                Constants.isUpdate = false;
                btn_register.setVisibility(View.VISIBLE);
                break;

            case R.id.menu_btn_search:
                Constants.SEARCH_PATH = 1;
                Intent search = new Intent(RegistrationPage.this, SearchPage.class);
                startActivity(search);
                finish();
                btn_register.setVisibility(View.VISIBLE);
                btn_update.setVisibility(View.GONE);
                Constants.isUpdate = false;
                break;

            case R.id.btn_update:
                Log.i("", "update");
                arraySelectedBrand = new ArrayList<String>();
                if (Check_Entries() == true) {
                    Log.i("", "no error");
                    Log.d("=========PATIENT CODE", patient_code);
                    // get md_code given the Md_name
                    // check if the md name is in the database, if yes get the md
                    // code. if not, generate unique md code
//				if (Constants.choosefilterQueryName == false) {SSSS
                    Cursor check_md = databaseHandler.get_mdFalse(txt_md.getText()
                            .toString().trim());

                    if (check_md.getCount() != 0) {
                        Log.i("MD in Database", "YES");
                        md_code = check_md.getString(check_md
                                .getColumnIndex(Constants.MD_CODE));
                    } else {
                        Log.i("ELI", "NO");
                        md_code = "000" + patient_code;


                    }
                    arraySelectedBrand.clear();

//				}else{
//
//					Cursor check_md = databaseHandler.get_mdTrue(txt_md.getText()
//							.toString().trim());
//					if (check_md.getCount() != 0) {
//						Log.i("MD in Database", "YES");
//						md_code = check_md.getString(check_md
//								.getColumnIndex(Constants.MDLIST_CODE));
//					} else {
//						Log.i("ELI", "NO");
//						md_code = "000" + patient_code;
//						databaseHandler.SaveMD(md_code, txt_md.getText().toString()
//								.toUpperCase());
//					}
//
//				}

                    try {
                        for (int j = 0; j < checkSelected.length; j++) {
                            checkSelected[0] = false;
                            if (checkSelected[j] == true) {
                                String brand_name = BrandListAdapter.mListItems
                                        .get(j);
                                Cursor c_code = databaseHandler
                                        .get_brand_Code(brand_name);
                                String brand_code = c_code.getString(c_code
                                        .getColumnIndex(Constants.BRAND_CODE));
                                arraySelectedBrand.add(brand_code);
                            }
                        }
                        Log.i("Selected Brands", arraySelectedBrand.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (databaseHandler.isValidDoctor(txt_md.getText().toString(), Constants.choosefilterQueryName)) {
                        new Updating().execute();
                    } else {
                        Toast.makeText(context, "MD does not exist.", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.menu_btn_updateRecepient:
                DialogManager.getInstance().showEmailSettingDialog(this, databaseHandler);
                break;

            case R.id.menu_btn_logout:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        RegistrationPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to log out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        Intent intent = new Intent(
                                                RegistrationPage.this, Login.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }
    }

    private void GenerateRecordID() {
        // setting the record id
        try {
            String queryCount = "SELECT COUNT(*) FROM tbl_Patients";
            Cursor dataCount = databaseHandler.getCodeCount(queryCount);
            Log.d("check cursor", String.valueOf(dataCount.getInt(0)));
            if (dataCount.getInt(0) == 0) {
                patient_code = "PSR-00001";
            } else {
                try {
                    String query = "SELECT MAX(_id) AS _id FROM "
                            + Constants.DATABASE_TABLE_PATIENTS;
                    Cursor cursorReportCode = databaseHandler
                            .getLastRecordId(query);
                    String _id = cursorReportCode.getString(cursorReportCode
                            .getColumnIndex(Constants.COLUMN_ID));

                    Log.i("===============", "GenerateCode");
                    Log.i("Previous:", _id);
                    int length = _id.length();
//					String _lastnumber = _id.substring(length - 1, length);

//					int reportIndex = Integer.parseInt(_lastnumber);
//					int incrementedReportIndex = reportIndex + 1;

                    int incrementLastId = Integer.parseInt(_id) + 1;

                    patient_code = String.format("PSR-" + "%05d",
                            incrementLastId);
                    Log.i("New Created: ", patient_code);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Boolean Check_Entries() {
        String fname = txt_fname.getText().toString();
        String lname = txt_lname.getText().toString();
        String mobile = txt_mobile.getText().toString();
        String landline = txt_landline.getText().toString();
        String md = txt_md.getText().toString();

        String landlineNumRegEx = "\\d{16}";
        Matcher matcherLandlineNo = Pattern.compile(landlineNumRegEx).matcher(
                txt_landline.getText().toString());

        if (fname.equals("") || lname.equals("") || landline.equals("")
                || mobile.equals("") || md.equals("")
                || BrandListAdapter.selectedCount == 0) {

            if (fname.equals("")) {
                message = "Please enter your first name";
                txt_fname.requestFocus();
                return false;
            } else if (lname.equals("")) {
                message = "Please enter your last name";
                txt_lname.requestFocus();
                return false;
            } else if (mobile.length() <= 3) {

                if (!landline.equals("")) {
                    return true;
                } else {
                    message = "Please enter either your mobile or landline number.";
                    txt_mobile.requestFocus();
                    return false;
                }

            }

            // else if(landline.length() <= 9){
            //
            // message = "Invalid landline number.";
            // txt_landline.requestFocus();
            // return false;
            //
            // }

            else if (!landline.equals("") && landline.length() < 9
                    && !matcherLandlineNo.matches()) {
                message = "Invalid landline number.";
                txt_landline.requestFocus();
                return false;
            }

            // else if(landline.length() == 0){
            // if(mobile.length() >= 3){
            //
            // return false;
            // }
            //
            // // return true;
            //
            // }
            //
            // else if (landline.length() < 9){
            // message = "Invalid landline number.";
            // txt_landline.requestFocus();
            // return false;
            // }

            // else if (landline.length() < 9){
            // if(mobile.length() <= 3){
            // return true;
            // }else{
            //
            //
            // message = "Invalid landline number.";
            // txt_landline.requestFocus();
            // return false;
            // }
            // }
            //
            //
            //
            //
            // }

            // else if(landline.equals("")){
            // message = "Please enter your landline number";
            // txt_landline.requestFocus();
            // return false;
            // }

            else if (md.equals("")) {
                message = "Please enter your MD's name";
                txt_md.requestFocus();
                return false;
            } else if (BrandListAdapter.selectedCount == 0) {
                message = "Please enter your brand prescribed";
                txt_md.requestFocus();
                return false;
            }

        } else if (!fname.equals("") && !lname.equals("")
                && !landline.equals("") // && !mobile.equals("")
                && !md.equals("") && BrandListAdapter.selectedCount != 0) {
            if (!InputValidation.isMobileNumberValid(mobile)) {
                message = "Invalid mobile number.";
                txt_mobile.requestFocus();
                return false;
            }

            // if(mobile.length() != 12 || !mobile.startsWith("639")){
            // message = "Invalid mobile number.";
            // txt_mobile.requestFocus();
            // return false;
            // }

            else if (!landline.equals("")
                    && (landline.length() > 16 || landline.length() < 15)) {
                Log.i("landline.length()", String.valueOf(landline.length()));

                if ((txt_landline.getText().toString().length() > 14)) {
                    if (txt_landline.getText().toString().length() == 15) {
                        return true;
                    } else if (txt_landline.getText().toString().length() == 16) {
                        return true;
                    } else {
                        message = "Invalid landline number.";
                        return false;
                    }
                } else {
                    message = "Invalid landline number.";
                    return false;
                }
            }

        } else {
            return true;
        }

        return true;


    }

    public class Saving extends AsyncTask<String, String, Boolean> {
        private ProgressDialog progressDialog;
        Exception exception = null;
        String message_content = "";

        // DatabaseHandler databaseHandler;

        protected void onPreExecute() {
            progressDialog = new ProgressDialog(RegistrationPage.this);
            progressDialog.setMessage("Registering....");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            try {
                Calendar calendar = Calendar.getInstance();

                Timestamp timeStamp = new Timestamp(
                        calendar.get(Calendar.YEAR) - 1900,
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        calendar.get(Calendar.HOUR),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND), 0);
                patient_fname = txt_fname.getText().toString();
                patient_lname = txt_lname.getText().toString();
                patient_mobile = "+" + txt_mobile.getText().toString();
                patient_landline = txt_landline.getText().toString();
                Log.i(TAG, "Insertend data " + arraySelectedBrand.toString());
                Log.i(TAG, "list of brand" + arraySelectedBrand.size());
                for (int i = 0; i < arraySelectedBrand.size(); i++) {
                    String brand_code = "";
                    Cursor get_keyword = databaseHandler
                            .getKeyword(arraySelectedBrand.get(i));
                    String keyword = get_keyword.getString(get_keyword
                            .getColumnIndex(Constants.BRAND_KEYWORD));

                    if (arraySelectedBrand.get(i) == "V1"
                            || arraySelectedBrand.get(i) == "V2") {
                        brand_code = "V";
                    } else {
                        brand_code = arraySelectedBrand.get(i);
                    }

                    String coupon_code = brand_code + Constants.g_psrCode + md_code;
                    Log.i(TAG, "coupon code " + coupon_code);
                    String remarks = "";
                    Log.i("Brand Saved: ", "" + arraySelectedBrand.get(i));
                    databaseHandler.SaveBrand(patient_code,
                            arraySelectedBrand.get(i), coupon_code);

                    Log.i("Coupon Code " + String.valueOf(i) + ":", coupon_code);
                    // if(!txt_mobile.getText().toString().equals("639")){

                    String mobile = "";
//                    if (!txt_mobile.getText().toString().equals("639")) {
//                        mobile = patient_mobile.replace("+", "");
//                        mobile = "0" + mobile.substring(2, 12);
//                        Log.i("MOBILE", mobile);
//                    }

                    message_content = "Key: " + Constants.KEY +
                            "\nName: " + patient_lname + ", " + patient_fname +
                            "\nCoupon code: " + coupon_code +
                            "\nMobile Number: " + mobile +
                            "\nLandline: " + patient_landline +
                            "\nRemarks: " + remarks;

                    Constants.TEXT_MESSAGE = message_content;

                    // sms.sendTextMessage(Constants.SMS_RECEPIENT, null,
                    // keyword + " " + patient_lname + ","
                    // + patient_fname + ", " + mobile, null, null);

                    sendSMS();


                    // }

                }


//				databaseHandler.SaveMD_Temp(md_code, txt_md.getText()
//						.toString().toUpperCase());


//				databaseHandler.SaveMD(md_code, txt_md.getText()
//						.toString().toUpperCase());
                Log.i("Save: ", patient_code + patient_fname + patient_lname + patient_mobile + patient_landline + Constants.g_psrCode + md_code + timeStamp.toString());
                databaseHandler.SavePatient(patient_code, patient_fname,
                        patient_lname, patient_mobile, patient_landline,
                        Constants.g_psrCode, md_code, timeStamp.toString());

            } catch (Exception e) {
                Log.e("ClientServerDemo", "Error:", e);
                exception = e;
                return false;
            }

            return true;
        }

        private void saveUnsentSMS(String text) {

            String textFileName = "UnsentRegistration.txt";
            String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/PSR_Registration_Form/Unsent Registration/";
            File dir = new File(dirPath);
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Toast.makeText(context, "Failed to create directory.", Toast.LENGTH_LONG).show();
                }
            }

            File fileUnsentText = new File(dir, textFileName);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(fileUnsentText);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            try {
                out.write(text.getBytes());
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onPostExecute(Boolean valid) {
            try {
                progressDialog.dismiss();

                Log.d("RESULT", String.valueOf(valid));
                if (valid) {

//					sendSMS();

                    Log.i("", "SAVED!");
                    RegistrationPage.this.startActivity(new Intent(
                            RegistrationPage.this, ExitScreenActivity.class));
                    finish();
                    clear_entry();

                } else {
                    Toast.makeText(RegistrationPage.this, "Failed to save..", Toast.LENGTH_SHORT)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void sendSMS() {
            Constants.noticationCount = 0;
            String sent = "SMS_SENT";
            String DELIVERED = "android.provider.Telephony.SMS_DELIVERED";

            PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                    new Intent().putExtra(Constants.INTENT_ACTION_TAG, Constants.SMS_SENT_RECEIVER)
                            .setAction(Constants.SMS_RECEIVER), 0);

            PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                    new Intent().putExtra(Constants.INTENT_ACTION_TAG, Constants.SMS_DELIVERED_RECEIVER)
                            .setAction(Constants.SMS_RECEIVER), 0);

////			//---when the SMS has been sent---
//            context.registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            Toast.makeText(getBaseContext(), "SMS sent",
//                                    Toast.LENGTH_SHORT).show();
//
//                            break;
//                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                            Toast.makeText(getBaseContext(), "Generic failure",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_NO_SERVICE:
//                            Toast.makeText(getBaseContext(), "No service",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_NULL_PDU:
//                            Toast.makeText(getBaseContext(), "Null PDU",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_RADIO_OFF:
//                            Toast.makeText(getBaseContext(), "Radio off",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }
//            }, new IntentFilter(sent));
////
//            //---when the SMS has been delivered---
//            registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            Toast.makeText(getBaseContext(), "SMS delivered",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case Activity.RESULT_CANCELED:
//                            Toast.makeText(getBaseContext(), "SMS not delivered",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }
//            }, new IntentFilter(DELIVERED));

            SmsManager sms = SmsManager.getDefault();
            String recipient = InputFormatter.formatMobileNumber(txt_mobile.getText().toString());
            sms.sendTextMessage(recipient, null, message_content, sentPI, deliveredPI);
            LoggerHelper.log(TAG, "SMS Message: " + message_content + " to " + recipient);
        }
    }

    public class Updating extends AsyncTask<String, String, Boolean> {
        private ProgressDialog progressDialog;
        Exception exception = null;

        // DatabaseHandler databaseHandler;

        protected void onPreExecute() {
            progressDialog = new ProgressDialog(RegistrationPage.this);
            progressDialog.setMessage("Updating....");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(String... arg0) {
            try {
                Calendar calendar = Calendar.getInstance();

                Timestamp timeStamp = new Timestamp(
                        calendar.get(Calendar.YEAR) - 1900,
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        calendar.get(Calendar.HOUR),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND), 0);
                patient_fname = txt_fname.getText().toString();
                patient_lname = txt_lname.getText().toString();
                patient_mobile = "+" + txt_mobile.getText().toString();
                patient_landline = txt_landline.getText().toString();
                Log.i("Updating" + patient_code, arraySelectedBrand.toString());
                databaseHandler.deleteBrand(patient_code);
                for (int i = 0; i < arraySelectedBrand.size(); i++) {
                    String brand_code = "";
                    Cursor get_keyword = databaseHandler
                            .getKeyword(arraySelectedBrand.get(i));
                    String keyword = get_keyword.getString(get_keyword
                            .getColumnIndex(Constants.BRAND_KEYWORD));

                    if (arraySelectedBrand.get(i) == "V1"
                            || arraySelectedBrand.get(i) == "V2") {
                        brand_code = "V";
                    } else {
                        brand_code = arraySelectedBrand.get(i);
                    }

                    String coupon_code = brand_code + psr_code + md_code;
                    String remarks = "";
                    databaseHandler.SaveBrand(patient_code,
                            arraySelectedBrand.get(i), coupon_code);

                    // if(!txt_mobile.getText().toString().equals("639")){

                    String mobile = "";
                    if (!txt_mobile.getText().toString().equals("639")) {
                        mobile = patient_mobile.replace("+", "");
                        mobile = "0" + mobile.substring(2, 12);
                        Log.i("MOBILE", mobile);
                    }

                    String message_content = Constants.KEY + " " + patient_lname
                            + "," + patient_fname + "," + coupon_code + ","
                            + mobile + "," + patient_landline + "," + remarks;

                    // sms.sendTextMessage(Constants.SMS_RECEPIENT, null,
                    // keyword + " " + patient_lname + ","
                    // + patient_fname + ", " + mobile, null, null);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(Constants.SMS_RECEPIENT, null,
                            message_content, null, null);
                    // }
                }

//				databaseHandler.SaveMD_Temp(md_code, txt_md.getText()
//						.toString().toUpperCase());

                databaseHandler.SaveMD(md_code, txt_md.getText().toString()
                        .toUpperCase());


                databaseHandler.updateDetails(patient_code, patient_fname,
                        patient_lname, patient_mobile, patient_landline,
                        psr_code, md_code, timeStamp.toString());

            } catch (Exception e) {
                Log.e("ClientServerDemo", "Error:", e);
                exception = e;
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean valid) {
            try {
                progressDialog.dismiss();

                Log.d("RESULT", String.valueOf(valid));
                if (valid) {

                    Log.i("", "SAVED!");
                    Toast.makeText(getApplicationContext(), "Updated",
                            Toast.LENGTH_SHORT).show();
//					arraySelectedBrand = new ArrayList<String>();
//					arraySelectedBrand = new ArrayList<String>();
                    BrandListAdapter.selectedCount = 0;
                    Constants.isUpdate = false;
                    Constants.UpdateID = "";
                    btn_update.setVisibility(View.GONE);
                    btn_register.setVisibility(View.VISIBLE);
                    Intent i = new Intent(RegistrationPage.this,
                            RegistrationPage.class);
                    startActivity(i);
                    finish();
                    clear_entry();

                } else {
                    Toast.makeText(RegistrationPage.this, "Failed to save..", Toast.LENGTH_SHORT)
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clear_entry() {
        txt_fname.setText("");
        txt_lname.setText("");
        txt_mobile.setText("");
        txt_landline.setText("");
        txt_md.setText("");
        BrandListAdapter.selectedCount = 0;
        BrandCount = 0;
        for (int j = 0; j < checkSelected.length; j++) {
            checkSelected[j] = false;
        }
        lbl_BrandSelected.setText("Select Brand");
    }


    public void clear_entry2() {
        txt_fname.setText("");
        txt_lname.setText("");
        txt_mobile.setText("");
        txt_landline.setText("");
        txt_md.setText("");
        BrandListAdapter.selectedCount = 0;
        BrandCount = 0;
        for (int j = 0; j < checkSelected.length; j++) {
            checkSelected[j] = false;
        }
        lbl_BrandSelected.setText("Select Brand");
    }


    @Override
    public void onBackPressed() {

        if (Constants.isUpdate == true) {
            Intent i = new Intent(RegistrationPage.this, SearchPage.class);
            startActivity(i);
            finish();
        } else {
            // if (doubleBackToExitPressedOnce) {
            // android.os.Process.killProcess(android.os.Process.myPid());
            // // super.onBackPressed();
            // return;
            // }
            // this.doubleBackToExitPressedOnce = true;
            // Toast.makeText(this, "Please click BACK again to exit",
            // Toast.LENGTH_SHORT).show();
            // new Handler().postDelayed(new Runnable() {
            // @Override
            // public void run() {
            // doubleBackToExitPressedOnce = false;
            // }
            // }, 2000);

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        menu_clicked = false;
        dropdown_menu.setVisibility(View.GONE);
    }


    public void dialog_for_MenuUpdate() {
        final Dialog dialog = new Dialog(RegistrationPage.this);

        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        int height2 = (displayMetrics.heightPixels * 2) / 4;
        int width2 = (displayMetrics.widthPixels * 2) / 5;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_update);
        dialog.getWindow().setLayout(width2, height2);

        // Castings
        final CheckBox checkBox_MD = (CheckBox) dialog
                .findViewById(R.id.checkBox_MD);
        final CheckBox checkBox_BrandProduct = (CheckBox) dialog
                .findViewById(R.id.checkBox_BrandProduct);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Button btn_sync = (Button) dialog.findViewById(R.id.btn_sync);

        dialog.show();

        // CANCEL BUTTON
        btn_cancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });

        // SYNC BUTTON
        btn_sync.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // 1. PAG HINDI GUMANA YUNG TRUE, GAWIN PURO IF CONDITION AND
                // TRY
                // 2. Lagay sa unang if you may && then else if na yung tig
                // isang check lang - WORKING

                if (NetworkHelper.isInternetAvailable(RegistrationPage.this)) {
                    if (checkBox_MD.isChecked() == true
                            && checkBox_BrandProduct.isChecked() == true) {
                        Constants.isCheckBoth = true;
                        LoggerHelper.log(TAG, "MD and BrandProduct checked");
                        // IF BOTH CHECKED MIRROR ASYCH FOR MD AND PRODUCTS CALL
//						Async_FTP_Download_BrandProduct_Mirror a2 = new Async_FTP_Download_BrandProduct_Mirror(
//								context);
//						a2.execute();
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                        dialog.dismiss();

                    } else if (checkBox_BrandProduct.isChecked()) {
                        LoggerHelper.log(TAG, "BrandProduct checked");
//						Async_FTP_Download_BrandProduct asyncuser = new Async_FTP_Download_BrandProduct(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadBrand brand = new FTP_DownloadBrand(context);
                        brand.execute();
                        dialog.dismiss();

                    } else if (checkBox_MD.isChecked()) {
                        LoggerHelper.log(TAG, "MD checked");
//						Async_FTP_Download_MD asyncuser = new Async_FTP_Download_MD(
//								context);
//						asyncuser.execute();
                        Constants.isCheckBoth = false;
                        FTP_DownloadMD md_codes = new FTP_DownloadMD(context);
                        md_codes.execute();
                        dialog.dismiss();

                    } else {
                        LoggerHelper.log(TAG, "Select first");
                        Toast.makeText(RegistrationPage.this,
                                "Please select an item to update.",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(RegistrationPage.this,
                            "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }
        });

    }


    private void updateRecepientDialog() {

        LayoutInflater factory = LayoutInflater.from(context);
        final View dialogView = factory.inflate(R.layout.dialog_email_settings, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);


        // cast
        final EditText txt_emailRecipient = (EditText) dialog.findViewById(R.id.txt_emailRecipient);
        TextView lbl_header_settings = (TextView) dialog.findViewById(R.id.lbl_header_settings);
        Button btn_addEmail = (Button) dialog.findViewById(R.id.btn_addEmail);
        final ListView lv_recipients = (ListView) dialogView
                .findViewById(R.id.lv_recipients);

        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lv_recipients.setAdapter(recipientAdapter);


        lv_recipients.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long id) {

                final String emailAdd = (String) arg0.getItemAtPosition(position);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        RegistrationPage.this);
                alertDialogBuilder.setTitle("Please Confirm");
                alertDialogBuilder
                        .setMessage("Are you sure you want to delete this email address?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        Log.i("adsf", emailAdd);
                                        databaseHandler.deleteRecipient(emailAdd);

                                        ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                                        ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);
                                        recipientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        lv_recipients.setAdapter(recipientAdapter);
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

                return false;
            }
        });

//        // SET FONTS
//        txt_emailRecipient.setTypeface(tnr_normal);
//        btn_addEmail.setTypeface(tnr_normal);
//        lbl_header_settings.setTypeface(tnr_bold);

        btn_addEmail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i("btn_addEmail", "btn_addEmail");
                String emailAdd = txt_emailRecipient.getText().toString();

                if (emailAdd.length() == 0) {
                    LoggerHelper.toast(RegistrationPage.this, "Please enter email address");

                } else {
                    // Invalid Email Address
                    if (!isValidEmail(emailAdd)) {
                        LoggerHelper.toast(RegistrationPage.this, "Invalid email address");

                    } else {

                        if (!databaseHandler.checkIfEmailExist(emailAdd)) {
                            databaseHandler.addRecepient(emailAdd);

                            ArrayList<String> recipient = databaseHandler.getEmailRecipient();
                            ArrayAdapter<String> recipientAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, recipient);

                            lv_recipients.setAdapter(recipientAdapter);

                            txt_emailRecipient.setText("");

                            LoggerHelper.toast(RegistrationPage.this, "Email address successfully added.");

                        } else {

                            LoggerHelper.toast(RegistrationPage.this, "Email address already exist.");
                        }


                    }
                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.show();

    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Log.i("Pattern", " " + pattern);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}